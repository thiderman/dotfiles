(require 'hydra)

(defvar project-hydra-projects '()
  "A list of the projects")

(defmacro defprojecthydra (project-name &rest args)
  ""
  (declare (indent defun))
  (let* ((name (symbol-name project-name))
         (hydra-name (make-symbol (format "project-hydra/%s" name))))
    `(progn
       (defhydra ,hydra-name
         ,@args)
       (add-to-list 'project-hydra-projects ,name t))))

(defun project-hydra (&optional project-name)
  "Launch a project hydra.

PROJECT-NAME is optional. If omitted it will try the directory name of the
  current `projectile-project-root' and will be prompted for otherwise."
  (interactive
   (list (let ((dir (f-base (or (projectile-project-root) ""))))
           (if (member dir project-hydra-projects)
               dir
             (completing-read "project: " project-hydra-projects nil t)))))

  (when (not (member project-name project-hydra-projects))
    (user-error "No known project-hydra for `%s`" project-name))

  (funcall (intern (format "project-hydra/%s/body" project-name))))

(provide 'project-hydra)
