(require 'json)

(defun drunkenfall-tournaments/switch ()
  (interactive)
  (setq drunkenfall-tournament-id (tabulated-list-get-id))
  (drunkenfall-overview))

(define-derived-mode drunkenfall-tournaments-mode tabulated-list-mode
  "DrunkenFall"
  "List of all DrunkenFall tournaments"
  (let ((entry (drunkenfall-table
                :kind "tournament"
                :url "tournaments/"
                :id-field 'id
                :fields (list '("id" 4 t)
                              '("league_id" 8 t)
                              '("name" 50 t)
                              '("slug" 15 t)
                              '("opened" 20 t)
                              '("scheduled" 20 t)
                              '("started" 20 t)
                              '("qualifying_end" 20 t)
                              '("ended" 20 t)
                              '("color" 20 t)
                              '("cover" 20 t)
                              '("length" 20 t)
                              '("final_length" 20 t))
                :hidden (list "opened" "scheduled" "qualifying_end" "length" "final_length"))))
    (setq tabulated-list-entries (drunkenfall-table-entries entry))
    (setq tabulated-list-format (drunkenfall-table-headers entry)))

  (let ((map drunkenfall-tournaments-mode-map))
    (define-key map [(return)] #'drunkenfall-tournaments/switch)
    (define-key map "g" #'drunkenfall-tournaments)
    (define-key map "s" #'drunkenfall-scores)
    (define-key map "o" #'drunkenfall-overview)
    map)
  (hl-line-mode 1)
  (tabulated-list-init-header))

(defun drunkenfall-tournaments ()
  "Show an interactive list of all DrunkenFall tournaments"
  (interactive)
  (with-current-buffer (get-buffer-create "*drunkenfall-tournaments*")
    (drunkenfall-tournaments-mode)
    (tabulated-list-print)
    (switch-to-buffer (current-buffer))))

(provide 'drunkenfall-tournaments-mode)
