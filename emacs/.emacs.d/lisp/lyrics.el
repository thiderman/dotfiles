(setq-default lyrics-start nil)

(defun lyrics-timestamp ()
  (interactive)
  (beginning-of-line)
  (if (not lyrics-start)
      ;; If we haven't started in the current buffer yet, set the time of start
      ;; so we know what we're working with
      (progn
        (beginning-of-buffer)
        (lyrics-play)
        (setq lyrics-start (current-time))
        (message "Lyrics session started"))

    ;; If we have one, it's time to calculate the current relative time and
    ;; insert the timestamp
    (let* ((timestamp (time-subtract (current-time) lyrics-start)))
      (insert
       (format "[%02d:%02d.%02d] "
               (/ (second timestamp) 60)
               (mod (second timestamp) 60)
               (floor (/ (third timestamp) 10000.0))))
      (next-line)
      ;; Skipping empty lines so we can have paragraph blocks
      ;; (when (and (bolp) (eolp))
      ;;   (next-line))
      ))
  (recenter)
  (beginning-of-line))

(defvar lyrics-modify nil)

(defun lyrics-alter (seconds)
  "Parse the timestamp on the current line and add the time from `lyrics-modify'"
  (interactive
   (list
    (or (and (not current-prefix-arg) lyrics-modify)
        (setq lyrics-modify (read-number "seconds: ")))))
  (back-to-indentation)
  (let* ((nums (lyrics--parse-timestamp-at-point))
         (altered (lyrics--add-time seconds nums)))
    (insert
     (format "[%02d:%02d.%02d]"
             ;; TODO(thiderman): There is a better way to do this, but I'm not
             ;; looking for it right now
             (first altered)
             (second altered)
             (third altered)))
    (delete-char 10)
    (next-line)))

(defun lyrics--parse-timestamp-at-point ()
  (let* ((str (thing-at-point 'list t))
         (nums (-map
                'string-to-number
                (rest (s-match "\\(..\\):\\(..\\).\\(..\\)" str)))))
    nums))

(defun lyrics--add-time (seconds nums)
  "Adds an amount of seconds to an existing timestamp"
  (let* ((new-seconds (+ seconds (second nums)))
         (ret-seconds (mod new-seconds 60))
         (ret-minutes (+ (/ new-seconds 60) (first nums))))
    (list
     ret-minutes
     ret-seconds
     (third nums))))


(defun lyrics-play ()
  "Play the song for the current lyrics file"
  (let* ((song (s-replace ".txt" ".ogg" (buffer-file-name))))
    (start-process-shell-command
     (f-base song)
     "*lyrics/song*"
     (format "mpv --volume=85 %s" song))))

(defun lyrics-stop ()
  (interactive)
  "Play the song for the current lyrics file"
  (let* ((song (s-replace ".txt" ".ogg" (buffer-file-name))))
    (kill-process
     (f-base song))))

(defun lyrics-reset ()
  (interactive)
  (beginning-of-buffer)
  (while (re-search-forward "^\\[..:..\\...\\] " nil t)
    (replace-match ""))
  (setq lyrics-start nil)
  (beginning-of-buffer)
  (lyrics-stop)
  (message "Lyrics data cleared"))

(defun lyrics-prepare ()
  (interactive)
  (beginning-of-buffer)
  ;; Clear all empty lines
  (flush-lines "^$")
  (beginning-of-buffer)
  ;; Add the stupid || that we need
  (while (re-search-forward "$" nil t)
    (replace-match " ||")
    (next-line))
  (beginning-of-buffer)
  (save-buffer))

(defvar lyrics-keywords
  '(("^\\[..:..\\...\\] " . font-lock-keyword-face)
    (" ||" . font-lock-comment-face)))

(defvar lyrics-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "SPC") 'lyrics-timestamp)
    (define-key map (kbd "R") #'lyrics-reset)
    (define-key map (kbd "a") #'lyrics-alter)
    (define-key map (kbd "s") #'lyrics-stop)
    (define-key map (kbd "p") #'lyrics-prepare)
    (define-key map (kbd "q") #'lyrics-mode)
    map)
  "Keymap for `lyrics-mode'.")

(define-minor-mode lyrics-mode
  "Minor mode to timeset lyrics"
  nil "🎶" lyrics-mode-map
  (font-lock-add-keywords nil lyrics-keywords)
  (if (fboundp 'font-lock-flush)
      (font-lock-flush)
    (when font-lock-mode
      (with-no-warnings (font-lock-fontify-buffer)))))

(bind-key "C-c l" #'lyrics-mode)

(provide 'lyrics)
