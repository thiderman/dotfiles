(require 'json)

(defun drunkenfall-matches/entries ()
  "Make a list of tournaments suitable for display in a tabulated list"
  (let* ((json-object-type 'alist)
         (json-array-type 'list)
         (json (drunkenfall-get (format
                                 "tournaments/%s/matches/"
                                 drunkenfall-tournament-id)))
         (matches (alist-get 'matches json)))
    (-map #'drunkenfall-matches/compose matches)))

(defun drunkenfall-matches/compose (match)
  (let* ((input (assoc-delete-all 'players (assoc-delete-all 'pause match)))
         (id (alist-get 'id match))
         (values (loop for item in input
                       vconcat (list (format "%s" (cdr item))))))
    (list id values)))

(defun drunkenfall-matches/switch ()
  (interactive)
  (message "omfg %s" (tabulated-list-get-id)))

(define-derived-mode drunkenfall-matches-mode tabulated-list-mode
  "DrunkenFall"
  "List of all matches for a given tournament"
  (setq tabulated-list-entries 'drunkenfall-matches/entries)
  (setq tabulated-list-format
        (vector '("id" 4 t)
                '("tid" 4 t)
                '("kind" 10 t)
                '("index" 7 t)
                '("length" 8 t)
                '("title" 20 t)
                '("round" 10 t)
                '("scheduled" 30 t)
                '("started" 30 t)
                '("ended" 30 t)
                '("level" 10 t)
                '("ruleset" 10 t)))
  (let ((map drunkenfall-matches-mode-map))
    (define-key map [(return)] #'drunkenfall-matches/switch)
    (define-key map "g" #'drunkenfall-matches)
    (define-key map "o" #'drunkenfall-overview)
    (define-key map "q" #'drunkenfall-tournaments)
    map)
  (tabulated-list-init-header))

(defun drunkenfall-matches ()
  "Show an interactive list of all DrunkenFall tournaments"
  (interactive)
  (with-current-buffer (get-buffer-create "*drunkenfall-matches*")
    (drunkenfall-matches-mode)
    (tabulated-list-print)
    (switch-to-buffer (current-buffer))))

(provide 'drunkenfall-matches-mode)
