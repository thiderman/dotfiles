(defvar exwm-status-hooks
  '(th/ew/-hud-internal
    exwm-status-date
    exwm-status-week
    exwm-status-unixtime
    exwm-status-utc
    exwm-status-w
    exwm-status-dunst
    exwm-status-battery))

(defvar exwm-status-always-on
  '(th/ew/-hud-internal
    exwm-status-date))

(defun exwm-status-date ()
  "Returns the date"
  (format-time-string "%Y-%m-%d %T"))

(defun exwm-status-week ()
  "Returns the week information"
  (format-time-string "@ %a w%W"))

(defun exwm-status-unixtime ()
  "Returns the seconds since the epoch"
  (format-time-string "%s"))

(defun exwm-status-utc ()
  "Returns the time in UTC"
  (format-time-string "(%T UTC)" nil t))

(defun exwm-status-battery ()
  "Returns battery status, joining multiple batteries together"
  ;; The extra check here is so that we will only run this when acpi is present
  ;; _even_ when we are forcing, i.e. on a desktop.
  (if (exwm-status-battery-p)
      (s-replace
       "\n" "; "
       (s-trim (shell-command-to-string "acpi -b")))
    nil))

(defun exwm-status-battery-p ()
  "Prejudicate to determine if we're running the battery hook or not"
  (and (executable-find "acpi")
       (< (length th/ew/screens) 1)))

(defun exwm-status-w ()
  "Returns some simple system stats"
  (s-replace
   "\n" "; "
   (s-trim (shell-command-to-string "w | head -n 1"))))

(defun exwm-status-dunst ()
  "Returns if dunst is silenced or not"
  (when th/dunst-silenced
    (propertize "Notifications off" 'face 'font-lock-string-face)))

(defun exwm-status-dunst-p ()
  th/dunst-silenced)

(defun exwm-status-exec (hook force-run)
  "Runs a single hook"
  (let* ((name (symbol-name hook))
         (pred (intern (format "%s-p" name))))

    ;; A hook should be run under these circumstances:
    ;;  1) We are forcing all hooks to run, or;
    ;;  2) It is a default hook that is always on, or;
    ;;  3) It has a prejudicate function that returns t.
    (when (or force-run
              (memq hook exwm-status-always-on)
              (and (fboundp pred)
                   (funcall pred)))
      (exwm-status-escape (funcall hook)))))

;;
(defun exwm-status-escape (s)
  "Escapes a string"
  ;; Replace percent signs with a double percent since `message' will be upset otherwise.
  (when s
    (s-replace "%" "%%" s)))

(defun exwm-status (arg)
  (interactive "P")
  (message (s-join (propertize " ; " 'face 'font-lock-comment-delimiter-face)
                   (remove-if nil (mapcar (lambda (f) (funcall 'exwm-status-exec f arg))
                                          exwm-status-hooks)))))

 (provide 'exwm-status)
