;;* Variables and setup
(defface overlay-expander-box
  ;; Default colors graciously borrowed from the glorious lispy.el
  ;; https://github.com/abo-abo/lispy
  '((((class color) (background light))
     :background "#d8d8f7")
    (((class color) (background dark))
     :background "#333333"))
  "Face for the content box")

(defvar overlay-expander-buffer-format "*overlay-expander/%s*"
  "The format string of the buffers to put content into")

(defvar overlay-expander-overlays nil
  "Current overlays registered (all buffers)")

(defvar overlay-expander-lines-threshold nil
  "The amount of lines for when we prefer making overlays.

If `nil', the height of the current window is used.

When the threshold is passed, upon toggling we pop to the special
buffer. If set to zero, always pop to the buffer")

(defvar overlay-expander-expanders nil
  "Expanders to run.")

(defvar overlay-expander-last-expander nil
  "The latest used expander.")

;;* Core functions
(defun overlay-expander-make-overlay (start end content)
  "Make an overlay that shadows the newline at the end of the current line.

This effectively puts it in between two lines as long as the
content is surrounded by newlines."
  (let* ((o (make-overlay start end)))
    (overlay-put o 'display (concat "\n" content "\n"))
    (overlay-put o 'face 'overlay-expander-box)
    (add-to-list 'overlay-expander-overlays o)))



(defun overlay-expander-get-for-current-line ()
  "Get the overlay for the current line"
  (overlays-at (point-at-eol)))

(defun overlay-expander-expand-line (p)
  "Run all the defined expanders on the current line.

Stops processing once one has been found."
  (let (content)
    (mapc (lambda (exp)
            (when (null content)
              (setq content (overlay-expander-execute exp))
              (when content
                (setq overlay-expander-last-expander exp)
                (if (or (overlay-expander-overlay-or-buffer-p exp)
                        (= p 4))
                    (overlay-expander-pop-to-buffer)
                  (overlay-expander-make-overlay start end content)))))
          overlay-expander-expanders)

    (when (null content)
      (message "No expansion found on current line"))))

;;* Interactive functions
;;;###autoload
(defun overlay-expander-clear ()
  "Clear all the overlays in all buffers, and empty the special buffer"
  (interactive)
  (mapc 'delete-overlay overlay-expander-overlays)
  (setq overlay-expander-overlays nil)
  ;; (with-current-buffer overlay-expander-buffer-format
  ;;   (erase-buffer))
  )

;;;###autoload
(defun overlay-expander-toggle (p)
  "Toggles an overlay for the current line.

Makes an overlay under the current line, except for when;
 * The overlay would be longer than `overlay-expander-lines-threshold'.
 * `overlay-expander-lines-threshold' is nil and the overlay would be
   longer than the current window.
 * The universal argument is passed.
In those cases, a buffer with the expanded content is popped."

  (interactive "p")
  ;; Store the positions where the
  (let* ((start (point-at-eol))
         (end (+ 1 start))
         (overlay (overlay-expander-get-for-current-line)))
    (if overlay
        ;; If there already is a overlay for the current line, remove it
        (let ((o (first overlay)))
          (delete-overlay o)
          (remove o overlay-expander-overlays)
          (beginning-of-line))
      (overlay-expander-expand-line p))))

;;;###autoload
(defun overlay-expander-pop-to-buffer ()
  "Pop the work buffer

This lets you inspect and yank etc of the contents of the overlay.

The contents will be from the latest overlay."
  (interactive)

  (when (null overlay-expander-last-expander)
    (user-error "No expander has been run yet"))

  (overlay-expander-pop-buffer overlay-expander-last-expander))

;;* Object classes
;;** Core
(defclass overlay-expander ()
  ((name :initarg :name
         :type string
         :documentation
         "The name of the expander class. Used when making buffers.")))

(cl-defmethod overlay-expander-name ((exp overlay-expander))
  "Get the name of the current expander."

  (oref exp name))

(cl-defmethod overlay-expander-buffer ((exp overlay-expander))
  "Get the buffer to operate on for the current expander."

  (get-buffer-create
   (format
    overlay-expander-buffer-format
    (oref exp name))))

(cl-defmethod overlay-expander-execute ((exp overlay-expander))
  "Put the content into the special buffer, format it, and add faces."
  (condition-case err
      ;; Store the content before switching to the special buffer
      (let* ((content (overlay-expander-extract exp)))
        (when content
          (with-current-buffer (overlay-expander-buffer exp)
            ;; Clear the buffer, and insert the contents
            (erase-buffer)
            (insert content)

            ;; Run the subclass-specific expander
            (overlay-expander-expand exp)

            ;; Force font lock rendering so that the object gets syntax highlighting
            (font-lock-ensure)

            ;; Return all of it, with properties and all.
            (buffer-substring (point-min) (point-max)))))
    (error (user-error "%s expander failed: %s"
                       (overlay-expander-name exp) err))))

(cl-defmethod overlay-expander-overlay-or-buffer-p ((exp overlay-expander))
  "Decide if the expansion should be an overlay or pop to buffer"
  ;; The lines calculation uses `count-lines' in the special buffer so that we
  ;; don't have to count newlines or whatever ourselves.
  (let* ((lines (with-current-buffer (overlay-expander-buffer exp)
                  (count-lines (point-min) (point-max)))))
    (> lines (if (null overlay-expander-lines-threshold)
                 (window-height)
               overlay-expander-lines-threshold))))

(cl-defmethod overlay-expander-pop-buffer ((exp overlay-expander))
  "Pop to the buffer of the current expander."
  (pop-to-buffer (format
                  overlay-expander-buffer-format
                  (oref exp name))))

(cl-defgeneric overlay-expander-expand ((exp overlay-expander))
  "Expand the extracted text.

Executed in a special buffer where the contents have already been
  inserted. If the expansion needs a major mode enabled for font
  locking, it's up to this function to enable it.")

(cl-defgeneric overlay-expander-extract ((exp overlay-expander))
  "Find and return expandable text on the current line.

Should return `nil' if there is nothing to extract on the current line.")

;;** JSON
(defclass overlay-expander-json (overlay-expander)
  ())

(cl-defmethod overlay-expander-expand ((exp overlay-expander-json))
  "Generates the JSON overlay.

Uses `json-mode' and `json-mode-beautify'."
  (json-mode)
  (json-mode-beautify)

  ;; Need to call it twice since it reverses the keys for some reason.
  ;; However, it also converts \n to actual newlines, which is helpful
  ;; but makes the JSON technically invalid. In those cases, just ignore
  ;; the error.
  (let ((inhibit-message t))
    (with-demoted-errors
        (json-mode-beautify))))

(cl-defmethod overlay-expander-extract ((exp overlay-expander-json))
  "Extract one-line JSON on the current line.

Tries to look for an opening brace, and assumes that we are looking at JSON if
  one is found."
  (condition-case err
      (let* ((start)
             (end))
        (save-excursion
          (beginning-of-line)

          (when (not (looking-at "{"))
            (search-forward "{" (point-at-eol))
            (backward-char 1))

          (setq start (point))
          (forward-list)
          (setq end (point))

          (buffer-substring-no-properties start end)))
    ;; TODO(thiderman): Should probably add error handling for debugging here.
    (error nil)))

;;** SQL
(defclass overlay-expander-sql (overlay-expander)
  ())

(cl-defmethod overlay-expander-expand ((exp overlay-expander-sql))
  "Generates the SQL overlay.

Uses `sql-mode'."
  (sql-mode)
  (when (executable-find "sqlfmt")
    (shell-command-on-region (point-min) (point-max)
                             "sqlfmt" nil t)))

(cl-defmethod overlay-expander-extract ((exp overlay-expander-sql))
  "Extract one-line SQL on the current line.

Tries to look for a SELECT statement, and assumes that we are looking at SQL if
  one is found."
  (condition-case err
      (let* ((start)
             (end)
             (eol (point-at-eol)))
        (save-excursion
          (beginning-of-line)

          (when (not (looking-at "SELECT"))
            (search-forward "SELECT" eol)
            (backward-word 1))

          (setq start (point))
          (search-forward ";" eol)
          (setq end (point))

          (buffer-substring-no-properties start end)))
    ;; TODO(thiderman): Should probably add error handling for debugging here.
    (error nil)))

;;* Initialization of expanders

(add-to-list 'overlay-expander-expanders (overlay-expander-json :name "json"))
(add-to-list 'overlay-expander-expanders (overlay-expander-sql :name "sql"))

;;* End
(provide 'overlay-expander)
