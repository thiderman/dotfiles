(require 'json)

(defun drunkenfall-overview/entries ()
  "Make a list of tournaments suitable for display in a tabulated list"
  (let* ((json-object-type 'alist)
         (json-array-type 'list)
         (json (drunkenfall-get (format
                                 "tournaments/%s/overview/"
                                 drunkenfall-tournament-id)))
         (summaries (alist-get 'summaries json)))
    (-map #'drunkenfall-overview/compose summaries)))

(defun drunkenfall-overview/compose (player)
  (let* ((input player)
         (id (alist-get 'nick player))
         (values (loop for item in input
                       vconcat (list (format "%s" (cdr item))))))
    (list id values)))

(defun drunkenfall-overview/switch ()
  (interactive)
  (message "omfg %s" (tabulated-list-get-id)))

(define-derived-mode drunkenfall-overview-mode tabulated-list-mode
  "DrunkenFall"
  "List of all matches for a given tournament"
  (setq tabulated-list-entries 'drunkenfall-overview/entries)
  (setq tabulated-list-format
        (vector '("pos" 4 t)
                '("psid" 5 t)
                '("pid" 4 t)
                '("tid" 4 t)
                '("nick" 20 t)
                '("kills" 7 t :right-align t)
                '("sweeps" 7 t :right-align t)
                '("self" 7 t :right-align t)
                '("shots" 7 t :right-align t)
                '("matches" 7 t :right-align t)
                '("total_score" 15 t :right-align t)
                '("skill_score" 15 t :right-align t)))
  (let ((map drunkenfall-overview-mode-map))
    (define-key map [(return)] #'drunkenfall-overview/switch)
    (define-key map "g" #'drunkenfall-overview)
    (define-key map "s" #'drunkenfall-scores)
    (define-key map "q" #'drunkenfall-tournaments)
    map)
  (tabulated-list-init-header))

(defun drunkenfall-overview ()
  "Show an interactive list of all DrunkenFall tournaments"
  (interactive)
  (with-current-buffer (get-buffer-create "*drunkenfall-overview*")
    (drunkenfall-overview-mode)
    (tabulated-list-print)
    (switch-to-buffer (current-buffer))))

(provide 'drunkenfall-overview-mode)
