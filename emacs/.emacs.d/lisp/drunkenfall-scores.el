(require 'json)

(defun drunkenfall-scores/entries ()
  "Make a list of tournaments suitable for display in a tabulated list"
  (let* ((json-object-type 'alist)
         (json-array-type 'list)
         (json (drunkenfall-get (format
                                 "tournaments/%s/scores/"
                                 drunkenfall-tournament-id)))
         (scores (alist-get 'scores json)))
    (-map #'drunkenfall-scores/compose scores)))

(defun drunkenfall-scores/compose (player)
  (let* ((input player)
         (id (alist-get 'nick player))
         (values (loop for item in input
                       vconcat (list (format "%s" (cdr item))))))
    (list id values)))

(defun drunkenfall-scores/switch ()
  (interactive)
  (message "omfg %s" (tabulated-list-get-id)))

(define-derived-mode drunkenfall-scores-mode tabulated-list-mode
  "DrunkenFall"
  "List of all matches for a given tournament"
  (setq tabulated-list-entries 'drunkenfall-scores/entries)
  (setq tabulated-list-format
        (vector '("tid" 4 t)
                '("mid" 5 t)
                '("title" 20 t)
                '("pid" 5 t)
                '("nick" 20 t)
                '("color" 8 t)
                '("kills" 7 t :right-align t)
                '("sweeps" 7 t :right-align t)
                '("self" 7 t :right-align t)
                '("shots" 7 t :right-align t)
                '("matches" 7 t :right-align t)
                '("total_score" 15 t :right-align t)
                '("match_score" 15 t :right-align t)))
  (let ((map drunkenfall-scores-mode-map))
    (define-key map [(return)] #'drunkenfall-scores/switch)
    (define-key map "g" #'drunkenfall-scores)
    (define-key map "o" #'drunkenfall-overview)
    (define-key map "q" #'drunkenfall-tournaments)
    map)
  (hl-line-mode 1)
  (tabulated-list-init-header))

(defun drunkenfall-scores ()
  "Show an interactive list of all DrunkenFall tournaments"
  (interactive)
  (with-current-buffer (get-buffer-create "*drunkenfall-scores*")
    (drunkenfall-scores-mode)
    (tabulated-list-print)
    (switch-to-buffer (current-buffer))
    (end-of-buffer)
    (previous-line)))

(provide 'drunkenfall-scores-mode)
