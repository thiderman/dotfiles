(require 'json)

(defvar drunkenfall-root "https://drunkenfall.com/api"
  "Root server to call for data")
(defvar drunkenfall-tournament-id ":latest"
  "The ID for the tournament we are currently browsing")

(defun drunkenfall-get (url)
  (let ((url (format "%s/%s" drunkenfall-root url)))
    (with-current-buffer (url-retrieve-synchronously url)
      (beginning-of-buffer)
      (re-search-forward "^{")
      (beginning-of-line)
      (json-read))))

(defclass drunkenfall-table ()
  ((kind :initarg :kind
         :type string
         :documentation
         "The kind of the entry.")
   (url :initarg :url
        :type string
        :documentation
        "The URL from which to grab data from")
   (id-field :initarg :id-field
             :type symbol
             :documentation
             "The symbol to grab the id from")
   (fields :initarg :fields
           :type list
           :documentation
           "A vector of tabulated-list compatible entries. See `tabulated-list-format'")
   (hidden :initarg :hidden
           :type list
           :documentation
           "A list of fields that should not be displayed in the tabulated list")))

(cl-defmethod drunkenfall-table-kind ((e drunkenfall-table))
  "Get the kind of the current drunkenfall-table."
  (oref e kind))

(cl-defmethod drunkenfall-table-url ((e drunkenfall-table))
  "Get the relative URL we are grabbing data from"
  (oref e url))

(cl-defmethod drunkenfall-table-get ((e drunkenfall-table))
  "Grab the RAW data entries from the endpoint"
  (let* ((json-object-type 'alist)
         (json-array-type 'list)
         (json (drunkenfall-get (drunkenfall-table-url e)))
         (items (cdar json)))
    items))

(cl-defmethod drunkenfall-table-headers ((table drunkenfall-table))
  "Get the headers we are to display.

The result is to be fed into `tabulated-list-format'"
  (vconcat (-filter (lambda (x) (not (member (car x) (oref table hidden))))
                    (oref table fields))))

(cl-defmethod drunkenfall-table-entries ((table drunkenfall-table))
  "Get all the data we want to show"
  (-map (lambda (line)
          (drunkenfall-table-compose-fields table line))
        (drunkenfall-table-get table)))

(cl-defmethod drunkenfall-table-compose-fields ((table drunkenfall-table)
                                                row)
  "Grabs the fields formatted and in the order that we want to
show them"
  (list
   ;; First item is the ID to be used for reference
   (alist-get (oref table id-field) row)
   ;; Second item is a vector of the values we want to display
   (vconcat (-map (lambda (key)
                    ;; Cast to string, since that's needed by
                    ;; `tabulated-list-mode'
                    (format "%s" (alist-get key row)))
                  (drunkenfall-table-keys table)))))

(cl-defmethod drunkenfall-table-keys ((e drunkenfall-table))
  "Get the keys we grab data from, in order"
  (-map (lambda (x) (intern (car x)))
        (drunkenfall-table-headers e)))

(provide 'drunkenfall-mode)


;; (let ((e (drunkenfall-table
;;           :kind "tournament"
;;           :url "tournaments/"
;;           :fields (list '("id" 4 t)
;;                         '("league_id" 8 t)
;;                         '("name" 50 t)
;;                         '("slug" 15 t)
;;                         '("opened" 20 t)
;;                         '("scheduled" 20 t)
;;                         '("started" 20 t)
;;                         '("qualifying_end" 20 t)
;;                         '("ended" 20 t)
;;                         '("color" 20 t)
;;                         '("cover" 20 t)
;;                         '("length" 20 t)
;;                         '("final_length" 20 t))
;;           :hidden (list "opened"
;;                         "scheduled"
;;                         "qualifying_end"
;;                         "length"
;;                         "final_length"))))
;;   (drunkenfall-table-compose-fields e))
