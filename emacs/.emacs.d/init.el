;;;* Unclutter the interface immediately

;; We want to do this right away as to reduce as much of the clutter as possible
(when (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(when (fboundp 'blink-cursor-mode) (blink-cursor-mode -1))
(when (fboundp 'mouse-wheel-mode) (mouse-wheel-mode 1))

;;;* Disable the hangers

;; These keys will hang Emacs by suspending. This is a bad idea when Emacs is
;; your window manager.
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-x C-z"))

;;;* Initialize straight

(require 'cl)
(require 'url-handlers)
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(straight-use-package 'use-package)

(setq straight-use-package-by-default t)
(setq use-package-version 'straight)

(defmacro use-feature (name &rest args)
  "Like `use-package', but with `straight-use-package-by-default' disabled."
  (declare (indent defun))
  `(use-package ,name
     :straight nil
     ,@args))

;;;* Load custom

(setq custom-file "~/.emacs.d/custom.el")
(unless (file-exists-p custom-file)
  (with-temp-buffer (write-file custom-file)))
(load custom-file)

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;;* Emacs core settings
;;;* Core

;; Libraries that are simply useful always
(use-package no-littering)
(use-package cl-lib)
(use-package dash)
(use-package f)
(use-package s)
(use-package hydra)
(use-package ivy)
(use-package autothemer)
(use-package tramp)
(use-package diminish)
(use-package blackout
  :straight (:host github :repo "raxod502/blackout"))

(use-package pretty-hydra
  :straight (:host github :repo "jerrypnz/major-mode-hydra.el"))

(use-package major-mode-hydra
  :straight (:host github :repo "jerrypnz/major-mode-hydra.el")
  :after hydra
  :preface
  (defun with-faicon (icon str &optional height v-adjust)
    "Displays an icon from Font Awesome icon."
    (s-concat (all-the-icons-faicon icon :v-adjust (or v-adjust 0) :height (or height 1)) " " str)))


(setq inhibit-startup-screen t)
(setq initial-scratch-message ";; *scratch*\n\n")
(setq scroll-step 10)
(setq scroll-conservatively 100)
(setq debug-on-error nil)
(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 1024 1024)) ; 1mb
(setq-default bidi-display-reordering nil)

(fset 'yes-or-no-p 'y-or-n-p)

;; I always thought this was the other way around :'(
(set-default 'truncate-lines t)

(setenv "GOPATH" "$HOME" t)
(setenv "GO111MODULE" "on")
(setenv "JAVA_HOME" "/usr/lib/jvm/java-8-jdk")

(setq th/custom-paths
      (-map 'substitute-env-vars
            '("$HOME/.local/bin"
              "$HOME/.local/share/infect/util"
              "$GOPATH/bin")))

;; Set new PATH elements that won't be there otherwise
(unless (s-contains? ".local/" (getenv "PATH"))
  (setenv "PATH"
          (concat
           (s-join ":" th/custom-paths)
           ":"
           (getenv "PATH")))
  (setq exec-path (append th/custom-paths exec-path)))

;;;* Browser

(setq browse-url-browser-function 'browse-url-generic)
(setq browse-url-generic-program "firefox")
(defvar th/browser "firefox")
(defvar th/browser-name th/browser)
(defvar th/browser-session th/browser)
(progn
  (defvar
    th/browser-sessions
    '(;; DrunkenFall
      ("drunkenfall" . ("https://dev.drunkenfall.com"))
      ("notion"      . ("https://notion.so"))

      ;; Hacking
      ("dotfiles"    . ("https://gitlab.com/thiderman/dotfiles"))
      ("syncthing"   . ("http://localhost:8384" "https://drunkenfall.com:8384"))

      ;; Messaging
      ("messenger"   . ("https://messenger.com"))
      ("riot"        . ("https://g33k.se/chat/#/room/#thosepeople:g33k.se"))
      ("discord"     . ("https://discordapp.com/channels/509473908534018049/518135218821922836"))
      ("facebook"    . ("https://facebook.com"))

      ;; IRL
      ("banking"     . ("https://internetbanken.privat.nordea.se/nsp/login"))
      ("commute"     . ("https://sl.se/#/Travel/SearchTravelById/Terrassen%20(Stockholm)/Liljeholmen%20(Stockholm)/1535/9294/null/depart/sv/null/null/2,8,1,4,96,/null/null/null/null/null/false/null/0/0/null/false/0/,0/0/,0/normal"))

      ;; Work
      ("mail"        . ("https://mail.google.com"))
      ("calendar"    . ("https://calendar.google.com"))
      ("slack"       . ("https://tink.slack.com"))
      ("github"      . ("https://github.com/notifications"))
      ("index"       . ("https://index.tink.network"))
      ("jira"        . ("https://tinkab.atlassian.net/jira/software/projects/STAT/boards/189"))
      ("grafana"     . ("https://grafana.global-production.tink.network/"))
      ("prometheus"  . ("https://prometheus-federation.global-production.tink.network/"))
      ("book"        . ("http://localhost:1313"))
      ("aws"         . ("https://accounts.google.com/o/saml2/initsso?idpid=C048pmkj7&spid=679200740039&forceauthn=false"))))

  (when (string= (system-name) "dragonisle")
    ;; ( ͡° ͜ʖ ͡°)
    (add-to-list 'th/browser-sessions '("xxx" . ("https://reddit.com/me/m/nsfw")))))

(pretty-hydra-define th/browser-hydra
  (:color teal :quit-key "q" :title (with-faicon "firefox" "Firefox"))
  ("Default"
   (("s-q" (browser th/ew/current) th/ew/current)
    ("s-SPC" browser-start-session "Session select"))
   "Messaging"
   (("s-m" (browser "messenger") "Messenger")
    ("s-s" (browser "slack") "Slack")
    ("s-f" (browser "facebook") "Facebook"))
   "Applications"
   (("s-d" th/discord "Discord")
    ("s-r" th/steam "Steam"))
   "DrunkenFall"
   (("s-n" (browser "notion") "Notion"))
   "Work"
   (("s-c" (browser "calendar") "Calendar")
    ("s-t" (browser "mail") "Mail")
    ("s-g" (browser "github") "Github")
    ("s-b" (browser "book") "Book")
    ("s-i" (browser "index") "Index")
    ("s-j" (browser "jira") "Jira")
    ("s-a" (browser "aws") "AWS"))))

(defun th/browser-session-urls (session)
  ;; Helper function to get the URLs from the data structure above given a
  ;; session name.
  (rest (-first (lambda (x) (string-equal (first x) session))
                th/browser-sessions)))

(defun browser (session &optional urls)
  "Go to a browser, or launch its session"
  (let* ((name (format "%s<%s>"
                       th/browser-name
                       ;; If we've given a prefix argument to any calling
                       ;; function, make this an alternate window with the same
                       ;; session data.
                       (concat session (and current-prefix-arg "-alt"))))
         (urls (or urls (th/browser-session-urls session)))
         ;; The --new-window is to make sure that we open a new window if there
         ;; is only one target. Otherwise, the browser will re-use an existing
         ;; window.
         ;; TODO(thiderman): Fix the spawning so that multiple URLs end up in
         ;; the same new window.
         ;; Currently, the new window will spawn with the first URL and then all
         ;; the rest of them will spawn in whichever _other_ window you focused last.
         (cmd (format "%s --new-window %s"
                      th/browser
                      (s-join " " (mapcar 'shell-quote-argument urls)))))
    (if (get-buffer name)
        ;; The browser already exists - switch to it
        (progn
          (switch-to-buffer name)
          (message "Switched to %s" session))
      ;; Set this global one so that exwm can set the title to this one
      ;; accordingly.
      (setq th/browser-session name)
      (start-process-shell-command th/browser nil cmd)
      (message "Started session for %s (%s)" session cmd))))

(defun browser-start-session ()
  "Select a session to start."
  (interactive)
  (let* ((sessions (sort (mapcar 'first th/browser-sessions) #'string<))
         (session (ivy-read "session: " sessions)))
    (browser session (th/browser-session-urls session))))

;;;* Themes

(require 'linum)
(global-linum-mode 0)
(setq linum-format " %4d ")

;; These are needed before we do the colorscheme loading
(use-package rainbow-mode
  :config
  (add-hook 'css-mode-hook 'rainbow-mode)
  (add-hook 'scss-mode-hook 'rainbow-mode))
(use-package rainbow-delimiters)
(use-package rainbow-identifiers)

(defun th/boldly-go ()
  "Automatic bolding in all colorschemes"

  (let ((faces (face-list)))
    (mapc
     (lambda (face)
       (set-face-attribute face nil ':bold t))
     (-filter
      (lambda (face)
        (let ((fn (symbol-name face)))
          (or (s-starts-with? "font-lock" fn)
              (s-starts-with? "org-level" fn)
              (s-starts-with? "rainbow" fn))))
      faces))

    (mapc
     (lambda (face)
       (set-face-attribute face nil ':inverse-video t))
     (-filter
      (lambda (face)
        (let ((fn (symbol-name face)))
          (s-starts-with? "avy-lead" fn)))
      faces))))

(defadvice load-theme (after load-theme-auto-bold activate)
  (th/boldly-go))

(require 'nightmare-theme)
;(load-theme 'nightmare)

;; all-the-icons - fantastic icons package <3
(use-package all-the-icons
  :load-path "/home/thiderman/src/github.com/domtronn/all-the-icons.el/"
  :config
  (setq all-the-icons-scale-factor 1.0)
  (use-package all-the-icons-dired
    :diminish all-the-icons-dired-mode
    :after (dired)
    :config)

  (use-package all-the-icons-ivy
    :config
    (all-the-icons-ivy-setup))

  (defun with-faicon (icon str &optional height v-adjust)
    (s-concat (all-the-icons-faicon icon :v-adjust (or v-adjust 0) :height (or height 1)) " " str))

  (defun with-fileicon (icon str &optional height v-adjust)
    (s-concat (all-the-icons-fileicon icon :v-adjust (or v-adjust 0) :height (or height 1)) " " str))

  (defun with-octicon (icon str &optional height v-adjust)
    (s-concat (all-the-icons-octicon icon :v-adjust (or v-adjust 0) :height (or height 1)) " " str))

  (defun with-material (icon str &optional height v-adjust)
    (s-concat (all-the-icons-material icon :v-adjust (or v-adjust 0) :height (or height 1)) " " str)))

(setq custom-safe-themes t)

;;;* Git

(setq vc-handled-backends '(Git))

(use-package gitconfig-mode)
(use-package gitignore-mode)
(use-package git-commit
  :init
  (setq git-commit-summary-max-length 79))

;; Move back and forth between commits <3
(use-package git-timemachine)

(use-package browse-at-remote
  :init
  (setq browse-at-remote-remote-type-domains
        '(("bitbucket.org" . "bitbucket")
          ("github.com" . "github")
          ("gh" . "github")
          ("gitlab.com" . "gitlab")
          ("git.savannah.gnu.org" . "gnu")
          ("gist.github.com" . "gist")
          ("git.sr.ht" . "sourcehut")
          ("pagure.io" . "pagure")
          ("src.fedoraproject.org" . "pagure"))))

;; <3 <3 <3
(use-package magit
  :bind (("C-x g" . magit-status))
  :commands (magit-read-repository)
  :init
  (setq magit-no-confirm '(reverse stage-all-changes))
  (setq ediff-window-setup-function 'ediff-setup-windows-plain)
  (setq magit-save-repository-buffers 'dontask)
  (setq magit-repository-directories '(("~/src/" . 3)))

  (setq magit-clone-default-directory #'clone-url-to-dir)
  (setq magit-clone-always-transient nil)
  (setq magit-clone-set-remote.pushDefault t)

  ;; https://emacs.stackexchange.com/questions/26266/projectile-and-magit-branch-checkout
  (defun run-projectile-invalidate-cache (&rest _args)
    (projectile-invalidate-cache nil))
  (advice-add 'magit-checkout :after #'run-projectile-invalidate-cache)
  (advice-add 'magit-branch-and-checkout :after #'run-projectile-invalidate-cache)

  (defadvice magit-status-setup-buffer (around magit-fullscreen activate)
    (magit-save-repository-buffers t)
    (window-configuration-to-register :magit-fullscreen)
    ad-do-it
    (delete-other-windows))

  (defadvice magit-mode-bury-buffer (after magit-restore-screen activate)
    "Restores the previous window configuration and kills the magit buffer"
    (when (eq major-mode 'magit-status-mode)
      (jump-to-register :magit-fullscreen)))

  :hook
  (magit-credential . th/auto-add-ssh-key))

(use-package magit-section)

;; Disabled: https://github.com/emacscollective/closql/issues/3
;; (use-package forge
;;   :after magit)

;;;###autoload
(defun th/auto-add-ssh-key ()
  "Try to add an ssh-key based on the remote"
  ;; Uses when-let so that it aborts if either the remote is nil or if no
  ;; matching key can be found.
  (when-let* ((disable-this nil)
              (url (car (magit-config-get-from-cached-list "remote.origin.url")))
              (key (cl-remove-if-not
                    (lambda (s) (s-contains? s url))
                    (mapcar
                     (lambda (s) (f-base s))
                     (cl-remove-if-not
                      (lambda (s) (s-suffix? ".rsa" s))
                      (directory-files "~/.ssh/"))))))
    (ssh-agent-add-key
     (concat (getenv "HOME")
             (format "/.ssh/%s.rsa" (car key))))))

(use-package git-gutter-fringe+
  :diminish (git-gutter+-mode " gut")
  :config
  (add-hook 'prog-mode-hook 'git-gutter+-mode))

(defhydra th/git-hydra (:columns 3)
  "git"
  ("n" git-gutter+-next-hunk "next")
  ("p" git-gutter+-previous-hunk "prev")
  ("s" git-gutter+-stage-hunks "stage")
  ("r" git-gutter+-revert-hunk "revert")
  ("C-r" browse-at-remote "at remote")
  ("g" magit-status "magit" :exit t)
  ("b" magit-blame "blame" :exit t)
  ("c" magit-clone "clone" :exit t)
  ("d" ediff-buffers "ediff" :exit t)   ; Technically not git, but hey
  ("l" magit-log-buffer-file "log" :exit t)
  ("m" git-gutter+-mode "gutter-mode")
  ("t" git-timemachine "timemachine" :exit t)
  ("s" th/smerge-hydra/body "smerge" :exit t)
  ("RET" th/goto-repo "repo" :exit t)
  ("SPC" git-gutter+-show-hunk-inline-at-point "show")
  ("q" nil))

(global-set-key (kbd "C-x C-g") 'th/git-hydra/body)

(defun clone-url-to-dir (url)
  "Turns a git clone URL into a path on the GOPATH

The repo name itself is removed since git will create that for you.

Supported formats:
 - Full URLs (https://github.com/org/repo.git)
 - SSH URLs (git@gitlab.com:org/repo)
 - SSH shorthands (github:org/repo, gh:org/repo, gl:org/repo)"
  (let* ((clean (s-replace
                 ":" "/"
                 (s-chop-prefixes
                  '("http://" "https://" "git@")
                  (s-chop-suffix ".git" url))))
         (host (first (s-split "/" clean)))
         (dir (cond
               ;; github specifics
               ((or (s-equals? host "github")
                    (s-equals? host "gh"))
                (s-replace host "github.com" clean))
               ;; gitlab specifics
               ((or (s-equals? host "gitlab")
                    (s-equals? host "gl"))
                (s-replace host "gitlab.com" clean))
               ;; Otherwise just do it
               (t
                clean))))
    (f-dirname
     (f-join (getenv "GOPATH") "src" dir))))

(defhydra th/smerge-hydra (:foreign-keys warn
                           :pre (smerge-mode 1))
  "smerge"
  ("n" smerge-next "next")
  ("p" smerge-prev "prev")
  ("a" smerge-keep-all "all")
  ("c" smerge-keep-current "current")
  ("RET" smerge-keep-current "current")
  ("o" smerge-keep-other "other")
  ("DEL" smerge-keep-other "other")
  ("m" smerge-keep-mine "mine")
  ("SPC" smerge-keep-mine "mine")
  ("b" smerge-keep-base "base")
  ("q" (smerge-mode -1) :exit t))

;; TODO(thiderman) This would be super nice to have activate immediately when
;; you enter a buffer that has smerge!
;; (add-hook 'smerge-mode-hook 'th/smerge-hydra/body)

(defun th/get-repo-url ()
  "Browse the current repo in the browser

Assumes that the repo lives on a $GOPATH-like path where the path
after $GOPATH is a canonical URL."
  (interactive)
  (s-replace (format "%s/src/" (getenv "GOPATH"))
             "https://"
             (projectile-project-root)))

(defun th/goto-repo ()
  "Browse the current repo in the browser"
  (interactive)
  ;; TODO(thiderman): Adapt for other things than github
  (let* ((url (concat (th/get-repo-url) "pulls"))
         (split (s-split "/" url))
         (slug (s-join "/" (list
                            (nth 3 split)
                            (nth 4 split)))))
    (browser slug (list url))))

;;;* Window management

(use-package ace-window
  :bind
  ("C-x o"   . ace-select-window)
  ("C-x C-o" . ace-swap-window)

  :config
  (setq aw-keys '(?a ?s ?d ?f ?q ?w ?e ?r ?t ?g ?h ?j ?k ?l ?y ?u ?i ?o)))

(winner-mode 1)
(bind-key "M-s-n" 'winner-undo)
(bind-key "M-s-p" 'winner-redo)

(use-package shackle
  :config
  (setq shackle-default-rule '(:other t :inhibit-window-quit t :noframe t))
  (setq shackle-rules '((".*transient.*" :regexp t :align t)))
  (shackle-mode 1))

(use-package buffer-move)

;; Try to make emax split vertically when possible
(setq split-height-threshold 100)
(setq split-width-threshold 160)

;; So that `compile' and other commands re-use already open buffers in other
;; frames. Really useful when using dual monitors.
(setq display-buffer-reuse-frames t)

(defun th/balance-windows ()
  "Balance windows, but only if there are no X windows close by"
  (interactive)
  (when (not (frame-parameter nil 'th/prohibit-balance))
    (balance-windows)))

(defun th/toggle-prohibit-balance ()
  "Toggles if balance prohibiting is on or off"
  (interactive)
  (let ((enabled (not (frame-parameter nil 'th/prohibit-balance))))
    (set-frame-parameter nil 'th/prohibit-balance enabled)

    ;; When we are resetting back, balance the windows immediately
    (when (not enabled)
      (balance-windows))

    (message (if enabled "Prohibit enabled" "Prohibit disabled"))))

(defun th/split-move-p ()
  "Returns boolean if we should move automatically when doing a split"
  ;; Avoid doing it in git commit buffers, because otherwise we end up in the diff
  ;; section when writing commit messages et.c.

  (and (not (s-contains? "git" (symbol-name major-mode)))
       (not (s-suffix? "COMMIT_EDITMSG" (buffer-file-name)))))

(defadvice split-window-right (after split-window-right-move activate)
  "Move to the right whenever splitting right."
  (when (th/split-move-p)
    (windmove-right))
  (th/balance-windows))

(defadvice split-window-below (after split-window-below-move activate)
  "Move to down whenever splitting below."
  (when (th/split-move-p)
    (windmove-down))
  (th/balance-windows))

(defadvice customize-group (before customize-group-split-window activate)
  "Makes `customize-group' appear in its own window."
  (split-window-sensibly)
  (other-window 1))

(defadvice split-window-sensibly (after split-window-sensibly-autobalance activate)
  (th/balance-windows))

(defadvice delete-window (after delete-window-autobalance activate)
  (th/balance-windows))

(defun split-window-sensibly (&optional window)
  "A split-window-sensibly that's actually sensible.

If there is room for a window (80 chars) to the side, split to
there. Otherwise split downwards.

Checks `window-splittable-p' as well so that windows that already have
a fixed size can retain that fixed size. `magit-popup' is a common use
case for this."
  (let ((window (or window (selected-window))))
    (if (or (and (or (s-contains? "magit" (symbol-name major-mode))
                     (s-contains? "kubernetes" (symbol-name major-mode))
                     (s-contains? "docker" (symbol-name major-mode)))
                 (not (window-splittable-p window t)))
            (not (>= (window-width window) 120))
            (frame-parameter nil 'th/prohibit-balance))
        (split-window-vertically)
      (split-window-horizontally))))

;;;* Interface
;;;;* Hydra

;; (use-package posframe)

;; (use-package hydra-posframe
;;   :straight (:host github :repo "Ladicle/hydra-posframe")
;;   :hook (after-init . hydra-posframe-enable)
;;   :config
;;   (setq hydra-posframe-border-width 3)
;;   (setq hydra-posframe-border-face "gray100")
;;   (setq hydra-posframe-parameters '((internal-border-width . 5)
;;                                     (parent-frame . nil))))


;;;;* Others

(use-package avy
  :commands avy-goto-word-1
  :bind
  ("C-l"     . avy-goto-word-1))

;; dumb-jump - IDE-like goto that just uses grep tools and context
(use-package dumb-jump
  :bind (("M-." . dumb-jump-go)
         ("C-c C-." . dumb-jump-go)
         ("C-x ." . th/dumb-jump-hydra/body)
         ("C-t" . th/dumb-jump-go)
         ("C-<backspace>" . dumb-jump-back)
         ("C-c C-t" . th/dumb-jump-hydra/body))
  :config
  (setq dumb-jump-selector 'ivy)
  (setq dumb-jump-prefer-searcher 'ag)

  (defhydra th/dumb-jump-hydra (:exit t :foreign-keys warn)
    "Dumb Jump"
    ("." dumb-jump-go "Go")
    ("C-t" dumb-jump-go "Go")
    ("b" dumb-jump-back "Back" :exit nil)
    ("l" dumb-jump-quick-look "Look" :exit nil)
    ("e" dumb-jump-go-prefer-external "External")
    ("w" dumb-jump-go-other-window "Window")
    ("q" nil)))

(defun th/dumb-jump-go (p)
  "Dumb jump, or split if given the universal argument"
  (interactive "p")
  (if (= p 4)
      (dumb-jump-go-other-window)
    (dumb-jump-go))
  (recenter))

;; projectile - project management <3
(use-package projectile
  :diminish projectile-mode
  :bind-keymap (("C-x p" . projectile-command-map))
  :commands (projectile-switch-project)
  :config
  (projectile-global-mode)
  (setq projectile-completion-system 'ivy)
  (setq projectile-enable-caching t)
  (setq projectile-files-cache-expire (* 24 60 60))
  (setq projectile-mode-line nil)
  )

;; ivy - better fuzzy selection
(use-package ivy
  :diminish ivy-mode
  :bind (("M-x"     . counsel-M-x)
         ("C-x y"   . counsel-yank-pop)
         ("C-c C-r" . ivy-resume))
  :commands (ivy-set-actions)
  :config
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-height 25)
  (setq magit-completing-read-function 'ivy-completing-read)

  ;; So that we can do space separation in file name completions
  (setq ivy-re-builders-alist
        '((t . ivy--regex-ignore-order)))

  (ivy-set-actions
   'projectile-find-file
   '(("z" (lambda (fn)
            (interactive)
            (split-window-below)
            (windmove-down)
            (find-file (expand-file-name fn (projectile-project-root))))
      "horz")
     ("s" (lambda (fn)
            (interactive)
            (split-window-right)
            (windmove-right)
            (find-file (expand-file-name fn (projectile-project-root))))
      "vert")))

  (ivy-set-actions
   'ivy-switch-buffer
   '(("z" (lambda (buf)
            (interactive)
            (split-window-below)
            (windmove-down)
            (switch-to-buffer buf))
      "horz")

     ("s" (lambda (buf)
            (interactive)
            (split-window-right)
            (windmove-right)
            (switch-to-buffer buf))
      "vert")

     ("d" (lambda (buf)
            (interactive)
            (kill-buffer buf)
            (message "Buffer %s killed" buf))
      "delete")
     ("k"
      (lambda (x)
        (kill-buffer x)
        (ivy--reset-state ivy-last))
      "kill")
     ("j"
      ivy--switch-buffer-other-window-action
      "other window")
     ("r"
      ivy--rename-buffer-action
      "rename")))

  (ivy-mode 1))

(use-package counsel
  :bind (("C-x C-f" . counsel-find-file)
         (:map ivy-minibuffer-map
               ("M-s" . ivy-yank-symbol)))
  :init
  ;; https://github.com/abo-abo/swiper/issues/685#issuecomment-249162962
  (setq counsel-find-file-ignore-regexp
        (concat
         ;; File names beginning with # or .
         "\\(?:\\`[#.]\\)"
         ;; File names ending with # or ~
         "\\|\\(?:\\`.+?[#~]\\'\\)"))
  (when (executable-find "rg")
    (setq counsel-grep-base-command
          "rg -i -M 120 --hidden --no-heading --line-number --color never '%s'")
    (setq counsel-git-grep-cmd "rg -i -M 120 --hidden --max-depth 20 --no-heading --line-number --color never '%s'")))

(defun th/search ()
  (interactive)
  (let* ((dir (or (projectile-project-p) default-directory))
         (opts "-i -M 120 --hidden --max-depth 20"))
    ;; If we're in a project - use projectile. Otherwise, just search the
    ;; current directory
    (if (projectile-project-p)
        (counsel-projectile-rg opts)
      (counsel-rg nil dir opts))))


(use-package counsel-projectile
  :after counsel)

(use-package ace-link
  :init
  (ace-link-setup-default))

(setq uniquify-buffer-name-style 'forward)

(defun th/goto-header ()
  "Find and focus on an outline header in a lisp file"
  (interactive)
  (counsel-grep-or-swiper "^;;+\\* ")
  (beginning-of-line)
  (outline-hide-body)
  (lispy-tab)
  (lispy-view))

(defun th/goto-hydra ()
  "Find and focus on a hydra in the current file"
  (interactive)
  (counsel-grep-or-swiper "hydra def ")
  (beginning-of-line)
  (outline-show-entry)
  (lispy-view))

(pretty-hydra-define th/search-hydra
  (:color teal :idle 0.5 :title (with-faicon "search" "Search"))
  ("Default"
   (("C-s" counsel-grep-or-swiper "Search")
    ("." (counsel-git-grep (thing-at-point 'symbol)) "Search at point")
    ("a" swiper-all "All buffers")
    ("g" th/search "git grep")
    ("s" counsel-grep-or-swiper "Search"))
   "Config"
   (("C-h" th/goto-hydra "Hydras")
    ("h" th/goto-header "Headers"))
   "emacs"
   (("i" counsel-imenu "imenu")
    ("k" counsel-descbinds "Keybindings"))))

(global-set-key (kbd "C-s") 'th/search-hydra/body)

(use-package fixme-mode
  :diminish fixme-mode
  :config
  (add-hook 'prog-mode-hook 'fixme-mode)
  (setq fixme-background-color "none")
  (setq fixme-highlighted-words
        '("FIXME" "TODO" "BUG" "FIX" "HACK" "REFACTOR" "NOCOMMIT" "XXX")))

(use-package helpful
  :init
  (defalias #'describe-key #'helpful-key)
  (defalias #'describe-function #'helpful-callable)
  (defalias #'describe-variable #'helpful-variable)
  (defalias #'describe-symbol #'helpful-symbol)
  (setq helpful-switch-buffer-function #'pop-to-buffer)
  :bind
  (("C-h f" . helpful-callable)
   ("C-h v" . helpful-variable)
   ("C-h k" . helpful-key)
   ("C-h C-d" . helpful-at-point)
   ("C-h C-x" . helpful-command)))

(defun th/buffer-switch (p)
  "Switch buffers - locally to project if we are in one

Universal arguments means always all buffers"
  (interactive "p")
  (if (and (projectile-project-p)
           (= p 1))
      (counsel-projectile-switch-to-buffer)
    (counsel-switch-buffer)))

;; http://endlessparentheses.com/emacs-narrow-or-widen-dwim.html
(defun th/narrow-or-widen-dwim (p)
  "Widen if buffer is narrowed, narrow-dwim otherwise.
Dwim means: region, org-src-block, org-subtree, or
defun, whichever applies first. Narrowing to
org-src-block actually calls `org-edit-src-code'.

With prefix P, don't widen, just narrow even if buffer
is already narrowed."
  (interactive "P")
  (declare (interactive-only))
  (cond ((and (buffer-narrowed-p) (not p)) (widen))
        ((region-active-p)
         (narrow-to-region (region-beginning)
                           (region-end)))
        ((derived-mode-p 'org-mode)
         ;; `org-edit-src-code' is not a real narrowing
         ;; command. Remove this first conditional if
         ;; you don't want it.
         (cond ((ignore-errors (org-edit-src-code) t)
                (delete-other-windows))
               ((ignore-errors (org-narrow-to-block) t))
               (t (org-narrow-to-subtree))))
        (t (narrow-to-defun))))

;; This line actually replaces Emacs' entire narrowing
;; keymap, that's how much I like this command. Only
;; copy it if that's what you want.
(bind-key "C-x n" #'th/narrow-or-widen-dwim)

(global-set-key (kbd "C-x <C-return>") 'browse-url)

;; Indicators on the edges of the screen
(fringe-mode 12)
(setq-default indicate-empty-lines t)
(setq-default cursor-in-non-selected-windows nil)

(setq enable-recursive-minibuffers t)

;; Set cursor things
(blink-cursor-mode 1)
(setq blink-cursor-interval 0.7)
(setq-default cursor-type 'bar)

(setq echo-keystrokes 0.2)
(setq whitespace-style '(face trailing tabs newline empty indentation
                              space-before-tab tab-mark))

(defmacro th/in-all-buffers (&rest args)
  "Run something inside of all buffers.

Useful to set buffer-local variables globally or something of the
sort.

The current buffer is bound to `buffer' and can be used in the
body."
  (declare (indent defun))
  `(dolist (buffer (buffer-list))
     (with-current-buffer buffer
       ,@args)))

(use-package command-log-mode)

;;;* Modeline

(defun th/modeline-icon ()
  (let ((icon (all-the-icons-icon-for-buffer)))
    (unless (symbolp icon) ;; This implies it's the major mode
      (format " %s"
              (propertize icon
                          'help-echo (format "Major-mode: `%s`" major-mode)
                          'face `(:height 1.2 :family ,(all-the-icons-icon-family-for-buffer)))))))

(defun th/tracking-status ()
  "Return the current track status.

This returns a list suitable for `mode-line-format'.

Modified version that does not do any properties."
  (if (not tracking-buffers)
      ""
    (let* ((buffer-names (cl-remove-if-not #'get-buffer tracking-buffers))
           (shortened-names (tracking-shorten tracking-buffers))
           (result (list " [")))
      (while buffer-names
        (push (car shortened-names)
              result)
        (setq buffer-names (cdr buffer-names)
              shortened-names (cdr shortened-names))
        (when buffer-names
          (push "," result)))
      (push "] " result)
      (nreverse result))))

(use-package telephone-line
  :init
  (telephone-line-defsegment th/telephone-buffer-segment ()
    (list
     (when (buffer-file-name)
       (if buffer-read-only
           (all-the-icons-faicon "ban"
                                 :face 'font-lock-keyword-face
                                 :v-adjust -0.01)
         (when (buffer-modified-p)
           (all-the-icons-faicon "floppy-o"
                                 :face 'error
                                 :v-adjust -0.01))))
     " "
     (let* ((bn (buffer-name)))
       ;; If we're looking at a mail inbox, show its query rather than the
       ;; buffer name.
       (if (string-equal bn "*mu4e-headers*")
           mu4e~headers-last-query
         bn))))

  (telephone-line-defsegment th/telephone-clock-segment ()
    (when (telephone-line-selected-window-active)
      ;; The org-clocking-p function isn't necessarily loaded until we start
      ;; actually clocking something.
      (if (and (functionp 'org-clocking-p) (org-clocking-p))
          (org-clock-get-clock-string)
        "")))

  (telephone-line-defsegment th/telephone-workspace ()
    (format "%s" (th/ew/name-for-exwm-workspace
                  exwm-workspace-current-index)))

  (telephone-line-defsegment th/telephone-email-segment ()
    (let ((alert mu4e-alert-mode-line))
      (when (and (telephone-line-selected-window-active) alert)
        (propertize (format "%sm" (substring-no-properties alert 7 -2))
                    'face 'font-lock-function-name-face))))

  (telephone-line-defsegment* th/vc-segment ()
    (when (not (file-remote-p (buffer-file-name)))
      (telephone-line-raw
       (replace-regexp-in-string "git." "" (substring-no-properties (if vc-mode vc-mode "")) t t) t)))

  (setq telephone-line-faces
        '((accent . (telephone-line-accent-active . telephone-line-accent-inactive))
          (track  . (isearch-fail . mode-line-inactive))
          (email  . (font-lock-constant . mode-line-inactive))
          (nil    . (mode-line . mode-line-inactive))))

  (setq telephone-line-lhs
        '((accent . (telephone-line-process-segment))
          (accent . (telephone-line-major-mode-segment))
          (nil    . (th/telephone-buffer-segment))))

  (setq telephone-line-rhs
        '((nil    . (th/telephone-clock-segment))
          (track  . (th/telephone-email-segment))
          (accent . (telephone-line-minor-mode-segment))
          (nil    . (telephone-line-airline-position-segment))))

  (require 'telephone-line-separators)
  (setq telephone-line-primary-left-separator telephone-line-utf-abs-left)
  (setq telephone-line-secondary-left-separator telephone-line-utf-abs-left)
  (setq telephone-line-primary-right-separator telephone-line-utf-abs-right)
  (setq telephone-line-secondary-right-separator telephone-line-utf-abs-right)

  :config
  (telephone-line-mode 1))

;;;* Backups

(defvar th/backup-directory (concat user-emacs-directory "backups"))

(if (not (file-exists-p th/backup-directory))
    (make-directory th/backup-directory t))

(setq backup-directory-alist `(("." . ,th/backup-directory)))

(setq make-backup-files nil)
(setq backup-by-copying t)
(setq version-control nil)
(setq delete-old-versions t)
(setq delete-by-moving-to-trash nil)
(setq create-lockfiles nil)
(setq kept-old-versions 1)
(setq kept-new-versions 1)

(setq-default auto-save-default nil)
(setq-default auto-save-timeout 5)
(setq-default auto-save-interval 300)

(setq global-auto-revert-non-file-buffers t)
(setq auto-revert-verbose nil)
(setq revert-without-query '("."))

(setq backup-inhibited t)
(setq make-backup-files nil)

;; Save all tempfiles in $TMPDIR/emacs-$UID/
(defconst th/emacs-tmp-dir
  (format "/tmp/emacs-%s/" (user-uid)))
(make-directory th/emacs-tmp-dir t)

(setq temporary-file-directory th/emacs-tmp-dir)
(setq auto-save-list-file-prefix th/emacs-tmp-dir)

;;;* Hooks

(add-hook 'emacs-lisp-mode-hook 'semantic-mode)
(add-hook 'go-mode-hook 'semantic-mode)
(add-hook 'python-mode-hook 'semantic-mode)

(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook (lambda ()
                            (auto-save-mode -1)))
(add-hook 'text-mode-hook 'auto-fill-mode)
(add-hook 'text-mode-hook (lambda ()
                            (setq fill-column 80)))
(add-hook 'prog-mode-hook 'auto-fill-mode)
;; (add-hook 'text-mode-hook 'fci-mode)

(add-hook 'prog-mode-hook 'abbrev-mode)
(add-hook 'text-mode-hook 'abbrev-mode)

;;;* Utilities

;; Major mode utilities - little to no config
(use-package csv-mode)
(use-package highlight-symbol
  :config (setq highlight-symbol-idle-delay 0))
(use-package logview)
(use-package markdown-mode
  :config (setq markdown-asymmetric-header t))
(use-package nginx-mode)
(use-package protobuf-mode)
(use-package ssh-config-mode)
(use-package terraform-mode)
(use-package yaml-mode)
(use-package systemd)
(use-package daemons)

;; minor mode utilities - little to no config
(use-package adaptive-wrap)
(use-package alert
  :commands (alert)
  :init
  (setq alert-default-style 'libnotify))
(use-package buffer-move)
(use-package define-word)
(use-package emojify
  :config
  (global-emojify-mode 1))
(use-package fill-column-indicator
  :config
  (setq fci-rule-color "#4b2d54")
  (setq-default fill-column 80)
  (setq-default comment-fill-column 80))
(use-package paredit
  :diminish paredit-mode)
(use-package restclient
  :mode ("\\.http\\'" . restclient-mode))
(use-package transpose-frame)
(use-package wrap-region
  :diminish wrap-region-mode
  :hook (prog-mode . wrap-region-mode))
(use-package xkcd)

;; builtins
(require 'semantic)
(setq-default global-semantic-idle-scheduler-mode nil)
(setq-default global-semanticdb-minor-mode nil)
(semantic-mode -1)

(eval-after-load 'semantic
  (add-hook 'semantic-mode-hook
            (lambda ()
              (dolist (x (default-value 'completion-at-point-functions))
                (when (string-prefix-p "semantic-" (symbol-name x))
                  (remove-hook 'completion-at-point-functions x))))))

(setq sh-indentation 4)
(setq vc-follow-symlinks t)

(use-package edit-server
  :init
  (add-hook 'after-init-hook 'server-start t)
  (add-hook 'after-init-hook 'edit-server-start t))

(setq tramp-default-method "ssh")
(add-to-list 'tramp-remote-path 'tramp-own-remote-path)
(add-to-list 'tramp-remote-path "/home/thiderman/bin")

;; Simple functions that have nowhere else to live
(defun th/unixtime (arg)
  "Prints a date of the unixtime under point, with a hint of how long ago it was.

If given the universal argument, the current unixtime is inserted
at point."
  (interactive "p")

  (let ((now (s-trim (shell-command-to-string "date +%s"))))
    (if (= arg 4)
        (insert now)
      ;; The 0 10 part is to cut away milliseconds. It's far from elegant, but
      ;; it does the thing.
      (let* ((thing (substring-no-properties (symbol-name (symbol-at-point)) 0 10))
             (out (s-trim (shell-command-to-string (format "date -d @%s" thing))))
             (diff (- (string-to-number now)
                      (string-to-number thing))))
        ;; TODO(thiderman): Handle timestamps from the future
        (message "%s (%ss ago)" out diff)))))

(global-set-key (kbd "C-x t") 'th/unixtime)

;;;* Toggle

(defun th/kill-file-name (p)
  "Adds the current file name to the kill ring, with the current project prefix removed.

Adding universal argument will display the full path regardless."

  (interactive "P")
  (if-let ((fn (buffer-file-name)))
      (kill-new
       (message
        (if (and (projectile-project-p)
                 (not p))
            (s-chop-prefix (projectile-project-root) fn)
          fn)))
    (user-error "Buffer '%s' is not visiting a file" (buffer-name))))

(define-key global-map (kbd "C-x ?") 'th/kill-file-name)

(defun th/switch-to-previous-buffer ()
  "Switch to previously open buffer.
Repeated invocations toggle between the two most recently open buffers."
  (interactive)
  (cond
   ((string-equal (buffer-name) "Spotify")
    (switch-to-buffer th/browser-name))
   ((string-equal (buffer-name) th/browser)
    (if (get-buffer "Spotify")
        (switch-to-buffer "Spotify")
      (exwm-execute "spotify")))
   (t
    (switch-to-buffer (other-buffer (current-buffer) 1)))))

(defmacro th/mode-menu--body (mode name)
  `(let ((buffers
         (--map
          (buffer-name it)
          (--filter
           (equal ',mode (with-current-buffer it major-mode))
           (buffer-list)))))

    (cond
     ((= (length buffers) 1)
      (switch-to-buffer (car buffers))
      (message "Visiting only open %s buffer" ,name))

     ((> (length buffers) 1)
      (switch-to-buffer
       (completing-read (format "%s buffers: " ,name) buffers)))

     (t
      (message "There are no %s buffers open right now" ,name)))))

(defmacro th/mode-menu (mode)
  (let* ((name (symbol-name mode))
         (defun-name (make-symbol (format "%s-select-buffer" name))))

    `(defun ,defun-name ()
       (interactive)
       (th/mode-menu--body ,mode ,name))))

(global-set-key (kbd "C-x M-d") (th/mode-menu dired-mode))
(global-set-key (kbd "C-x M-c") (th/mode-menu compilation-mode))
(global-set-key (kbd "C-x M-b") 'ibuffer)

(defun th/goto-scratch ()
    (interactive)
    (let ((content initial-scratch-message)
          (buf "*scratch*"))
      (when (get-buffer buf)
        (setq content ""))
      (switch-to-buffer buf)
      (insert content)))

(define-key global-map (kbd "<f4>") #'th/goto-scratch)

(defun th/buffer-or-back (name)
  "Switch to the named buffer, or go back if we are already are visiting it."
  (interactive)
  (switch-to-buffer
   (if (string-equal (buffer-name) name)
       (other-buffer (current-buffer) 1)
     name)))
;;;* File handlers

(defun th/cycle-files-in-dir (&optional step)
  "Go STEP steps in the current directory listing.

Given a step of 1 (the default), will go to the next file. -1 means previous file. Other
numbers can apply, but these two are the ones that will make the most sense.

Loops around the directory boundaries, so a -1 on the first file will go to the last."
  (when (not buffer-file-name)
    (user-error "Not visiting a file - cannot cycle"))
  (let* ((arg (or step 1))
         (file (f-filename buffer-file-name))
         (files (-filter #'f-file?
                         (directory-files default-directory)))
         (index (-elem-index file files))
         (new-index (mod (+ arg index) (length files))))
    (find-file (nth new-index files))
    (th/cycle-files-print files new-index)))

(defvar th/cycle-files-surround 2
  "The amount of files to print around the current one")

(defun th/cycle-files-print (files index)
  "Prints a status list of the surrounding files we are visiting"
  (let* ((lower (- index th/cycle-files-surround))
         (upper (+ index th/cycle-files-surround))
         (indexes (cl-loop for x below (length files)
                           if (and (>= x lower)
                                   (<= x upper))
                           collect x)))
    (message (s-join (propertize " / " 'face 'font-lock-comment-delimiter-face)
                     (-map
                      (lambda (x)
                        (concat
                         (when (= x 0)
                           (propertize "| " 'face 'font-lock-comment-face))
                         (propertize (nth x files) 'face (when (= x index)
                                                           'font-lock-keyword-face))
                         (when (= x (- (length files) 1))
                           (propertize " |" 'face 'font-lock-comment-face))))
                      indexes)))))

(defun th/cycle-next ()
  (interactive)
  (th/cycle-files-in-dir 1))

(defun th/cycle-prev ()
  (interactive)
  (th/cycle-files-in-dir -1))

;;;* ssh

(require 'ssh-agent)
(use-package ssh-tunnels
  :config
  (setq ssh-tunnels-configurations
      '((:name "DrunkenFall db"
          :local-port 35432
          :remote-port 5432
          :host "localhost"
          :login "df")
        (:name "Undercover db"
          :local-port 45432
          :remote-port 5532
          :host "localhost"
          :login "df"))))

;;* emacs extensions
;;;* Company

(use-package company
  :diminish company-mode
  :config
  ;; Let company do its thing as often as possible.
  (global-company-mode t)

  (setq company-tooltip-limit 20)       ; bigger popup window
  (setq company-idle-delay .3)
  (setq company-minimum-prefix-length 1)
  (setq company-echo-delay 0)  ; remove annoying blinking
  (setq company-begin-commands '(self-insert-command)) ; start autocompletion only after typing

  ;; Why did anyone think that we wanted to downcase and ignore cases? What?
  (setq company-dabbrev-downcase nil)
  (setq company-dabbrev-ignore-case nil)

  ;; https://oremacs.com/2017/12/27/company-numbers/
  (setq company-show-numbers t)
  (let ((map company-active-map))
    (mapc
     (lambda (x)
       (define-key map (format "%d" x) 'ora-company-number))
     (number-sequence 0 9))
    (define-key map " " (lambda ()
                          (interactive)
                          (company-abort)
                          (self-insert-command 1)))
    (define-key map (kbd "<return>") nil)))

(defun ora-company-number ()
  "Forward to `company-complete-number'.

Unless the number is potentially part of the candidate.
In that case, insert the number."
  (interactive)
  (let* ((k (this-command-keys))
         (re (concat "^" company-prefix k)))
    (if (cl-find-if (lambda (s) (string-match re s))
                    company-candidates)
        (self-insert-command 1)
      (company-complete-number (string-to-number k)))))

(use-package company-flx
    :config
    (company-flx-mode 1))

(use-package company-quickhelp
  :after company
  :config
  (company-quickhelp-mode 1))

;;;* Dired

(use-feature dired
  :init
  (setq-default dired-hide-details-mode t)
  :config
  (setq dired-clean-up-buffers-too t)
  (setq dired-clean-confirm-killing-deleted-buffers nil)
  (setq dired-hide-details-hide-information-lines t)
  (setq dired-listing-switches "-Ahlv --group-directories-first --time-style=long-iso")
  (setq dired-recursive-copies 'always)
  (setq dired-recursive-deletes 'always)
  (setq dired-dwim-target t)
  (setq dired-no-confirm '(move copy))
  (setq dired-guess-shell-alist-user
        '(("\\.mp.*\\'" "mpv")
          ("\\.avi\\'" "mpv")
          ("\\.pdf\\'" "zathura")))
  (put 'dired-find-alternate-file 'disabled nil)

  :bind
  (:map dired-mode-map
        ("c" . wdired-change-to-wdired-mode)
        ("r" . counsel-rg)
        ("f" . th/find-name-dired)
        ("/" . th/dired-goto-root)
        ("~" . th/dired-goto-home)
        ("_" . th/dired-goto-tmp)
        ("N" . th/dired-goto-nest)
        ("i" . dired-omit-mode)
        ("I" . all-the-icons-dired-mode)
        ("e" . th/eshell-dired)
        ("<left>" . dired-jump)
        ("<right>" . dired-find-file)
        ("h" . dired-jump)
        ("j" . dired-next-line)
        ("k" . dired-previous-line)
        ("l" . dired-find-file)
        ("," . dired-do-async-shell-command))
  :hook (dired-mode . dired-hide-details-mode))

(use-feature dired-x
  :after dired
  :init
  (setq-default dired-omit-files-p t)
  :config
  (setq dired-omit-files (concat dired-omit-files "\\|^\\..+$")) ; For dotfiles
  (setq dired-omit-verbose nil) ;; https://open.spotify.com/track/2XRl0NfORYPEvUJXLtJiND
  (setq dired-omit-mode t))

;;;###autoload
(defun th/find-name-dired (pattern)
  "Call `find-name-dired' in the current directory.

I dislike having to go through pointless dialogs. This is a perfect example."
  (interactive
   "sfind-name (filename wildcard): ")
  (find-name-dired default-directory pattern))

(use-package dired-narrow
  :bind (:map dired-mode-map
              ("C-c C-s" . dired-narrow-regexp)
              ("C-c C-f" . dired-narrow-fuzzy)))

(use-package wdired)

;; These three advices makes sure to disable icons in dired when renaming files.
;; Seems that the icons might confuse the renaming process otherwise.
(defadvice wdired-change-to-wdired-mode (before wdired-no-icons activate)
  (all-the-icons-dired-mode -1))

(defadvice wdired-finish-edit (after wdired-reset-icons-success activate)
  (all-the-icons-dired-mode 1))

(defadvice wdired-abort-changes (after wdired-reset-icons-abort activate)
  (all-the-icons-dired-mode 1))

(defun th/dired-goto-root ()
  "Shortcut to browse to root via dired"
  (interactive)
  (dired "/"))

(defun th/dired-goto-home ()
  "Shortcut to browse to $HOME via dired"
  (interactive)
  (dired "~"))

(defun th/dired-goto-tmp ()
  "Shortcut to browse to $HOME/tmp via dired"
  (interactive)
  (dired "~/tmp"))

(defun th/dired-goto-nest ()
  "Shortcut to browse to /mnt/nest via dired"
  (interactive)
  (dired "/mnt/nest"))

;; Override of the normal find-name-dired since I always want to
;; search in the current directory anyways.
;; (defun find-name-dired (pattern)
;;   (interactive "sdired find: ")
;;   (find-dired
;;    default-directory
;;    (concat find-name-arg " " (shell-quote-argument pattern))))

(defvar th/image-dirname "sorted")
(defvar th/image-last-target nil)

(defun th/image-sort ()
  "Sort the current image to somewhere else"
  (interactive)

  (when (not (derived-mode-p 'image-mode))
    (error "Not viewing an image"))

  (let* ((file (buffer-file-name))
         (dir (f-join default-directory th/image-dirname))
         ;; This is a bit hacky, but it creates the sorted dir if it doesn't yet exist. I was hoping
         ;; that `make-directory' would return the path it created, but sadly no.
         (_ (make-directory dir t))
         ;; targets is a list of all of the directories inside the sorted/ directory, suitable for completion
         (targets (-map
                   (lambda (d) (s-chop-prefix (concat dir "/") d))
                   (-filter
                    #'f-dir?
                    (directory-files-recursively dir "." t))))
         (target-dir (f-join dir
                             ;; This is for caching so we can suggest this suggestion as the default
                             ;; next time
                             (setq th/image-last-target
                                   (ivy-read "target: " targets
                                             :preselect th/image-last-target))))
         (target (th/maybe-rename-file target-dir file))
         ;; If there is more than two images in the current directory, then we know that we have
         ;; more work to do and can switch to the next image once we are done with this one.
         ;; Weirdly `image-mode--images-in-directory' returns both the absolute path /and/ the
         ;; relative path for the current file, so there's at least two items in the directory when
         ;; there are images in it. /shrug
         ;; TODO(thiderman): Maybe make something that just removes the absolute path version?
         (more-images (> (length (image-mode--images-in-directory file)) 2)))
    (make-directory target-dir t)
    (rename-file file target)
    ;; We go to the next image before we move it, because otherwise the buffer will move with it and
    ;; the next image would be something we did not intend.
    (if more-images
        ;; If there are more images, go to the next image and run this function again.
        (progn
          (image-next-file)
          (th/image-sort))
      ;; If there are no more images, start `dired' in the directory where the files landed.
      (dired dir)
      (message "No more images left"))))

(defun th/maybe-rename-file (target-dir filename)
  "Query for a new name for the current file.

The extension is preserved.
If no new name is given, the current filename is returned."
  (let* ((base (f-base filename))
         (ext (f-ext filename))
         (read (read-string (format "name (%s): " base)))
         ;; If we did not provide a new name, we keep the old one
         (new (if (string-equal read "")
                  base
                read)))
    (f-join target-dir
            (concat new "." ext))))

(use-feature image-mode
  :bind
  (:map image-mode-map
        ("s" . #'th/image-sort)))

(defun th/image-collect ()
  "Sort all images in the current directory into a directory called 'img/'"
  (interactive)
  (let* ((dir (f-join default-directory "img"))
         (rxp (concat
               "\\("
               (s-join "\\|" (-map #'car image-type-file-name-regexps))
               "\\)"))
         (images (directory-files default-directory t rxp)))
    (make-directory dir t)
    (dolist (img images)
      (rename-file img
                   (concat dir "/" (f-relative img))))))

;;;* Editing

(use-package hungry-delete
  :diminish hungry-delete-mode
  :config
  (global-hungry-delete-mode))

(use-package wgrep
  :config
  (setq wgrep-auto-save-buffer t)
  (setq wgrep-enable-key "e"))

(use-package undo-tree
  :commands (undo-tree-undo undo-tree-redo)
  :diminish undo-tree-mode
  :bind
  (("C-z"     . th/undo-if-allowed)
   ("C-M-z"   . undo-tree-redo)
   ("C-x u"   . undo-tree-visualize)
   ("C-x C-z"   . th/toggle-undo-allowed))

  :config
  (global-undo-tree-mode +1))

(defvar th/undo-allowed nil
  "If `C-z' undo is allowed or not.

Used to force behavior to use Keyboard.io binds of Fn-bn instead.")

;;;###autoload
(defun th/undo-if-allowed ()
  "Runs `undo-tree-undo' if `th/undo-allowed' is `t'.

Denies otherwise."
  (interactive)
  (if th/undo-allowed
      (undo-tree-undo)
    (message "C-z undo disabled.")))

;;;###autoload
(defun th/toggle-undo-allowed ()
  "Toggles the state of `th/undo-allowed'."
  (interactive)
  (message "th/undo-allowed set to `%s'"
           (setq th/undo-allowed (not th/undo-allowed))))


(defun insert-random-uuid ()
  "Insert a random UUID.
Example of a UUID: 1df63142-a513-c850-31a3-535fc3520c3d

WARNING: this is a simple implementation. The chance of
generating the same UUID is much higher than a robust
algorithm.."
  (interactive)
  (random t)
  (insert
   (format "%04x%04x-%04x-%04x-%04x-%06x%06x"
           (random (expt 16 4))
           (random (expt 16 4))
           (random (expt 16 4))
           (random (expt 16 4))
           (random (expt 16 4))
           (random (expt 16 6))
           (random (expt 16 6)))))

(bind-key "C-x M-u" #'insert-random-uuid)

(defun insert-ulid ()
  "Insert an ULID for the current timestamp"
  (interactive)
  (random t)
  (insert (s-trim
           (shell-command-to-string "ulid"))))

(bind-key "C-x C-u" #'insert-ulid)

(defvar th/clean-whitespace-p t "Cleaning whitespace or not")
(setq delete-trailing-lines t)

(defun th/clean-whitespace ()
  ;; This mostly annoys me when we are in org, so let's remove it.
  (when (and (not (derived-mode-p 'org-mode))
             th/clean-whitespace-p
             (not (looking-back " ")))
    (save-excursion
      (save-restriction
        (widen)
        (delete-trailing-whitespace)))))

(add-hook 'before-save-hook #'th/clean-whitespace)

(defun yank-with-indent ()
  "Yank, but keep the indent in the start of the line"
  ;; https://emacs.stackexchange.com/questions/31646/how-to-paste-with-indent
  (interactive)
  (let ((indent
         (buffer-substring-no-properties (line-beginning-position) (line-end-position))))
    (message indent)
    (yank)
    (save-excursion
      (save-restriction
        (narrow-to-region (mark t) (point))
        (pop-to-mark-command)
        (replace-string "\n" (concat "\n" indent))
        (widen)))))

(bind-key (kbd "C-c y") #'yank-with-indent)


(setq completion-styles '(initials basic partial-completion emacs22))

(defun th/insert-file-name-to-minibuffer ()
  (interactive)
  (insert (file-truename
           (buffer-name
            (window-buffer (minibuffer-selected-window))))))

(define-key minibuffer-local-map [f3] #'th/insert-file-name-to-minibuffer)

(defun th/comment-block ()
  (interactive)
  (let ((start (line-beginning-position))
        (end (line-end-position)))
    (when (region-active-p)
      (setq start (save-excursion
                    (goto-char (region-beginning))
                    (beginning-of-line)
                    (point))
            end (save-excursion
                  (goto-char (region-end))
                  (end-of-line)
                  (point))))
    (comment-or-uncomment-region start end)))

(global-set-key (kbd "M-;") #'th/comment-block)

(defun th/back-to-indentation-or-bol ()
  "Go to first non whitespace character on a line, or if already on the first
        non whitespace character, go to the beginning of the previous non-blank line."
  (interactive)
  (if (= (point)
         (save-excursion
           (back-to-indentation)
           (point)))
      (beginning-of-line)
    (back-to-indentation)))

(global-set-key (kbd "C-a") #'th/back-to-indentation-or-bol)

(defun th/eol-or-bol ()
  (interactive)
  (if (eolp)
      (back-to-indentation)
    (move-end-of-line nil)))

(global-set-key (kbd "C-e") 'th/eol-or-bol)

(defun th/insertline-and-move-to-line (&optional up)
  "Insert a newline, either below or above depending on `up`. Indent accordingly."
  (interactive)
  (beginning-of-line)
  (if up
      (progn
        (newline)
        (forward-line -1))
    (move-end-of-line nil)
    (open-line 1)
    (forward-line 1))
  (indent-according-to-mode))

(global-set-key (kbd "C-o") #'th/insertline-and-move-to-line)
(global-set-key (kbd "C-M-o") (lambda ()
                                (interactive)
                                (th/insertline-and-move-to-line t)))

;; The default felt off
(global-set-key (kbd "M-j") (lambda () (interactive) (join-line -1)))

(defun unfill-paragraph (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max))
        ;; This would override `fill-column' if it's an integer.
        (emacs-lisp-docstring-fill-column t))
    (fill-paragraph nil region)))

(global-set-key (kbd "M-Q") 'unfill-paragraph)

(defun th/copy-unfilled-paragraphs ()
  "Kills paragraphs as one line.

Suitable for pasting to things that want one-liners rather than
split on 80 characters."
  (interactive)
  (when (not (region-active-p))
    (error "No region active; cannot unfill paragraphs"))

  (kill-ring-save 0 0 t)
  (with-temp-buffer
    (yank)
    (replace-regexp "^ +" "" nil (point-min) (point-max))
    (goto-char (point-min))
    (push-mark (point-max))
    (setq mark-active t)
    (unfill-paragraph t)
    (kill-ring-save (point-min) (point-max))
    (message "Unfilled paragraphs killed")))

(global-set-key (kbd "C-M-w") 'th/copy-unfilled-paragraphs)

(defun th/duplicate-current-line-or-region (arg)
  "Duplicates the current line or region ARG times.

If there's no region, the current line will be duplicated. However, if
there's a region, all lines that region covers will be duplicated."
  (interactive "p")
  (let (beg end (origin (point)))
    (if (and mark-active (> (point) (mark)))
        (exchange-point-and-mark))
    (setq beg (line-beginning-position))
    (if mark-active
        (exchange-point-and-mark))
    (setq end (line-end-position))
    (let ((region (buffer-substring-no-properties beg end)))
      (dotimes (i arg)
        (goto-char end)
        (newline)
        (insert region)
        (setq end (point)))
      (goto-char (+ origin (* (length region) arg) arg)))))

(global-set-key (kbd "C-x d") #'th/duplicate-current-line-or-region)
(use-package multiple-cursors)

;; These are the three special keys; led (tools), any (launch5) and butterfly (launch6)
(bind-key "<XF86Tools>" #'er/contract-region)
(bind-key "<XF86Launch5>" #'er/expand-region)
(bind-key "<XF86Launch6>" #'avy-goto-char-timer)

(bind-key "<C-XF86Tools>" 'mc/unmark-next-like-this)
(bind-key "<C-XF86Launch5>" 'mc/mark-next-like-this)
(bind-key "<s-XF86Launch5>" 'mc/mark-all-dwim)

;; When in mc-mode, make the lower special keys help out
(bind-key "<XF86Launch6>"'mc/cycle-forward mc/keymap)
(bind-key "<escape>" 'mc-hide-unmatched-lines-mode mc/keymap)

;; Fn-b and Fn-n to traverse undo and redo
(bind-key "<XF86Launch8>" 'undo-tree-undo)
(bind-key "<XF86Launch9>" 'undo-tree-redo)

(use-package expand-region
  :bind
  ("C-c SPC" . hydra-mark/body)
  ("C-c C-SPC" . hydra-mark/body)
  ("<XF86Launch7>" . hydra-mark/body)

  :config
  (setq shift-select-mode nil) ;; https://github.com/magnars/expand-region.el/issues/220
  (use-package change-inner)

  (defhydra hydra-mark (:color blue :columns 4)
    "Mark / mc"
    ("a" mc/mark-all-dwim "mark all")
    ("l" mc/mark-next-like-this "mark next" :exit nil)
    ("C-SPC" mc/mark-next-like-this "mark next" :exit nil)
    ("<XF86Launch7>" mc/mark-next-like-this "mark next" :exit nil)
    ("h" mc/mark-previous-like-this "mark previous" :exit nil)
    ("e" mc/edit-lines "edit lines")

    ("d" er/mark-defun "Defun / Function")
    ("f" er/mark-defun "Defun / Function")
    ("w" er/mark-word "Word")
    ("u" er/mark-url "Url")
    ("E" er/mark-email "Email")
    ("b" mark-whole-buffer "Buffer")
    ("p" er/mark-text-paragraph "Paragraph")
    ("s" er/mark-symbol "Symbol")
    ("S" er/mark-symbol-with-prefix "Prefixed symbol")
    ("q" er/mark-inside-quotes "Inside Quotes")
    ("Q" er/mark-outside-quotes "Outside Quotes")
    ("(" er/mark-inside-pairs "Inside Pairs")
    ("[" er/mark-inside-pairs "Inside Pairs")
    ("{" er/mark-inside-pairs "Inside Pairs")
    (")" er/mark-outside-pairs "Outside Pairs")
    ("]" er/mark-outside-pairs "Outside Pairs")
    ("}" er/mark-outside-pairs "Outside Pairs")
    ("t" er/mark-inner-tag "Inner Tag")
    ("T" er/mark-outer-tag "Outer Tag")
    ("c" er/mark-comment "Comment")
    ("i" change-inner "Inner")
    ("o" change-outer "Outer")
    ("SPC" er/expand-region "Expand Region" :exit nil)
    ("M-SPC" er/contract-region "Contract Region" :exit nil)
    ("q" nil)))

(use-feature ffap)

(defun th/ffap-dwim ()
  "Find the file under point or start browsing for it."
  (interactive)
  (let ((fn (ffap-file-at-point))
        (sym (symbol-name (symbol-at-point))))
    (split-window-sensibly)
    (if fn
        (find-file fn)
      (counsel-find-file sym))))

(global-set-key (kbd "C-x M-f") #'th/ffap-dwim)
(global-set-key (kbd "s-M-f") #'th/ffap-dwim)

;; TODO(thiderman): These kind of need region support
(defun th/move-line-up ()
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-for-tab-command))

(defun th/move-line-down ()
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-for-tab-command))

(global-set-key [M-up] #'th/move-line-up)
(global-set-key [M-down] #'th/move-line-down)

(use-package visual-regexp-steroids
  :bind
  ("C-r" . vr/replace)
  (:map vr/minibuffer-keymap
        ("C-q" . quoted-insert)))

(use-package hexrgb
  :commands (hexrgb-increment-red hexrgb-increment-green hexrgb-increment-blue))

(use-package whole-line-or-region
  :diminish whole-line-or-region-local-mode
  :config
  (whole-line-or-region-global-mode 1))

(bind-key "M-w" 'whole-line-or-region-kill-ring-save)

;; Hydra that can be used to gradually increase or decrease hex
;; colors. Very useful when designing color themes!
(defhydra th/hexrgb-hydra (:foreign-keys warn)
  "
Colors (rgb+, rgb-) [%`th/hexrgb-step]
"
  ("q" (th/hexrgb #'hexrgb-increment-red 1))
  ("w" (th/hexrgb #'hexrgb-increment-green 1))
  ("e" (th/hexrgb #'hexrgb-increment-blue 1))
  ("a" (th/hexrgb #'hexrgb-increment-red -1))
  ("s" (th/hexrgb #'hexrgb-increment-green -1))
  ("d" (th/hexrgb #'hexrgb-increment-blue -1))
  ("i" (setq th/hexrgb-step (read-number "Step number: ")) "Set increment")
  ("x" nil "exit" :exit t))

(defvar th/hexrgb-step 1 "Steps to move when using th/hexrgb")

(defun th/hexrgb (fun val)
  "Helper that runs color increments via the `th/hexrgb-hydra'."
  (when (looking-at "#")
    (forward-char 1))
  (let* ((color (format "#%s" (word-at-point)))
         (result (funcall fun color 2 (* th/hexrgb-step val))))
    (when (not (looking-at "#"))
      (search-backward "#"))
    (delete-char 1)
    (kill-word 1)
    (insert result)))

(defun th/filter-symbol-at-point (p)
  "Use `keep-lines' or `flush-lines' on the symbol under the cursor.

For the latter, give the prefix argument."
  (interactive "P")
  (save-excursion
    (let* ((name (symbol-name (symbol-at-point)))
           (func (if p 'flush-lines 'keep-lines)))
      (goto-char (point-min))
      (funcall func name)
      (message "filter-symbol-at-point: (%s \"%s\")" func name))))

(global-set-key (kbd "C-x C-i") 'th/filter-symbol-at-point)

;; Pasting into minibuffer
(defun th/paste-from-x-clipboard ()
  (interactive)
  (shell-command "echo $(xsel -o)" 1))

(defun th/paste-in-minibuffer ()
  (local-set-key (kbd "M-y") 'th/paste-from-x-clipboard)
  (local-set-key [mouse-2] 'th/paste-from-x-clipboard))

(add-hook 'minibuffer-setup-hook 'th/paste-in-minibuffer)

(setq mouse-yank-at-point t)

(defun crontab-e ()
  (interactive)
  (with-editor-async-shell-command "crontab -e"))

(defadvice save-buffer (around save-buffer-as-root-around activate)
  "Create non-existing parent directories; sudo to save the current buffer if permissions are lacking."
  (interactive "p")

  ;; Only do any of this if we actually have a file
  (if (buffer-file-name)
      (progn
        ;; Create the parent directories
        (make-directory (f-dirname (buffer-file-name)) t)

        ;; If the file is not writeable, try
        (if (not (file-writable-p (buffer-file-name)))
            (let ((buffer-file-name (format "/sudo::%s" buffer-file-name)))
              ad-do-it))
        ad-do-it)
    ad-do-it))

(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

(show-paren-mode t)

(global-set-key (kbd "C-x l") 'downcase-region)

(global-set-key (kbd "<C-tab>") 'dabbrev-expand)

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

(electric-pair-mode t)
(global-auto-revert-mode t)
(auto-fill-mode t)

(defun th/auto-fill-paragraph ()
  "Runs `fill-paragraph' on `post-self-insert-hook' when in `markdown-mode'"
  (when (and (eq major-mode 'markdown-mode)
             ;; Without this check, it's impossible to write spaces, because they are immediately
             ;; removed by the call to `fill-paragraph'.
             (not (looking-at "$")))
    (fill-paragraph)))

(add-hook 'post-self-insert-hook 'th/auto-fill-paragraph)

(setq comment-column 42)
(setq fill-column 78)
(setq word-wrap t)
(setq sentence-end-double-space nil)

(setq-default indent-tabs-mode nil)
(setq-default tab-always-indent 'complete)
(setq-default tab-width 2)
(setq-default standard-indent 2)
;;;;* Emoji

(defun clipboard (string &optional message)
  "Put string into the clipboard"
  (call-process-region string nil "xsel" nil nil nil "-i")
  (let ((message (or message string)))
    (message "%s: %s" (propertize "Clipboarded" 'face 'font-lock-string-face) message)))

;;;###autoload
(defun shrug (arg)
  "Shrug. ¯\\_(ツ)_/¯ "
  (interactive "p")
  (let ((shrug "¯\\_(ツ)_/¯"))
    (when (eq arg 4)
      (insert shrug))
    (clipboard shrug)))

;;;###autoload
(defun purple-heart ()
  "Inserts 💜 into the X clipboard."
  (interactive)
  (clipboard "💜" (emojify-string ":purple_heart:")))

(defun th/wide-emoji-text ()
  "Prompt for text and emoji and make them into :eggplant: W I D E :eggplant: mode. :weary::sweat_drops:"
  (interactive)
  (let* ((text (read-string "Text to widen: "))
         (emoji (or (th/wide-emoji-default text)
                    (emojify-completing-read "Emoji: "))))
    (clipboard (format "%s %s %s"
                       (emojify-string emoji)
                       (th/widen-text text)
                       (emojify-string emoji)))))

(defvar th/wide-emoji-defaults '((".*shiv.*" ":dagger_knife:")
                                 (".*dong.*" ":eggplant:")
                                 (".*magnum.*" ":eggplant:"))
  "A list of regexps that return a certain emoji if something matches")

(defun th/wide-emoji-default (text)
  "Returns a default emoji for a text.

Specifically if one is found via regexp in
  `th/wide-emoji-defaults'."
  (second (-first (lambda (s) (s-match (car s) text))
           th/wide-emoji-defaults)))

(defun th/widen-text (text)
  "Turns \"text\" into \"T E X T\"."
  (s-trim (s-replace-regexp
           ;; This adds spaces everywhere via a fairly stupid regexp
           "\\(.\\)" "\\1 "
           (s-upcase text))))

;;;;* Bash the Building

(defun verb-the-noun ()
  (interactive)
  (let* ((url "https://spire.yasa.gs")
         (phrase (with-current-buffer (url-retrieve-synchronously url)
                   (capitalize
                    (first (last (split-string (buffer-string) "\n")))))))
    (notifications-notify
     :title phrase
     :app-icon (f-join user-emacs-directory "var" "slay-the-spire.png"))
    ;; Also put it on the clipboard
    (call-process-region
     phrase nil
     "xsel" nil nil nil
     "-i")))

;;;* Eshell

(use-package vterm
  :commands vterm
  :config
  (setq vterm-disable-bold-font nil)
  (setq vterm-disable-inverse-video nil)
  (setq vterm-disable-underline nil)
  (setq vterm-kill-buffer-on-exit nil)
  (setq vterm-max-scrollback 9999)
  (setq vterm-shell "/usr/bin/zsh")
  (setq vterm-term-environment-variable "xterm-256color"))

(require 'eshell)
;; (require 'em-tramp)
(setq eshell-prefer-lisp-functions nil)
(setq eshell-prefer-lisp-variables nil)
(setq eshell-banner-message "")
(setq password-cache nil) ; enable password caching
(setq password-cache-expiry 3600) ; for one hour (time in secs)

(setq eshell-scroll-to-bottom-on-input 'all
      eshell-error-if-no-glob t
      eshell-hist-ignoredups t
      eshell-save-history-on-exit t
      eshell-prefer-lisp-functions nil
      eshell-destroy-buffer-when-process-dies t)

(use-package eshell-prompt-extras
  :config
  (with-eval-after-load "esh-opt"
    (autoload 'epe-theme-lambda "eshell-prompt-extras")
    (setq eshell-highlight-prompt nil
          eshell-prompt-function 'epe-theme-lambda)))

(defun th/eshell-here (arg)
  "Opens up a new shell in the directory associated with the
current buffer's file. The eshell is renamed to match that
directory to make multiple eshell windows easier."
  (interactive "P")
  (let* ((root-buffer (window-buffer (selected-window)))
         (parent (if (buffer-file-name root-buffer)
                     (file-name-directory (buffer-file-name))
                   default-directory))
         (shellname (concat "*eshell: " parent "*"))
         (shell-buffer (get-buffer shellname)))

    ;; (when (not arg)
    ;;   (split-window-below -20)
    ;;   (windmove-down))

    (if shell-buffer
        ;; If we're already in eshell, go back to where we were.
        ;; Otherwise, switch to it said eshell.
        (if (string= (buffer-name root-buffer)
                     (buffer-name shell-buffer))
            (previous-buffer)
          (switch-to-buffer shell-buffer))

      ;; It doesn't exist yet - let's create it!
      (eshell "new")
      (rename-buffer shellname))))

(defun th/eshell-dired ()
  (interactive)
  (th/eshell-here 1))

(defun th/eshell-disable-company ()
  "Disables company, because dear $DEITY is it annoying in there"
  (company-mode -1))

(add-hook 'eshell-mode-hook #'th/eshell-disable-company)
(add-hook 'eshell-post-command-hook #'eshell-write-history)

;; (global-set-key (kbd "C-x e") #'th/eshell-here)

(setenv "PAGER" "cat")
(defalias 'e 'find-file)

(global-set-key (kbd "C-x M-e") (th/mode-menu eshell-mode))

(defun th/eshell-toggle-sudo ()
  "Add sudo at the beginning of the current line.

If already there, remove it."
  (interactive)
  (save-excursion
    (eshell-bol)
    (if (looking-at "sudo")
        (delete-forward-char 5)
      (insert "sudo "))))

(bind-key "C-c C-s" #'th/eshell-toggle-sudo eshell-mode-map)

(defun eshell-next-prompt (n)
  "Move to end of Nth next prompt in the buffer. See `eshell-prompt-regexp'."
  (interactive "p")
  (re-search-forward eshell-prompt-regexp nil t n)
  (when eshell-highlight-prompt
    (while (not (get-text-property (line-beginning-position) 'read-only) )
      (re-search-forward eshell-prompt-regexp nil t n)))
  (eshell-skip-prompt))

(defun eshell-previous-prompt (n)
  "Move to end of Nth previous prompt in the buffer. See `eshell-prompt-regexp'."
  (interactive "p")
  (backward-char)
  (eshell-next-prompt (- n)))

(defun eshell-insert-history ()
  "Displays the eshell history to select and insert back into your eshell."
  (interactive)
  (insert (ivy-completing-read "Eshell history: "
                               (delete-dups
                                (ring-elements eshell-history-ring)))))

(add-hook
 'eshell-mode-hook
 (lambda ()
   (define-key eshell-mode-map (kbd "M-P") 'eshell-previous-prompt)
   (define-key eshell-mode-map (kbd "M-N") 'eshell-next-prompt)
   (define-key eshell-mode-map (kbd "M-r") 'eshell-insert-history)
   (define-key eshell-mode-map (kbd "M-s") 'th/eshell-toggle-sudo)))

;;;* Fly

(use-package flycheck
  :config
  ;; We disable the overrides so that we can use the builtin navigation to
  ;; navigate errors from compilation buffers. Super nice that this is
  ;; configurable.
  (setq flycheck-standard-error-navigation nil)
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

(defhydra hydra-flycheck (:color blue :idle 0.3)
  "
^
^Flycheck^        ^Errors^          ^Checker^      ^Normal^
^────────^────────^──────^──────────^───────^──────^──────^
_q_ quit          _c_ check         _s_ select     _j_ previous
_v_ verify setup  _n_ next          _d_ disable    _k_ next
_m_ manual        _p_ previous      _?_ describe   _e_ first
                _l_ list
^^                  ^^                  ^^           ^^
"
  ("q" nil)
  ("e" first-error)
  ("s-y" first-error)
  ("j" next-error :color red)
  ("k" previous-error :color red)
  ("c" flycheck-buffer)
  ("d" flycheck-disable-checker)
  ("l" flycheck-list-errors :exit t)
  ("m" flycheck-manual)
  ("n" flycheck-next-error :color red)
  ("p" flycheck-previous-error :color red)
  ("s" flycheck-select-checker)
  ("v" flycheck-verify-setup)
  ("?" flycheck-describe-checker))

;;;* Hydra

(defvar whitespace-mode nil)

(defvar th/toggle-title (with-faicon "toggle-on" "Toggles" 1 -0.05))

(pretty-hydra-define th/toggle-hydra
  (:color amaranth :quit-key "q" :title th/toggle-title)
  ("Basic"
   (("a" abbrev-mode "abbrev" :toggle t)
    ("n" linum-mode "line number" :toggle t)
    ("w" whitespace-mode "whitespace" :toggle t)
    ("f" th/toggle-auto-fill :toggle t)
    ("L" page-break-lines-mode "page break lines" :toggle t)
    ("t" toggle-truncate-lines "truncate" nil))
   "Highlight"
   (("h" highlight-symbol-mode "symbol" :toggle t)
    ("l" hl-line-mode "line" :toggle t)
    ("r" rainbow-mode "rainbow" :toggle t))
   "System"
   (("s-a" keyboard-qwerty-a6-us-mode "a6 layout" :toggle t)
    ("s-d" keyboard-us-mode "us layout" :toggle t))
   "Emacs"
   (("d" toggle-debug-on-error "debug on error" :toggle (default-value 'debug-on-error))
    ("x" toggle-debug-on-quit "debug on quit" :toggle (default-value 'debug-on-quit)))
   "Hydras"
   (("s-f" th/font-hydra/body "font-hydra" :exit t)
    ("s-s" th/yas-hydra/body "yas-hydra" :exit t))))

(defvar th/toggle-auto-fill nil)
(defun th/toggle-auto-fill ()
  "Toggles auto fill and FCI at the same time"
  (interactive)
  (let ((arg (if auto-fill-function -1 1)))
    (setq th/toggle-auto-fill (= arg 1))
    (auto-fill-mode arg)
    (fci-mode arg)))

(defun th/iosevka (size)
  (set-frame-font (format "Iosevka-%s" size))
  (telephone-line-mode 1))

(defhydra th/font-hydra (:color amaranth)
  "Fonts"
  ("d" (th/iosevka 10))
  ("f" (th/iosevka 13))
  ("h" (th/iosevka 17))
  ("j" (th/iosevka 20))
  ("k" (th/iosevka 22))
  ("l" (th/iosevka 24))
  ("i" (describe-char (point)) "font information" :exit t)
  ("q" nil))

(defun th/git-grep-dotfiles ()
  "Start a search in the dotfiles"
  (interactive)
  (let* ((default-directory "~/src/gitlab.com/thiderman/dotfiles"))
    (counsel-git-grep)))

(defhydra th/exec-hydra (:foreign-keys warn :exit t :columns 5)
  "Execution"
  ("C-c" (progn
           (find-file "~/src/gitlab.com/thiderman/dotfiles/emacs/.emacs.d/init.el")
           (th/goto-header)) "init.el")
  ("C-f" (projectile-switch-project-by-name "~/src/gitlab.com/thiderman/dotfiles") "dotfiles")
  ("C-s" th/git-grep-dotfiles "conf search")
  ("C-g" (magit-status "~/src/gitlab.com/thiderman/dotfiles") "conf magit")
  ("d" (find-file "/ssh:di:") "dragonisle")
  ("C-d" daemons "daemons")
  ("C-a" ssh-agent-add-key "add ssh key")
  ("f" hydra-flycheck/body "flycheck")
  ("e" enved "enved")
  ("M-e" enved-load "Load 12FA env")
  ("h" hydra-helpful/body "helpful")
  ("g" customize-group "customize")
  ("k" kubernetes-overview "kubernetes")
  ("m" th/quickmajor "major-mode")
  ("n" th/toggle-minor-mode "minor-mode")
  ("p" list-processes "processes")
  ("t" ssh-tunnels "tunnels")
  ("M-t" (progn
           (tramp-cleanup-all-buffers)
           (message "tramp buffers killed")) "un-tramp")
  ("M-p" proced "proced")

  ("s" th/smerge-hydra/body "smerge")
  ("x" th/hexrgb-hydra/body "hexrgb")
  ("C-q" (save-buffers-kill-emacs t) "exit emacs")
  ("q" nil))

;; (global-set-key (kbd "s-SPC") 'th/exec-hydra/body)
(global-set-key (kbd "C-x C-C") 'th/exec-hydra/body)

(defhydra th/files-hydra (:color teal :idle 0.5)
  "Files"
  ("s-f" counsel-projectile-find-file "project files")
  ("f" counsel-projectile-find-file "project files")
  ("b" th/other-files-same-base "base")
  ("s-s" th/other-files "suffix")
  ("s" th/other-files "suffix")
  ("e" th/browse-extensions "extensions")

  ("g" (th/other-files ".go") "go")
  ("j" (th/other-files ".js") "js")
  ("v" (th/other-files ".vue") "vue")

  ("m" (th/other-files "Makefile" "./Makefile") "makefiles")
  ("r" (th/other-files ".http" "restclient.http") "rest")
  ("d" (th/other-files "Dockerfile" "Dockerfile") "docker")
  ("c" (th/other-files "docker-compose" "docker-compose.yml") "docker-compose")
  ("i" (th/other-files "gitlab-ci" ".gitlab-ci.yml") "gitlab")
  ("t" (th/other-files "Taskfile" "Taskfile.yml") "taskfile"))

(defun th/projects--title ()
  (let ((p (projectile-project-name)))
    (with-octicon "repo"
                  (if (s-blank-p p)
                      "Projects"
                    (s-concat "Projects (" p ")")))))

(pretty-hydra-define th/projects
  (:color teal :quit-key "q" :title (th/projects--title))
  ("Current Project"
   (("s-f" counsel-projectile "open file/buffer")
    ("s-d" counsel-projectile-find-dir "open directory")
    ("b" counsel-projectile-switch-to-buffer "switch to buffer")
    ("i" projectile-ibuffer "ibuffer")
    ("k" projectile-kill-buffers "kill buffers")
    ("I" projectile-invalidate-cache "invalidate cache"))

   "Filetypes"
   (("g" (th/other-files ".go") "go")
    ("j" (th/other-files ".js") "js")
    ("v" (th/other-files ".vue") "vue"))

   "Targets"
   (("m" (th/other-files "Makefile" "./Makefile") "Makefiles")
    ("r" (th/other-files ".http" "restclient.http") "Restclient")
    ("d" (th/other-files "Dockerfile" "Dockerfile") "Dockerfile")
    ("c" (th/other-files "docker-compose" "docker-compose.yml") "docker-compose")
    ("i" (th/other-files "gitlab-ci" ".gitlab-ci.yml") "Gitlab CI")
    ("t" (th/other-files "Taskfile" "Taskfile.yml") "taskfile"))

   "All Projects"
   (("p" th/ew/switch-project "switch"))))

(global-set-key (kbd "C-x f") 'th/files-hydra/body)

(defhydra th/goto-hydra (:color amaranth :columns 3)
  "Goto"
  ("C-h" beginning-of-buffer "bob" :exit t)
  ("h" beginning-of-buffer "bob")
  ("C-l" end-of-buffer "eob" :exit t)
  ("l" end-of-buffer "eob")
  ("u" scroll-down-command "up")        ; Sometimes...
  ("d" scroll-up-command "down")
  ("w" avy-goto-word-1 "word" :exit t)
  ("f" flycheck-list-errors "flycheck list" :exit t)
  ("M-g" goto-line "goto-line" :exit t)
  ("g" goto-line "goto-line" :exit t)
  ("e" first-error "first error")
  ("j" next-error "next error")
  ("k" previous-error "prev error")
  ("b" pop-to-mark-command "back")
  ("q" nil))

(pretty-hydra-define ph/goto (:foreign-keys warn
                              :idle 0.5
                              :title (with-faicon "arrows" "Navigation" 1 -0.05)
                              :quit-key "q")
  ("Absolutes"
   (("C-h" beginning-of-buffer "bob" :exit t)
    ("h" beginning-of-buffer "bob")
    ("C-l" end-of-buffer "eob" :exit t)
    ("l" end-of-buffer "eob"))

   "Navigation"
   (("u" scroll-down-command "up")      ; Sometimes...
    ("d" scroll-up-command "down")
    ("b" pop-to-mark-command "back"))

   "Specifics"
   (("w" avy-goto-word-1 "word" :exit t)
    ("g" goto-line "goto-line" :exit t))

   "Errors"
   (("f" flycheck-list-errors "flycheck list" :exit t)
    ("e" first-error "first error")
    ("j" next-error "next error")
    ("k" previous-error "prev error"))))

(global-set-key (kbd "C-l") 'ph/goto/body)
(bind-key "C-h C-h" #'beginning-of-buffer)

(defmacro hydra-from-keymap (keymap)
  "Take a keymap and make it into a hydra.

Works recursively, and sub-keymaps will be made into hydras as well"

  (let* ((name (symbol-name keymap))
         (hydra-name (make-symbol (format "hydra-keymap/%s-select-buffer"
                                          name)))
         (docstring (format "Autogenerated hydra for `%s'." name)))
    `(defhydra ,hydra-name (:exit t)
       ,docstring
       ,@(mapcar 'hydra--keymap-to-hydra-lists (reverse (cdr keymap))))))

(defun hydra--keymap-to-hydra-lists (part)
  (when (not (atom part))
    (let ((key (key-description (list part))))
      (cond
       ;; If the second part is a list, then it's a sub-keymap. Call recursively.
       ((listp (cdr part))
        (list key
              (hydra-from-keymap (cdr part))
              (format "%s-map" key)))
       ;; If not, then it's a key!
       (t
        (list key
              (cdr part)
              (hydra--guess-package-name-for-symbol (cdr part))))))))

(defun hydra--guess-package-name-for-symbol (s)
  ;; TODO(thiderman): Perhaps use nameless somehow?
  (let* ((name (symbol-name s))
         (prefix (car (s-split "-" name))))
    (s-replace (format "%s-" prefix) ":" name)))

;;;* Snippets

(use-package yasnippet
  :diminish yas-minor-mode
  :init
  (yas-global-mode)
  :config
  (add-hook 'prog-mode-hook 'yas-minor-mode)
  (add-hook 'markdown-mode 'yas-minor-mode)
  (add-hook 'org-mode-hook 'yas-minor-mode))

(use-package yasnippet-snippets
  :after yasnippet
  :config
  (yas-reload-all)) ;; Without this, it doesn't load...

;; Bound to be used in the toggle hydra
(pretty-hydra-define th/yas-hydra
  (:color teal :quit-key "q" :title (with-faicon "arrows" "Snippets" 1 -0.05))
  ("yas"
   (("s-s" yas-insert-snippet "snippet")
    ("s-c" yas-new-snippet "new")
    ("s-v" yas-visit-snippet-file "visit"))))

(defun th/taskfile-snippet ()
  (interactive)
  (yas-expand-snippet (yas-lookup-snippet "taskfile" 'yaml-mode)))

(use-package autoinsert
  :config
  (setq auto-insert-query nil)
  (auto-insert-mode 1)
  (add-hook 'find-file-hook 'auto-insert)
  (setq auto-insert-alist nil) ;; Remove the defaults
  (add-to-list 'auto-insert-alist '("Taskfile\\.yml$" . [th/taskfile-snippet])))

(defun th/taskfile-tasks ()
  "Get a list of the available tasks in the current Taskfile"
  (-map #'car (alist-get 'tasks (yaml-parse-buffer))))

(defun th/taskfile-complete-task ()
  "Complete available tasks in the current Taskfile"
  (completing-read "task: " (th/taskfile-tasks) nil t))

(defun th/taskfile-insert-task ()
  "Insert a task at point."
  (interactive)
  (let ((task (th/taskfile-complete-task)))
    ;; Simple support to build the deps list. If we are looking at the end of
    ;; the list but also not at the beginning of it, add commas for separation.
    (when (and (looking-at "\\]")
               (not (or (looking-back "\\[")
                        (looking-back ", "))))
      (insert ", "))
    (insert task)))

(defun th/taskfile-goto-task ()
  "Get a list of the available tasks in the current Taskfile"
  (interactive)
  (when-let ((task (th/taskfile-complete-task)))
    (beginning-of-buffer)
    (search-forward-regexp (format "^  %s:" task))
    (back-to-indentation)))

;; TODO(thiderman): M-n/p for traversing items?
(bind-key "C-c C-t" #'th/taskfile-insert-task yaml-mode-map)
(bind-key "C-c C-f" #'th/taskfile-goto-task yaml-mode-map)


;;;* Find-file

(defvar find-file-confirm-size-p t
  "If `find-file' should check if the file might be dangerously large for Emacs to open")

(defvar find-file-confirm-ratio-threshold 500
  "Average line length before `find-file' warns when visiting a file")

(defvar find-file-allowed-extensions '("png"
                                       "jpg"
                                       "jpeg"
                                       "gif"
                                       "pdf")
  "File types that are always allowed since they are not rendered as text")

(defun find-file-confirm-size (filename)
  (if (and find-file-confirm-size-p
           (file-exists-p filename)
           (not (file-directory-p filename))
           (not (member (f-ext filename) find-file-allowed-extensions)))
      ;; If the file exists, we should try to figure out if it might be
      ;; too large or not
      (let* ((size (file-attribute-size (file-attributes filename)))
             ;; Since the entire point of this function is that we can not use
             ;; Emacs builtin facilities, we shell out to `wc' to get the line
             ;; count of the file
             (lines (string-to-number
                     (first
                      (split-string
                       (shell-command-to-string
                        ;; TODO(thiderman): Might not work on other platforms,
                        ;; at least not on Windows. Maybe OSX, I dunno.
                        (format "wc -l %s" filename))))))
             ;; As long as there is more than one line, we are going to check
             ;; the ratio of the file. If there are zero lines (e.g. the file
             ;; ends without a linebreak), fake it to be one since Emacs
             ;; unfortunately still lacks the capability to divide by zero.
             (ratio (/ size (if (zerop lines) 1 lines)))
             (above-threshold (> ratio find-file-confirm-ratio-threshold)))

        ;; If are are above the threshold, make sure to ask the user if they
        ;; want to actually open the file or not.
        (if above-threshold
            (yes-or-no-p (format "%s has an average line length of %d. Really open?"
                                 filename ratio))
          t))
    t))

(global-so-long-mode 1)

;; This is copied directly from Emacs 26.1

;;;###autoload
;; (defun find-file (filename &optional wildcards)
;;   "Edit file FILENAME.
;; Switch to a buffer visiting file FILENAME,
;; creating one if none already exists.
;; Interactively, the default if you just type RET is the current directory,
;; but the visited file name is available through the minibuffer history:
;; type \\[next-history-element] to pull it into the minibuffer.

;; The first time \\[next-history-element] is used after Emacs prompts for
;; the file name, the result is affected by `file-name-at-point-functions',
;; which by default try to guess the file name by looking at point in the
;; current buffer.  Customize the value of `file-name-at-point-functions'
;; or set it to nil, if you want only the visited file name and the
;; current directory to be available on first \\[next-history-element]
;; request.

;; You can visit files on remote machines by specifying something
;; like /ssh:SOME_REMOTE_MACHINE:FILE for the file name.  You can
;; also visit local files as a different user by specifying
;; /sudo::FILE for the file name.
;; See the Info node `(tramp)File name Syntax' in the Tramp Info
;; manual, for more about this.

;; Interactively, or if WILDCARDS is non-nil in a call from Lisp,
;; expand wildcards (if any) and visit multiple files.  You can
;; suppress wildcard expansion by setting `find-file-wildcards' to nil.

;; To visit a file without any kind of conversion and without
;; automatically choosing a major mode, use \\[find-file-literally]."
;;   (interactive
;;    (find-file-read-args "Find file: "
;;                         (confirm-nonexistent-file-or-buffer)))
;;   (when (find-file-confirm-size filename)
;;     (let ((value (find-file-noselect filename nil nil wildcards)))
;;       (if (listp value)
;; 	        (mapcar 'pop-to-buffer-same-window (nreverse value))
;;         (pop-to-buffer-same-window value)))))

;;* Programming and developing
;;;* Compile

(use-package filenotify)

(use-package compile
  :init
  (add-to-list 'comint-output-filter-functions 'ansi-color-process-output)

  (ignore-errors
    (require 'ansi-color)
    (defun colorize-compilation-buffer ()
      (when (eq major-mode 'compilation-mode)
        (ansi-color-apply-on-region compilation-filter-start (point-max))))
    (add-hook 'compilation-filter-hook 'colorize-compilation-buffer))

  ;; We usually want to see all of it
  (add-hook 'compilation-mode-hook (lambda () (toggle-truncate-lines -1)))
  (add-hook 'grep-mode-hook (lambda () (toggle-truncate-lines 1)))

  (setq compilation-always-kill t)
  (setq compilation-ask-about-save nil)
  (setq compilation-auto-jump-to-first-error nil)
  (setq compilation-scroll-output t)
  (setq compilation-read-command nil))

(defun compile-dedicated (command buffer-name &optional no-focus always-compile)
  "Compile a command in a dedicated buffer, or switch to it if it exists.

The optional NO-FOCUS argument will just trigger a recompile
rather than switch to the buffer."
  (let ((buffer (get-buffer buffer-name)))
    (cond
     (buffer
      ;; If we already have a buffer, we should recompile in it. If `no-focus'
      ;; is given, trigger a recompile without going to the buffer.
      (if no-focus
          (with-current-buffer buffer
            ;; If we've marked that we always want to compile, we should trigger the command rather
            ;; than recompiling it
            (if always-compile
                (compile command)
              (recompile)))
        (switch-to-buffer buffer)
        (recompile)))

     ;; If we're not - start a new one and rename it accordingly.
     (t
      (compile command)
      (with-current-buffer (get-buffer "*compilation*")
        (rename-buffer buffer-name))))))

(use-package make-mode
  :bind (:map makefile-mode-map
              ("C-c C-p" . makefile-toggle-phony))
  :hook (makefile-mode . whitespace-mode))

(use-package executor
  :straight (executor :type git
                      :host gitlab
                      :repo "thiderman/executor.el"
                      :branch "master")
  :bind (:map compilation-mode-map
              ("c" . executor-execute)
              ("f" . executor-visit-file)
              ("b" . executor-select-buffer))
  :bind-keymap (("C-x C-m" . executor-global-map))
  :hook
  (prog-mode . executor-maybe-enable-mode)
  (text-mode . executor-maybe-enable-mode))

(defun orly-start (cmd &rest file-list)
  "Run CMD on FILE-LIST using nohup."
  (interactive
   (let* ((files (dired-get-marked-files t nil))
          (cmd (orly--guess-cmd files)))
     (if (cl-search (car files) cmd)
         (list cmd)
       (cons cmd files))))
  (let (fname)
    (if (and (null (cdr file-list))
             (not (file-directory-p (setq fname (car file-list))))
             (file-executable-p fname)
             (string-match-p "^\\(#!\\|ELF\\)" (counsel--command "head" "-1" fname)))
        (let ((buf (compile (concat "./" (car file-list)) t)))
          (select-window (cl-find buf
                                  (window-list)
                                  :key #'window-buffer))
          (goto-char (point-max)))
      (start-process
       cmd nil shell-file-name
       shell-command-switch
       (concat
        (unless (string-match-p "|" cmd)
          "nohup 1>/dev/null 2>/dev/null ")
        cmd
        " "
        (mapconcat #'shell-quote-argument file-list " "))))))

(defun orly--guess-cmd (files)
  (if current-prefix-arg
      (dired-read-shell-command "& on %s: " nil files)
    (let ((prog (dired-guess-default files)))
      (if (consp prog)
          (car prog)
        prog))))

(bind-key "C-<return>" #'orly-start dired-mode-map)

;; Make something similar to the above, but for when you are inside the script
(defun th/shell-script-execute-buffer ()
  "Runs the current script in a compilation buffer"
  (interactive)
  (compile-dedicated
   (format "./%s" (f-relative (buffer-file-name) default-directory))
   (format "*shell: %s*" (buffer-name))
   t))

(require 'sh-script)
(bind-key "C-<return>" #'th/shell-script-execute-buffer sh-mode-map)

(setq go-tc/kwds
      '(("RUN" . font-lock-function-name-face)
        ("FROM" . font-lock-function-name-face)
        ("ENV" . font-lock-function-name-face)
        ("WORKDIR" . font-lock-function-name-face)
        ("ADD" . font-lock-function-name-face)
        ("COPY" . font-lock-function-name-face)
        ("EXPOSE" . font-lock-function-name-face)
        ("ENTRYPOINT" . font-lock-function-name-face)
        ("CMD" . font-lock-function-name-face)
        ("PASS" . font-lock-type-face)
        ("FAIL" . font-lock-warning-face)
        ("===\\|---" . font-lock-comment-face)
        ("^WARN .*" . font-lock-warning-face)
        ("20..-..-..T..:..:..Z" . font-lock-comment-face)
        ("20..-..-..T..:..:......\\+...." . font-lock-comment-face)))

(define-minor-mode golang-test-compile-mode
  "Doc string."
  nil "☯" nil
  (font-lock-add-keywords nil go-tc/kwds)

  (if (fboundp 'font-lock-flush)
      (font-lock-flush)
    (when font-lock-mode
      (with-no-warnings (font-lock-fontify-buffer)))))

(add-hook 'compilation-mode-hook 'golang-test-compile-mode)

;;;###autoload
(defun makefile-toggle-phony ()
  "Toggle .PHONY on the current rule"
  (interactive)
  (save-excursion
    (backward-sentence)
    (if (looking-at ".PHONY")
        (kill-line 1)
      (insert (format ".PHONY: %s\n" (symbol-at-point))))))

(defun th/compilation-goto-error-trace ()
  "Goes to the first error trace in the buffer"
  (interactive)
  (beginning-of-buffer)
  (search-forward "Error Trace:")
  (recenter 5))

(defun th/compilation-next-error-trace ()
  "Goes to the next error trace in the buffer"
  (interactive)
  (end-of-line)
  (search-forward "Error Trace:")
  (recenter 5))

(defun th/compilation-prev-error-trace ()
  "Goes to the next error trace in the buffer"
  (interactive)
  (beginning-of-line)
  (search-backward "Error Trace:")
  (recenter 5))

;; (bind-key "n" #'next-line compilation-mode-map)
;; (bind-key "p" #'previous-line compilation-mode-map)
(bind-key "n" #'th/compilation-next-error-trace compilation-mode-map)
(bind-key "p" #'th/compilation-prev-error-trace compilation-mode-map)
(bind-key "t" #'toggle-truncate-lines compilation-mode-map)

(bind-key "<tab>" #'overlay-expander-toggle compilation-mode-map)
(bind-key "C-M-i" #'overlay-expander-clear compilation-mode-map)
(bind-key "b" #'overlay-expander-pop-to-buffer compilation-mode-map)

(bind-key "e" #'executor-execute compilation-mode-map)
(bind-key "f" #'executor-visit-file compilation-mode-map)

(bind-key "C-c C-e" #'th/compilation-goto-error-trace compilation-mode-map)
(bind-key "." #'th/compilation-goto-error-trace compilation-mode-map)

;;;* Docker

(use-package docker
  :bind (("C-x C-d" . docker-containers))
  :init

  (defadvice docker-containers (around docker-fullscreen activate)
    (window-configuration-to-register :docker-fullscreen)
    ad-do-it
    (delete-other-windows))

  (defadvice tablist-quit (after docker-restore-screen activate)
    (jump-to-register :docker-fullscreen)))

(use-package dockerfile-mode
  :bind (:map dockerfile-mode-map
              ("C-c C-c" . th/docker-compose)))
(use-package docker-tramp)

(use-package docker-compose-mode
  :straight (docker-compose-mode
             :host github
             :repo "thiderman/docker-compose-mode"
             :branch "executor"
             :upstream (:host github :repo "meqif/docker-compose-mode"))
  :bind (:map docker-compose-mode-map
              ("C-c C-c" . docker-compose-execute-command)
              ("C-c C-b" . docker-compose-build-buffer)
              ("C-c C-s" . docker-compose-start-buffer))
  :init
  (setq docker-compose-commands
        (list
         "build"
         "build --no-cache"
         "logs --follow"
         "logs --follow --tail=100"
         "up"
         "up --build"
         "create"
         "down"
         "exec"
         "images"
         "logs"
         "ps"
         "pull"
         "push"
         "restart"
         "rm"
         "rm -f"
         "run"
         "start"
         "stop"
         "top"
         )))

(defun th/docker-compose ()
  "Run docker compose on the current file"
  (interactive)
  (compile "make docker"))

(use-package kubernetes
  :commands (kubernetes-overview)
  :bind (:map kubernetes-overview-mode-map
              ("s" . kubernetes-set-namespace)))

;;* Languages
;;;* Lisp

(use-package lispy
  :diminish lispy-mode
  :init
  (setq lispy-no-space t)
  :bind
  (:map lispy-mode-map
        ("C-c q" . lispy-ace-paren)
        ("C-c a" . th/lispy-ace-symbol)
        ("C-c k" . outline-previous-heading)
        ("C-c j" . outline-next-heading)
        ("C-c m" . lispy-mark-symbol)
        ("C-c l" . th/lispy-toggle-let)
        ("C-c i" . th/lispy-indent)
        ("C-c C-j" . lispy-goto-symbol)))

(defun th/lispy-ace-symbol ()
  (interactive)
  (lispy-ace-symbol 2))

(defun lisp-hooks ()
  "Helper to set up multiple modes used in all lisps"
  (lispy-mode 1)
  (paredit-mode 1)
  (eldoc-mode 1)
  (rainbow-identifiers-mode 1)

  ;; emacs lisp should probably still be at 80
  (when (not (eq major-mode 'emacs-lisp-mode))
    (setq-local fill-column 100))

  (prettify-symbols-mode 1)
  (push '("interactive" . ?ι) prettify-symbols-alist)
  (push '("exwm-input-set-key" . ?Ψ) prettify-symbols-alist))

(defun th/lispy-toggle-let ()
  "Toggle between `let' and `let*'"
  (interactive)
  (let* ((found nil)
         (symbols
          (-flatten
           (-map
            (lambda (f) (list
                    (format "%s" f)
                    (format "-%s" f)
                    (format "-%s\\*" f)))
            '(let if-let and-let when-let))))
         (regexp (format "(\\(%s\\)" (s-join "\\|" symbols))))
    (save-excursion
      (while (and (not (bolp))
                  (not found))
        (lispy-backward 1)
        (save-excursion
          (when (looking-at regexp)
            (setq found t)
            (forward-char 1)
            (forward-sexp 1)
            (if (looking-back "*")
                (delete-char -1)
              (insert "*"))))))
    (th/lispy-indent)))

(defun th/lispy-indent ()
  (interactive)
  (let ((start (point)))
    (beginning-of-defun)
    (lispy--normalize-1)
    (goto-char start)))

;;;;* elisp

(use-feature elisp-mode
  :bind
  (:map emacs-lisp-mode-map
        ("C-c C-c" . eros-eval-defun)
        ("C-c C-b" . eval-buffer-or-region)
        ("C-c C-e" . emacs-lisp-macroexpand)
        ("C-c C-m" . lispy-mark-symbol))

  :init
  (setq edebug-print-length nil)
  :hook
  (emacs-lisp-mode . lisp-hooks)
  :blackout
  (emacs-lisp-mode . "elisp"))

(use-package nameless
  :diminish 'nameless-mode
  :after elisp-mode
  :hook (emacs-lisp-mode . nameless-mode))

(use-package eros
  :after elisp-mode
  :hook (emacs-lisp-mode . eros-mode))

(defun th/buffer-or-region (action-name buffer-func region-func)
  (let ((s "Buffer"))
    (if (use-region-p)
        (progn
          (funcall region-func (region-beginning) (region-end))
          (keyboard-escape-quit)
          (setq s "Region"))
      (funcall buffer-func))

    (message "buffer-or-region: %s %s" s action-name)))

(defun eval-buffer-or-region ()
  (interactive)
  (th/buffer-or-region "eval" #'eval-buffer #'eval-region))

(with-current-buffer "*scratch*"
  (emacs-lisp-mode))

;;;;* Common lisp

(setq inferior-lisp-program "/usr/bin/sbcl")
(add-hook 'lisp-mode-hook 'lisp-hooks)

(use-package sly
  :config
  ;; I vastly prefer ivy over other completion systems. The
  ;; `sly-completing-read' variant has some special handling of `REQUIRE-MATCH'
  ;; that this might miss. We'll see if that's a problem or not.
  (setq sly-completing-read-function #'ivy-completing-read)
  :bind (("C-c s" . sly)))

(use-package sly-quicklisp
  :after sly)

;;;* Golang

(use-package go-mode
  :bind
  (:map go-mode-map
        ("C-c C-j" . godef-jump-other-window)
        ("C-c C-." . godef-jump)

        ("C-c C-f" . go-goto-hydra/body)
        ("M-m"     . go-goto-hydra/body)
        ("C-c i"   . go-goto-imports)
        ("C-c e"   . th/go-run-main)

        ("C-c a"   . ff-find-other-file)
        ("C-c f"   . th/go-goto-failed-test)
        ("C-c ;"   . th/go-comment-block)
        ("C-M-x"   . th/go-single-test)
        ("C-c C-c" . th/go-single-test)

        ("DEL"     . delete-backward-char)
        ("C-d"     . delete-char)

        ("C-c r"   . go-rename)
        ("C-c C-m" . go-refactor-map)
        ("C-c C-e" . go-refactor-declaration-colon)
        ("M-r"     . go-refactor-unwrap)
        ("C-c l"   . go-refactor-toggle-nolint))

  :config
  (require 'lsp-go)
  (add-hook 'go-mode-hook 'th/go-hook)
  (add-hook 'go-mode-hook 'go-eldoc-setup)
  (add-hook 'go-mode-hook 'flycheck-mode)

  (setq gofmt-command "goimports")
  (setq gofmt-args "")

  (define-prefix-command 'go-refactor-map)
  (define-key go-refactor-map (kbd "i") 'go-refactor-wrap-if)
  (define-key go-refactor-map (kbd "f") 'go-refactor-wrap-for)
  (define-key go-refactor-map (kbd "g") 'go-refactor-wrap-goroutine)
  (define-key go-refactor-map (kbd "e") 'go-refactor-wrap-errif)
  (define-key go-refactor-map (kbd "u") 'go-refactor-unwrap)
  (define-key go-refactor-map (kbd "v") 'go-refactor-extract-variable)
  (define-key go-refactor-map (kbd "d") 'go-refactor-declaration-colon)
  (define-key go-refactor-map (kbd "r") 'go-refactor-method-receiver))

(use-package go-rename
  :after go-mode
  :config
  (defadvice go-rename (before save-go-rename activate)
    "Save the buffer before calling go-rename."
    (save-buffer)))

;; (use-package company-go :after go-mode)
(use-package go-guru :after go-mode)
(use-package go-eldoc :after go-mode)
(use-package go-impl
  :after go-mode
  :bind (:map go-mode-map ("C-c M-i" . go-impl)))
(use-package go-add-tags
  :after go-mode
  :bind (:map go-mode-map ("C-c t" . go-add-tags)))

(use-package flycheck-golangci-lint
  :hook (go-mode . flycheck-golangci-lint-setup))

(defun th/go-hook ()
  (add-hook 'before-save-hook 'gofmt-before-save)
  ;; (set (make-local-variable 'company-backends) '(company-go))
  (setq imenu-generic-expression
        '(("type" "^type *\\([^ \t\n\r\f]*\\)" 1)
          ("func" "^func *\\(.*\\) {" 1)))

  ;; This needs to be set in the hook rather than in the :map because
  ;; the go-guru mode map will override it otherwise.
  (define-key go-mode-map (kbd "C-c C-o") 'th/go-guru/body)

  (company-mode))

(defun th/go-install-tools ()
  "Install the various tools needed by this setup.

Can be called again to update and rebuild all of them."
  (interactive)
  (let ((default-directory (getenv "HOME")) ; Run in $HOME to avoid installing into projects
        (pkgs '("github.com/golangci/golangci-lint"
                "github.com/josharian/impl"
                "github.com/rogpeppe/godef"
                "golang.org/x/tools/cmd/goimports"
                "golang.org/x/tools/cmd/gorename"
                "golang.org/x/tools/cmd/guru"
                "golang.org/x/tools/gopls"
                "github.com/nsf/gocode")))
    (compile (format "go get -v -u %s" (s-join " " pkgs)))))

(defhydra go-goto-hydra (:exit t :columns 3 :foreign-keys warn)
  "goto"
  ("a" go-goto-arguments "arguments")
  ("d" go-goto-docstring "docstring")
  ("f" go-goto-function "function")
  ("i" go-goto-imports "imports")
  ("m" go-goto-method-receiver "receiver")
  ("n" go-goto-function-name "name")
  ("r" go-goto-return-values "return")
  ("q" nil))

(defhydra th/go-guru (:exit t :columns 3)
  "Guru commands"
  ("d" go-guru-describe "describe")
  ("f" go-guru-freevars "freevars")
  ("i" go-guru-implements "implements")
  ("c" go-guru-peers "peers (channels)")
  ("r" go-guru-referrers "referrers")
  ("j" go-guru-definition "definition")
  ("p" go-guru-pointsto "pointsto")
  ("s" go-guru-callstack "callstack")
  ("e" go-guru-whicherrs "whicherrs")
  ("<" go-guru-callers "callers")
  (">" go-guru-callees "callees")
  ("x" go-guru-expand-region "expand-region"))

(defun th/go-get-test-above ()
  "Gets the name of the test above point"
  (save-excursion
    (re-search-backward "^func \\(Test\\|Example\\|Benchmark\\)" nil t)
    (forward-word 2)
    (thing-at-point 'symbol t)))

(defvar th/go-last-single-test "go test"
  "The last single test command that was run")

(defvar th/go-test-coverage nil
  "If we should run coverage or not")

(defun th/go-single-test ()
  "If in test file, run the test above point. If not, run the last run test."
  (interactive)
  (projectile-save-project-buffers)

  ;; We only update the command when we are in a test file
  (when (s-contains? "_test.go" (buffer-file-name))
    (let* ((test (th/go-get-test-above)))
      (setq th/go-single-test
            (s-join " "
                    (list
                     "go test -failfast -v"
                     (format (if (s-starts-with? "Benchmark" test)
                                 "-run=XXX -bench=%s"
                               "-run=%s")
                             test)

                     (when th/go-test-coverage
                       "-coverprofile=coverage.out"))))))

  (compile-dedicated
   th/go-single-test
   (format "*go-test:%s*" (projectile-project-name))
   t
   t))

(defun th/go-select-type-signature ()
  "Make a selection of what type to attach a new func to"
  (save-excursion
    (let* ((empty "<none>")
           (type (completing-read
                  "Type: "
                  (append (list empty)
                          (go--get-types)))))
      (if (not (s-equals? type empty))
          (format
           " %s "
           (go--convert-type-name-to-receiver
            (car (s-split " " type))))
        " "))))

(defun th/go-run-main ()
  (interactive)

  (compile
   (format "go run -v %s"
           (s-join
            " "
            (-filter (lambda (fn)
                       (and (s-suffix? ".go" fn)
                            (not (s-suffix? "_test.go" fn))))
                     (directory-files default-directory))))))

(defun th/go-comment-block (arg)
  "Comment an entire block of code.

With the universal arguments, it adds a test skip."
  (interactive "p")

  (let ((start)
        (end))
    (save-excursion
      (re-search-backward "{$")
      (setq start (point))
      (forward-list)
      (setq end (point))

      (comment-or-uncomment-region (+ 2 start) (- end 1)))

    (when (= 4 arg)
      (goto-char start)
      (end-of-line)
      (newline-and-indent)
      (insert "t.Skip()")
      (backward-char 1))))

(defun th/go-goto-failed-test ()
  "Go to the first failed test mentioned by the test output"
  (interactive)
  (let* ((target)
         (msg)
         (start)
         (end))
    (with-current-buffer "*compilation*"
      ;; First, grab the target, as in `filename.go:123'
      (beginning-of-buffer)
      (setq start (re-search-forward "	Error Trace:	"))
      (end-of-line)
      (setq end (point))
      (setq target (buffer-substring-no-properties start end))

      ;; Then, grab the reason
      (forward-char 15)
      (setq start (point))
      (end-of-line)
      (setq end (point))
      (setq msg (buffer-substring-no-properties start end)))

    (let* ((split (s-split ":" target))
           (file (-first (lambda (f) (s-suffix? (first split) f))
                         (projectile-current-project-test-files))))
      (when (not file)
        (error "Could not find file %s" (first split)))

      (find-file (expand-file-name file (projectile-project-root)))
      (goto-line (string-to-number
                  (second split)))
      (back-to-indentation)
      (reposition-window))
    (message "%s: %s"
             (propertize "Error" 'face font-lock-warning-face)
             msg)))

;;;;* Docstring manipulation
(defun go-update-docstring ()
  "Update (or create) the docstring function name of the current function.

Designed to be called from hooks.

Will not update tests (beginning with Test/Example/Benchmark) or
private functions (lowercase)."
  (interactive)

  ;; Only run this hook when in go mode and if we are actually looking at the function definition
  ;; line.
  (when (and (eq major-mode 'go-mode)
             (go--function-definition-line-p))
    (save-excursion
      (if-let ((fn (go--function-name)))
          (when (go--should-generate-docstring-p fn)
            (go-goto-docstring)
            ;; Check if we need to update anything
            (when (and
                   (not (looking-at "$")) ;; If at the end of the line, the name has already been generated.
                   (not (looking-at (format "%s " fn)))) ;; If already looking at the correct name, then nothing changed.
              (kill-word 1)
              (insert fn)
              ;; If we updated and are at the end of the line, add a space.
              (if (looking-at "$")
                  (insert " ")
                (forward-char 1))))))))

(defun go--function-definition-line-p ()
  "Returns true if we are currently on a function definition line"
  (save-excursion
    (and
     (progn
       (beginning-of-line)
       (looking-at "^func "))
     (progn
       (end-of-line)
       (backward-char 1)
       (looking-at "{$")))))

(defun go--should-generate-docstring-p (func-name)
  "Check if we should update the docstring or not"
  (and
   ;; If the function name is a test, skip it.
   (not (or (s-prefix? "Test" func-name)
            (s-prefix? "Example" func-name)
            (s-prefix? "Benchmark" func-name)))
   ;; If the function name is lowercase, then we don't need a docstring
   (not (s-lowercase? (s-left 1 func-name)))))

(defmacro go-docstring-advice (func)
  "Make a generic macro that runs `go-update-docstring' after common operations."
  (let* ((name (symbol-name func))
         (advice-name (make-symbol (format "go-%s-docstring" name)))
         (docstring (format
                     "Update go function docstring after running `%s'." name)))
    `(defadvice ,func (after ,advice-name activate)
       ,docstring
       (go-update-docstring))))

(go-docstring-advice delete-char)
(go-docstring-advice backward-delete-char)
(go-docstring-advice kill-word)
(go-docstring-advice backward-kill-word)

(add-hook 'post-self-insert-hook 'go-update-docstring)

;;;;* Refactoring

(defun go-refactor-wrap-if ()
  "Wraps the current line in an if statement."
  (interactive)
  (go--refactor-wrap "if ")
  (forward-char 3))

(defun go-refactor-wrap-for ()
  "Wraps the current line in a for loop."
  (interactive)
  (go--refactor-wrap "for ")
  (forward-char 4))

(defun go-refactor-wrap-goroutine ()
  "Wraps the current line in a goroutine."
  (interactive)
  (go--refactor-wrap "go func()")

  ;; Also add the parenthesis at the end
  (save-excursion
    (end-of-line)
    (backward-char 1)
    (forward-list)
    (insert "()"))

  (forward-char 8))

(defun go-refactor-wrap-errif ()
  "Wraps the current statement in an ~err := <x> ; err != nil {~ block."
  ;; TODO(thiderman): Would be really neat if this could use inspection to
  ;; figure out if the statement actually returns an error or not
  (interactive)
  (beginning-of-line-text)
  (insert "err := ")
  (end-of-line)
  (insert "; err != nil {}")
  (backward-char 1)
  (newline-and-indent))

(defun go-refactor-unwrap ()
  "Take the current statement (or region) and raise it to replace its parent.

Naively tries to figure out what the opening statement is by finding
the previous line ending with an opening brace."

  (interactive)
  (let* ((pos (go--region-or-lines))
         (beg (car pos))
         (end (cadr pos)))

    ;; Figure out if we can actually do a raise
    (save-excursion
      (goto-char beg)
      ;; If looking back at the beginning of the line or just a single tab, we
      ;; cannot raise because that would always produce an unusable end result.
      (when (looking-back "^\t?")
        (error "Cannot raise top-level statements")))

    (kill-region beg end)
    (re-search-backward "{$" nil t)

    ;; Find out where the wrapping statement is and delete it
    (save-excursion
      (beginning-of-line-text)
      (setq beg (point)))
    (forward-list 1)
    (setq end (point))
    (delete-region beg end)

    ;; Finally, yank out the original statements.
    ;; Out of laziness, also run gofmt to make them prettier again.
    (yank)
    (gofmt)))

(defun go-refactor-extract-variable ()
  "Extract the current region into a variable"
  (interactive)

  (when (not (region-active-p))
    (error "No region active"))

  (save-excursion
    (let* ((var (read-string "Variable name: ")))
      (kill-region (region-beginning) (region-end))
      (insert var)

      (beginning-of-line-text)
      (newline-and-indent)
      (previous-line)
      (indent-according-to-mode)

      (insert (format "%s := " var))
      (yank))))

(defun go-refactor-declaration-colon ()
  "Toggle if the current line should use \"=\" or \":=\"."
  (interactive)
  (save-excursion
    (let (beg end)
      (end-of-line)
      (setq end (point))
      (beginning-of-line)
      (setq beg (point))

      (when (not (s-contains? "=" (buffer-substring beg end)))
        (error "No declaration on current line"))

      (search-forward "=")
      (backward-char 1)
      (if (looking-back ":" 1)
          (delete-char -1)
        (insert ":")))))

(defun go-refactor-toggle-nolint ()
  "Toggle the '// nolint' token on a line."
  (interactive)
  (save-excursion
    (end-of-line)
    (if (looking-back "// nolint")
        (progn
          (backward-char 10)
          (kill-line))
      (insert " // nolint"))))

(defun go-refactor-method-receiver ()
  "Changes or removes the method receiver of the current function.

A choice between all the types in the current file are
interactively presented. Also presented is an item `<none>',
which will remove the receiver if there is one.

If there was a receiver and a new one is chosen, all variables are changed."
  ;; TODO(thiderman): We need to undo twice to undo this. Investigate.
  (interactive)
  (save-excursion
    (go-goto-function t)
    (forward-char 5)

    (let* ((empty "<none>")
           (current-var
            (save-excursion
              (forward-char 1)
              (thing-at-point 'symbol t)))
           (type (completing-read
                  "Type: "
                  (append (go--get-types (buffer-file-name))
                          (list empty))))
           (receiver (when (not (s-equals? type empty))
                       (go--convert-type-name-to-receiver
                        (car (s-split " " type))))))

      (cond
       ;; If we are looking at an opening parenthesis, there is already a method receiver
       ((looking-at "(")
        ;; Firstly, store the current receiver variable name.

        ;; Then, delete the existing one.
        (delete-region
         (point)
         (save-excursion
           (forward-list 1)
           (point)))
        ;; If we do not have a receiver (i.e. we chose 'empty) we should
        ;; delete the extra space.
        (if (not receiver)
            (delete-char 1)
          ;; If there was a receiver previously and we set a new one, update the
          ;; variable name.
          (insert receiver)

          ;; And also update the variable name inside of the function.
          (when (and current-var receiver)
            (go--refactor-symbol-in-function
             current-var
             (s-downcase (s-left 1 type))))))
       ((and (not (looking-at "(")) receiver)
        ;; There is no receiver, but we are adding one. Just insert it.
        (insert (format "%s " receiver)))))))

(defun go--refactor-symbol-in-function (from to)
  "Changes instances of the symbol `from' into `to'.

Assumes that point is on line defining the function we are replacing in."
  (save-excursion
    (beginning-of-line)
    (let ((start
           (save-excursion
             (forward-line -1)
             (point)))
          (end
           (save-excursion
             ;; TODO(thiderman): Make a method that reliably moves to opening brace.
             (end-of-line)
             ;; In case of trailing whitespace...
             (search-backward "{")
             (forward-list 1)
             (backward-char 1)
             (point))))

      (replace-string from to t start end))))

(defun go--refactor-wrap (prefix)
  "Wraps the current line or region or statement in a templated statement.

If the current line ends in an opening brace, the entire
statement until that brace's end will be wrapped.

Point ends up on the beginning of the templated statement."
  (interactive)
  (save-excursion
    (let* ((pos (go--region-or-lines))
           (beg (car pos))
           (end (cadr pos)))
      (kill-region beg end)
      (indent-according-to-mode)
      (insert (format "%s {}" prefix))
      (backward-char 1)
      (newline-and-indent)
      (yank)))

  (beginning-of-line-text)

  ;; Indent the things we just wrapped
  (indent-region
   (point)
   (save-excursion
     (end-of-line)
     (backward-char 1)
     (forward-list)
     (point))))

(defun go--region-or-lines ()
  "Operate or regions or lines"

  (let (beg end)
    (if (and mark-active (> (point) (mark)))
        (exchange-point-and-mark))
    (setq beg (save-excursion
                (back-to-indentation)
                (point)))
    (if mark-active
        (exchange-point-and-mark))
    ;; If we're on a line that ends on an opening brace, set the end to
    ;; be the outside of that brace.
    (setq end
          (save-excursion
            (if (progn (end-of-line)
                       (backward-char 1)
                       (looking-at "{"))
                (progn
                  (forward-list)
                  (point))
              (line-end-position))))
    (list beg end)))

(defun go--convert-type-name-to-receiver (tn)
  "Converts from the string \"Type\" to \"(t *Type)\""
  (format "(%s *%s)" (s-downcase (s-left 1 tn)) tn))

(defun go--get-types (&optional filename)
  "Return a list of all the types found in the package that FILENAME is in.

The strings returned are based on all lines that begin with
'^type'. The letters 'type ' and the ending ' {' are both
removed."

  (let* ((filename (or filename (buffer-file-name)))
         (directory (f-dirname filename)))
    ;; First, loop over all of the Go files in the directory and insert their contents into a
    ;; temporary buffer
    (with-temp-buffer
      (dolist (fn (directory-files directory t "\\.go\\'"))
        (insert-file-contents fn))
      ;; Then loop over the lines, filtering out everything that's not a type and then cleaning the
      ;; strings up so that they are just the name and the kind.
      (-map
       (lambda (s)
         (s-chop-suffix " {" (s-chop-prefix "type " s)))
       (-filter
        (lambda (s)
          (s-prefix? "type " s))
        (split-string (buffer-string) "\n" t))))))

;;;* Python

(use-package python-mode
  :config
  (add-hook 'python-mode-hook 'flycheck-mode)
  (setq-default py-split-windows-on-execute-p nil))

(use-package anaconda-mode
  :config
  (add-hook 'python-mode-hook 'anaconda-mode))

(use-package pip-requirements)

;;;* Web

(use-package typescript-mode)
(use-package tide)

(use-package vue-mode
  :init
  (require 'js)
  (require 'css-mode)

  ;; Interestingly enough, the bindings that are vue specific needs to
  ;; be in all the maps.
  ;; TODO(thiderman): Improve on this.
  :bind (("C-c v" . th/vue-switcher)
         :map vue-mode-map
         ("C-c f" . th/vue-goto)
         ("C-c s" . th/vue-toggle-style-scope)
         ("C-c C-n" . th/vue-next-section)
         ("C-c C-p" . th/vue-prev-section)
         :map vue-html-mode-map
         ("C-c f" . th/vue-goto)
         ("C-c C-n" . th/vue-next-section)
         ("C-c C-p" . th/vue-prev-section)
         :map js-mode-map
         ("C-c f" . th/vue-goto)
         ("C-c C-n" . th/vue-next-section)
         ("C-c C-p" . th/vue-prev-section)
         :map css-mode-map
         ("C-c f" . th/vue-goto)
         ("C-c C-n" . th/vue-next-section)
         ("C-c C-p" . th/vue-prev-section)
         ("C-c s" . th/vue-toggle-style-scope))

  :mode "\\.vue\\'"
  :config
  (setq vue-modes
        '((:type template :name nil :mode web-mode)
          (:type template :name html :mode web-mode)
          (:type script :name nil :mode js-mode)
          (:type script :name js :mode js-mode)
          (:type script :name es6 :mode js-mode)
          (:type script :name babel :mode js-mode)
          (:type script :name typescript :mode typescript-mode)
          (:type style :name nil :mode css-mode)
          (:type style :name css :mode css-mode)
          (:type style :name stylus :mode stylus-mode)
          (:type style :name less :mode less-css-mode)
          (:type style :name scss :mode css-mode)
          (:type style :name sass :mode ssass-mode)))

  (setq css-indent-offset 2)
  (setq mmm-submode-decoration-level 0)
  (add-hook 'vue-mode-hook 'th/disable-semantic)
  (add-hook 'vue-mode-hook 'th/vue-auto-yas)
  (add-hook 'vue-mode-hook 'th/vue-auto-template-insert))

(defun th/disable-semantic ()
  (semantic-mode -1))

(defun th/vue-auto-yas ()
  (yas-minor-mode 1))

(defun th/vue-switcher ()
  (interactive)
  (th/other-files-suffix "vue"))

(defun th/vue-goto (kind)
  "Go to the vue section specified by `kind'.

Insert it if it does not exist."
  (interactive
   (list
    (completing-read "vue section: " '(template script style) nil t)))

  (beginning-of-buffer)
  (if (ignore-errors (re-search-forward (format "^<%s" kind)))
      ;; Found the section - go to the beginning of the line
      (beginning-of-line)
    ;; The section was not found. Add it.
    (end-of-buffer)
    (newline (if (and (bolp) (eolp)) 1 2))
    (insert kind)
    (when (string= kind "style")
      (insert "[lang=scss]"))
    (emmet-expand-line nil)
    (newline 1 t)
    (when (string= kind "style")
      (th/vue-toggle-style-scope))))

(defun th/vue-toggle-style-scope ()
  "Add or remove the 'scoped' keyword to the <style> tag."
  (interactive)
  (save-excursion
    (th/vue-goto "style")
    (forward-word 1)
    (when (looking-at " lang")
      (forward-word 2)
      (forward-char 1))
    (if (looking-at " *>")
        (insert " scoped")
      (kill-word 1))))

(defun th/vue-auto-template-insert ()
  "Insert the template block when `vue-mode' is enabled in an empty buffer."
  (when (= (buffer-size) 0)
    (dolist (tag '("template" "div"))
      (insert tag)
      (emmet-expand-line nil)
      (newline-and-indent))))

(defun th/vue-goto-section (search-func)
  "Search backwards or forwards for the beginning of a vue section"

  (let ((success (ignore-errors (funcall search-func "^<[^/]"))))
    (beginning-of-line)
    success))

(defun th/vue-prev-section ()
  "Go to the previous vue section."
  (interactive)
  (th/vue-goto-section 're-search-backward))

(defun th/vue-next-section ()
  "Go to the next vue section."
  (interactive)
  (when (looking-at "^<")
    ;; If we're already at the beginning of a tag, we need to move
    ;; forward to be able to search past it.
    (forward-char 1))

  (when (not (th/vue-goto-section 're-search-forward))
    ;; If it didn't work, check if that is because the next tag doesn't
    ;; exist yet. If such is the case then create it.
    (let ((l (length (th/vue-tags-in-buffer))))
      (cond
       ((= l 0)
        (th/vue-goto "template"))
       ((= l 1)
        (th/vue-goto "script"))
       ((= l 2)
        (th/vue-goto "style"))))))

(defun th/vue-tags-in-buffer ()
  "Returns the current root tags in the buffer"

  (-filter
   (lambda (s) (and (s-prefix? "<" s) (not (s-prefix? "</" s))))
   (s-split "\n" (buffer-substring-no-properties (point-min) (point-max)))))

(use-package web-mode
  :after vue-mode
  :bind (:map web-mode-map
              ("M-a" . web-mode-element-previous)
              ("M-e" . web-mode-element-next)
              ("M-h" . web-mode-mark-and-expand))

  :config
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-attr-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  :mode (("\\.tsx$" . web-mode)
         ("\\.html?$" . web-mode)))

(use-package emmet-mode
  :bind (:map emmet-mode-keymap
              ("C-j" . th/emmet-vue-expand))
  :init
  (setq emmet-indentation 2)
  (setq emmet-insert-flash-time 0.01)

  :config
  (unbind-key "C-c C-c w" emmet-mode-keymap)

  :hook
  (sgml-mode . emmet-mode)
  (css-mode .  emmet-mode)
  (web-mode .  emmet-mode)
  (vue-mode .  emmet-mode))

(defun th/emmet-vue-expand ()
  "Use `emmet-expand-line' and set `emmet-use-css-transform' depending on the major mode."
  (interactive)
  (let ((emmet-use-css-transform (derived-mode-p 'css-mode))
        (inhibit-message t))
    (emmet-expand-line nil)

    (when (derived-mode-p 'web-mode)
      (web-mode-buffer-indent))))

(setq httpd-port 8001)

(use-package npm-mode
  :config
  (add-hook 'json-mode-hook 'npm-mode))

(add-hook 'semantic-inhibit-functions
            (lambda () (member major-mode '(vue-mode))))

;;;* LSP

(use-package lsp-mode
  :init
  (setq lsp-prefer-flymake nil)
  (setq lsp-keymap-prefix "<f30>")

  :demand t
  :after jmi-init-platform-paths)

(defhydra hydra-lsp (:exit t :hint nil)
  "
 Buffer^^               Server^^                   Symbol
-------------------------------------------------------------------------------------
 [_f_] format           [_M-r_] restart            [_d_] declaration  [_i_] implementation  [_o_] documentation
 [_m_] imenu            [_S_]   shutdown           [_D_] definition   [_t_] type            [_r_] rename
 [_x_] execute action   [_M-s_] describe session   [_R_] references   [_s_] signature"
  ("d" lsp-find-declaration)
  ("D" lsp-ui-peek-find-definitions)
  ("R" lsp-ui-peek-find-references)
  ("i" lsp-ui-peek-find-implementation)
  ("t" lsp-find-type-definition)
  ("s" lsp-signature-help)
  ("o" lsp-describe-thing-at-point)
  ("r" lsp-rename)

  ("f" lsp-format-buffer)
  ("m" lsp-ui-imenu)
  ("x" lsp-execute-code-action)

  ("M-s" lsp-describe-session)
  ("M-r" lsp-restart-workspace)
  ("S" lsp-shutdown-workspace))

(use-package lsp-ui
  :config
  (setq lsp-ui-doc-enable nil
        lsp-ui-sideline-enable nil
        lsp-ui-flycheck-enable t)
  :after lsp-mode)

(use-package dap-mode
  :config
  (dap-mode t)
  (dap-ui-mode t))

(use-package company-lsp)
(use-package lsp-ivy)

;;;* Java
(use-package bazel-mode
  :mode ("\\BUILD\\'" . bazel-mode))
(use-package lsp-java
  :init
  (defun jmi/java-mode-config ()
    (toggle-truncate-lines 1)
    (setq-local tab-width 4)
    (setq-local c-basic-offset 4)
    (lsp))

  :config
  ;; Enable dap-java
  (require 'dap-java)

  (setq lsp-ui-doc-position 'bottom)
  (setq lsp-ui-doc-border "#604060")

  ;; Support Lombok in our projects, among other things
  (setq lsp-java-vmargs
        (list "-noverify"
              "-Xmx2G"
              "-XX:+UseG1GC"
              "-XX:+UseStringDeduplication"
              ;; (concat "-javaagent:" jmi/lombok-jar)
              ;; (concat "-Xbootclasspath/a:" jmi/lombok-jar)
              ))
  (setq lsp-file-watch-ignored '(".idea" ".ensime_cache" ".eunit" "node_modules"
                                 ".git" ".hg" ".fslckout" "_FOSSIL_"
                                 ".bzr" "_darcs" ".tox" ".svn" ".stack-work"
                                 "build"))
  (setq lsp-java-import-order '["" "java" "javax" "#"])
  ;; Don't organize imports on save
  (setq  lsp-java-save-action-organize-imports nil)
  ;; Formatter profile
  ;; (setq lsp-java-format-settings-url (concat "file://" jmi/java-format-settings-file))

  :hook (java-mode   . jmi/java-mode-config)

  :demand t
  :after (lsp lsp-mode dap-mode ;; jmi-init-platform-paths
              ))



;;;* Configuration management

(use-package salt-mode)

;;* Writing

(use-package writeroom-mode
  :init
  (setq writeroom-width 100)
  (setq writeroom-mode-line t))

(use-package flyspell
  :init
  (setq flyspell-abbrev-p t)
  (setq flyspell-issue-message-flag nil)
  (setq flyspell-issue-welcome-flag nil)

  (setq ispell-program-name "aspell")
  ;; Please note ispell-extra-args contains ACTUAL parameters passed to aspell
  (setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US")))

(use-package flyspell-correct-ivy
  :after flyspell
  :bind (:map flyspell-mode-map
        ("C-;" . hydra-spelling/body))
  :custom (flyspell-correct-interface 'flyspell-correct-ivy))

(defhydra hydra-spelling (:color blue :idle 0.5)
  "
  ^
  ^Spelling^          ^Errors^            ^Checker^
  ^────────^──────────^──────^────────────^───────^───────
  _q_ quit            _h_ previous        _c_ correction
  ^^                  _l_ next            _d_ dictionary
  ^^                  _f_ check           _m_ mode
  ^^                  ^^                  ^^
  "
  ("q" nil)
  ("C-;" flyspell-correct-word-generic)
  ("h" flyspell-correct-previous :color pink)
  ("l" flyspell-correct-next :color pink)
  ("c" ispell)
  ("d" ispell-change-dictionary)
  ("f" flyspell-buffer)
  ("m" flyspell-mode))

;;* Hugo

(defun th/hugo-complete-reference ()
  (let* ((files (-map
                 (lambda (f) (s-chop-prefix "content/" f))
                 (-filter
                  (lambda (f) (and (s-prefix? "content/" f)
                              (s-suffix? ".md" f)))
                  (projectile-project-files (projectile-project-root))))))
    (ivy-read "ref: " files :require-match t)))

(defun th/hugo-insert-reference ()
  "Insert a reference link"
  (interactive)
  (let* ((ref (th/hugo-complete-reference)))
    (insert (format "{{< ref \"%s\" >}}" ref))))

(defun th/hugo-replace-reference ()
  "Replace the reference link with another one completed from the file tree.

Tries to keep anchors intact."
  (interactive)

  (save-excursion
    (let* (;; Grabs the entire reference by using expand-region to grab the
           ;; surrounding curly braces. In there, search for the content inside of
           ;; the double quoted strings.
           (ref (second (s-match
                         "\"\\(.*\\)\""
                         (prog2
                             (er/mark-outside-pairs)
                             (buffer-substring-no-properties (point) (mark))
                           (deactivate-mark)))))
           ;; Grabs the anchor from the URL, if any
           (anchor (first (s-match "#.*" ref)))
           (url (th/hugo-complete-reference)))
      (replace-regexp (regexp-quote ref)
                      (concat url anchor)
                      nil
                      (point-min)
                      (point-max)))))

(defun th/hugo-new-section ()
  "Makes a new section in the current directory"
  (interactive)
  (let* ((section (read-string "new section: "))
         (dir (f-join default-directory section))
         (newfile (f-join dir "_index.md")))
    (when (not section)
      (user-error "No section provided"))

    (make-directory dir t)
    (find-file newfile)
    (message "Made new section %s" section)))

(defun th/hugo-split-to-section ()
  "Takes the current file and makes it into a section.

This means making a directory with the basename of the current
file, and moving the current file to be _index.md inside of it."
  (interactive)
  (let* ((buffer (current-buffer))
         (buffer-name (buffer-name buffer))
         (file (buffer-file-name buffer))
         (base (f-base file))
         (dir (f-join (f-dirname file) base))
         (newfile (f-join dir "_index.md")))

    (when (s-equals? base "_index")
      (user-error "Cannot figure out section name from _index.md file. Rename
      the file first."))

    (make-directory dir t)
    (rename-file file newfile)
    (kill-current-buffer)
    (find-file newfile)
    (message "Made %s into a section" buffer-name)))

(pretty-hydra-define th/hugo-hydra
  (:color teal :title (with-faicon "sitemap" "Hugo"))
  ("Commands"
   (("i" th/hugo-insert-reference "Reference insert")
    ("r" th/hugo-replace-reference "Reference replace")
    ("s" th/hugo-split-to-section "Split to section")
    ("n" th/hugo-new-section "New section"))))

(bind-key "C-c h" #'th/hugo-hydra/body markdown-mode-map)

;;* Drunkenfall

(setq th/df "/home/thiderman/src/gitlab.com/one-eye/drunkenfall/")

(defun th/start-drunkenfall ()
  (interactive)

  (let* ((dir th/df)
         (default-directory dir)
         (tf (concat dir "Taskfile.yml")))
    (find-file tf)

    (ssh-agent-add-key "/home/thiderman/.ssh/gitlab.rsa")

    (dolist (target '("backend:run" "proxy:run-dev" "db:start" "frontend:run"))
      (switch-to-buffer "*compilation*")
      (executor-execute tf target))))

(defun th/drunkenfall-db ()
  (interactive)
  (ssh-agent-add-key "/home/thiderman/.ssh/digitalocean.rsa")

  (copy-file
   "/scp:df:drunkenfall/data/db.sql"
   (concat th/df "sql/skeleton.sql")
   t))

(defun th/drunkenfall-psql ()
  (interactive)
  (let ((buffer (get-buffer "*SQL: drunkenfall-postgres*")))
    (if buffer
        (switch-to-buffer buffer)
      (let ((sql-postgres-program "psql")
            (sql-database "drunkenfall")
            (sql-server "localhost")
            (sql-user "postgres")
            (sql-product "postgres"))
        (sql-postgres "drunkenfall-postgres")
        (sql-set-sqli-buffer-generally)
        (sqlup-mode 1)
        (yas-minor-mode 1)))))

(defun th/drunkenfall-psql-prod ()
  (interactive)
  (let ((buffer (get-buffer "*SQL: drunkenfall-postgres-prod*")))
    (if buffer
        (switch-to-buffer buffer)
      (let ((sql-database "drunkenfall")
            (sql-server "localhost")
            (sql-port 35432)
            (sql-user "postgres")
            (sql-product "postgres"))
        (sql-postgres "drunkenfall-postgres-prod")
        (sql-set-sqli-buffer-generally)
        (sqlup-mode 1)
        (yas-minor-mode 1)))))

(defun th/drunkenfall-term ()
  "Spawn a terminal residing on the main DrunkenFall machine."
  (interactive)
  (ssh-agent-add-key "/home/thiderman/.ssh/digitalocean.rsa")
  (th/exwm-terminal "ssh df -t LANG=en_US.UTF-8 TERM=xterm-256color tmux new -A -s main -c ~/drunkenfall"))

(defun th/drunkenfall-verbosity ()
  "Toggles verbosity of the DrunkenFall databaz on or off"
  (interactive)
  (message
   "DB verbosity: %s"
   (setenv
    "DRUNKENFALL_DBVERBOSE"
    (if (string-equal "true" (getenv "DRUNKENFALL_DBVERBOSE"))
        "false"
      "true"))))

(defhydra th/drunkenfall-hydra (:foreign-keys warn :exit t :idle 0.5)
  "DrunkenFall"
  ("d" th/start-drunkenfall "start")
  ("s-C-d" th/start-drunkenfall "start")
  ("f" (projectile-switch-project-by-name "~/src/gitlab.com/one-eye/drunkenfall") "files")
  ("g" (magit-status "~/src/gitlab.com/one-eye/drunkenfall") "magit")
  ("h" (find-file "/ssh:df:drunkenfall") "host")
  ("t" th/drunkenfall-term "terminal")
  ("p" th/drunkenfall-psql "psql")
  ("s" (ssh-tunnels-run-tunnel) "ssh tunnel")
  ("M-p" th/drunkenfall-psql-prod "prod psql")
  ("v" th/drunkenfall-verbosity "db verbose")
  ("q" nil))

(bind-key "C-c d" #'th/drunkenfall-hydra/body)
(bind-key "s-C-d" #'th/drunkenfall-hydra/body)

;;* Chill

(defun th/chill-psql ()
  (interactive)
  (let ((buffer (get-buffer "*SQL: chill-postgres*")))
    (if buffer
        (switch-to-buffer buffer)
      (let ((sql-postgres-program "psql")
            (sql-database "chill")
            (sql-server "localhost")
            (sql-user "postgres")
            (sql-product "postgres"))
        (sql-postgres "chill-postgres")
        (sql-set-sqli-buffer-generally)
        (sqlup-mode 1)
        (yas-minor-mode 1)))))

;;* Undercover

(defun th/undercover-psql ()
  (interactive)
  (let ((buffer (get-buffer "*SQL: undercover-postgres*")))
    (if buffer
        (switch-to-buffer buffer)
      (let ((sql-postgres-program "psql")
            (sql-database "undercover")
            (sql-server "localhost")
            (sql-user "postgres")
            (sql-product "postgres"))
        (sql-postgres "undercover-postgres")
        (sql-set-sqli-buffer-generally)
        (sqlup-mode 1)
        (yas-minor-mode 1)))))

(defun th/undercover-psql-prod ()
  (interactive)
  (let ((buffer (get-buffer "*SQL: undercover-postgres-prod*")))
    (if buffer
        (switch-to-buffer buffer)
      (let ((sql-database "undercover")
            (sql-server "localhost")
            (sql-port 45432)
            (sql-user "postgres")
            (sql-product "postgres"))
        (sql-postgres "undercover-postgres-prod")
        (sql-set-sqli-buffer-generally)
        (sqlup-mode 1)
        (yas-minor-mode 1)))))


;;* Vanlig

(defun vanlig ()
  "Start the vanlig experience"
  (interactive)
  (sly)
  (with-current-buffer (get-buffer "*sly-mrepl for sbcl*")
    (sly-mrepl--send-string "(cl:in-package :vanlig)")))

;;* Lyrics

(require 'lyrics)

;;* Email

;; http://cachestocaches.com/2017/3/complete-guide-email-emacs-using-mu-and-/#using-mu4e-to-send-mail
;; https://gist.github.com/areina/3879626

(setenv "MAILDIR" "$HOME/mail" t)

(use-feature smtpmail)
(use-feature starttls)
(use-feature smtpmail-multi
  :init
  (setq smtpmail-multi-accounts
        '((fastmail . ("lowe@thiderman.org"
                       "smtp.fastmail.com"
                       587
                       nil
                       starttls nil nil "fastmail.com"))))

  (setq smtpmail-multi-associations
        '(("lowe@thiderman.org" fastmail)))
  (setq smtpmail-multi-default-account 'fastmail)
  (setq message-send-mail-function 'smtpmail-multi-send-it))

(use-package pinentry
  :init
  (setq epa-pinentry-mode 'loopback))
(pinentry-start)

;; mu4e is installed from the `mu` AUR package.
(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")
(use-feature mu4e
  :bind
  ("C-x m" . mail-hydra/body)
  (:map mu4e-headers-mode-map
        ("i" . th/mu4e-select-inbox)
        ("TAB" . th/mu4e-toggle-unread)
        ("SPC" . th/mu4e-to-browser)
        ("q" . bury-buffer))
  (:map mu4e-view-mode-map
        ("SPC" . th/mu4e-to-browser))

  :init
  ;; This is to tell the renderer to chill with the making of
  ;; gray/gray HTML displays
  (setq shr-color-visible-luminance-min 10)

  (add-hook 'mu4e-view-mode-hook
            (lambda ()
              (local-set-key (kbd "<tab>") 'shr-next-link)
              (local-set-key (kbd "<backtab>") 'shr-previous-link)
              (toggle-truncate-lines -1)))

  (setq user-full-name "Lowe Thiderman")
  (setq mu4e-maildir "~/mail")
  (setq mu4e-headers-date-format "%Y-%m-%d")
  (setq mu4e-headers-time-format "%H:%M:%S")
  (setq mu4e-hide-index-messages t)
  (setq mu4e-display-update-status-in-modeline t)
  (setq mu4e-completing-read-function 'ivy-completing-read)
  (setq message-kill-buffer-on-exit t)
  (setq mu4e-confirm-quit nil)
  (setq mu4e-trash-folder #'th/mu4e-trash-for-msg)

  ;; I have my "default" parameters from fastmail
  (setq mu4e-sent-folder "/fastmail/Sent")
  (setq mu4e-drafts-folder "/fastmail/Drafts")
  (setq user-mail-address "lowe@thiderman.org")
  (setq smtpmail-default-smtp-server "smtp.fastmail.com")
  (setq smtpmail-default-smtp-server nil)
  (setq smtpmail-smtp-server nil)
  (setq smtpmail-smtp-server "smtp.fastmail.com")
  (setq smtpmail-smtp-service 587)
  (setq smtpmail-smtp-user nil)
  (setq smtpmail-stream-type nil)
  (setq smtpmail-debug-info t)
  (setq smtpmail-starttls-credentials (expand-file-name "~/.authinfo.gpg"))
  (setq smtpmail-auth-credentials (expand-file-name "~/.authinfo.gpg"))
  (setq message-send-mail-function 'smtpmail-send-it)
  (setq starttls-use-gnutls t))

;; Since the above doesn't seem to load mu4e properly, I'm doing this ugly
;; workaround just to get it to load properly on startup...
(with-temp-buffer
  (insert-file-contents "/usr/share/emacs/site-lisp/mu4e/mu4e.el")
  (eval-buffer))

(defvar th/mu4e-account-alist
  '(("fastmail"
     (mu4e-sent-folder "/fastmail/Sent")
     (mu4e-sent-folder "/fastmail/Drafts")
     (user-mail-address "lowe@thiderman.org")
     (smtpmail-local-domain "fastmail.com")
     (smtpmail-smtp-user "lowe@thiderman.org")
     (smtpmail-default-smtp-server "smtp.fastmail.com")
     (smtpmail-smtp-server "smtp.fastmail.com")
     (smtpmail-smtp-service 465))))

(defun th/mu4e-set-account ()
  "Set the account for composing a message.
   This function is taken from:
     https://www.djcbsoftware.nl/code/mu/mu4e/Multiple-accounts.html"
  (if-let* ((account
             (if mu4e-compose-parent-message
                 (let ((maildir (mu4e-message-field mu4e-compose-parent-message :maildir)))
                   (string-match "/\\(.*?\\)/" maildir)
                   (match-string 1 maildir))
               (completing-read (format "account: ")
                                (mapcar #'(lambda (var) (car var)) th/mu4e-account-alist)
                                nil t nil nil (caar th/mu4e-account-alist))))
            (account-vars (cdr (assoc account th/mu4e-account-alist))))
      (mapc #'(lambda (var)
                (set (car var) (cadr var)))
            account-vars)
    (error "No email account found")))
(add-hook 'mu4e-compose-pre-hook 'th/mu4e-set-account)

(defun th/mu4e-trash-for-msg (msg)
  "Returns the trash path for the given message."
  (let* ((mb (f-dirname (plist-get msg :maildir)))
         (dir (if (string= mb "/fastmail")
                  "Archive"
                "Trash")))
    (format "%s/%s" mb dir)))

(use-package mu4e-alert
  :after mu4e
  :init
  ;; (setq mu4e-alert-modeline-formatter)
  (setq mu4e-alert-interesting-mail-query
    "flag:unread maildir:/fastmail/INBOX")
  (mu4e-alert-enable-mode-line-display)
  (defun gjstein-refresh-mu4e-alert-mode-line ()
    (interactive)
    (mu4e~proc-kill)
    (mu4e-alert-enable-mode-line-display))
  (run-with-timer 0 60 'gjstein-refresh-mu4e-alert-mode-line))

(pretty-hydra-define mail-hydra
  (:color teal :quit-key "q" :title (with-faicon "envelope" "Email"))
  ("Reading"
   (("s-m" (mu4e~headers-jump-to-maildir "/fastmail/INBOX") "Fastmail")
    ("s-=" (mu4e) "start mu4e")
    ("s-g" (mu4e~headers-jump-to-maildir "/gmail/INBOX") "Gmail")
    ("s-i" th/mu4e-select-inbox "Select")
    ("s-SPC" mu4e~headers-jump-to-maildir "All mailboxes")
    ("s-u" (mu4e-headers-search "flag:unread AND NOT flag:trashed") "Unread"))

   "Writing"
   (("c" (mu4e-compose-new) "Compose"))
   "Getting"
   (("RET" th/get-mail "Get all"))))

;;;###autoload
(defun th/get-mail ()
  (interactive)
  "Get all the mailboxes and start tailing the logs"
  (start-process
   "offlineimap-oneshot"
   "*offlineimap-oneshot*"
   "systemctl"
   "--user"
   "start"
   "offlineimap-oneshot.service")
  (compile-dedicated
   "journalctl --user -fu offlineimap-oneshot"
   "*mail-tail*"))

;;;###autoload
(defun th/mu4e-select-inbox ()
  "Select an inbox to go to"
  (interactive)
  (mu4e~headers-jump-to-maildir
   (ivy-completing-read "inbox: "
                    (cl-remove-if-not (lambda (s) (s-contains? "INBOX" s))
                                      (mu4e-get-maildirs)))))

;;;###autoload
(defun th/mu4e-to-browser ()
  "Open the current email in a browser window."
  (interactive)
  (shell-command
   (format "eml2browser %s"
           (plist-get (mu4e-message-at-point) :path))))

;;;###autoload
(defun th/mu4e-toggle-unread ()
  "Toggle between showing unread or not."
  (interactive)
  (let* ((query mu4e~headers-last-query)
        (flag " AND flag:unread")
        (final
         (if (s-suffix? flag query)
             (s-chop-suffix flag query)
           (concat mu4e~headers-last-query flag))))
    (mu4e-headers-search final)))

;;* File browsing and toggling
;;;* Alternate

(defun th/other-files (&optional pattern default)
  "Browse between files matching a pattern in the current project.

Defaults to the file suffix of the current buffer if none is given.

E.g. if you are visiting a .go file, this will list all other .go files."

  (interactive)

  (find-file
   (concat
    (projectile-project-root)
    (let* ((bn (buffer-file-name))
           ;; The pattern, the suffix, or the filename
           ;; This is to make Makefiles and Dockerfiles work,
           (pat (or pattern (f-ext bn) (f-base bn)))
           (files (-filter
                   (lambda (x)
                     (s-contains? pat x))
                   (projectile-dir-files-alien (projectile-project-root)))))

      (case (length files)
       (1
        (car files))
       (0
        (if (null default)
            (error (format "No files matching '%s' and no default given" pat))
          (when (yes-or-no-p (format "No files matching '%s'. Create '%s'?"
                                     pat default))
            default)))
       (t
        (completing-read
         (format "%s files: " pat)
         files)))))))

(defun th/other-files-same-base ()
  "Find other files that have the same base as the current
one. Complete if there are multiple found.

E.g. if you are visiting `user.go' and `User.vue' exists, visit
that. If there is also a `UserPanel.vue', start completion
between the matching files instead.

This is useful if you have backend and frontend code in the same repo."

  (interactive)

  (let* ((base (f-base (buffer-file-name)))
         (files (-filter
                 (lambda (x)
                   (and
                    (s-prefix? (downcase base)
                               (downcase (f-base (f-filename x))))
                    ;; Filter out test files, backup files and the current file
                    (not (s-contains? "test" x))
                    (not (s-contains? ".#" x))
                    (not (s-contains? x (buffer-file-name)))))
                 (projectile-dir-files-alien (projectile-project-root)))))
    (cond ((= (length files) 1)
           (find-file (car files)))

          ((> (length files) 1)
           (find-file
            (completing-read "Alt files: " files)))

          (t
           (error "No alternate files for %s" (buffer-name))))))

(defun th/browse-extensions ()
  "Select a file extension from the project and browse files with that extension."
  (interactive)
  (let* ((suffixes
          (remove nil
                  (-distinct
                   (-map (lambda (x) (format ".%s" (f-ext x)))
                         (projectile-dir-files-alien (projectile-project-root)))))))
    (th/other-files
     (completing-read "suffixes: " suffixes nil t))))

;;;* Quickfast

(defun list-major-modes (&optional modes)
  "Returns list of potential major mode names (without the final -mode).
Note, that this is guess work."
  ;; https://stackoverflow.com/a/19165202/983746
  (interactive)
  (let (l)
    (mapatoms
     #'(lambda (f) (and
               (commandp f)
               (string-match "-mode$" (symbol-name f))
               (if modes
                   (let ((parent f))
                     (while (and (not (memq parent modes))
                                 (setq parent (get parent 'derived-mode-parent))))
                     parent)
                 t)
               ;; auto-loaded
               (or (and (autoloadp (symbol-function f))
                        (let ((doc (documentation f)))
                          (when doc
                            (and
                             (let ((docSplit (help-split-fundoc doc f)))
                               (and docSplit ;; car is argument list
                                    (null (cdr (read (car docSplit)))))) ;; major mode starters have no arguments
                             (if (string-match "[mM]inor" doc) ;; If the doc contains "minor"...
                                 (string-match "[mM]ajor" doc) ;; it should also contain "major".
                               t) ;; else we cannot decide therefrom
                             ))))
                   (null (help-function-arglist f)))
               (setq l (cons (substring (symbol-name f) 0 -5) l)))))
    (when (called-interactively-p 'any)
      (with-current-buffer (get-buffer-create "*Major Modes*")
        (let ((standard-output (current-buffer)))
          (display-completion-list l)
          (display-buffer (current-buffer)))))
    (sort l 'string<)))

(defun th/quickmajor (p)
  "Create a quick buffer set in a major mode.

By default only presents a selection of modes derived from `prog-mode'
or `text-mode'.

With a single universal argument, the major mode of the current buffer
is used.

With a double universal argument, all major modes are shown."
  (interactive "p")
  (let* ((derived (unless (= p 16) '(prog-mode text-mode)))
         (modes (list-major-modes derived))
         (mode (if (= p 4)
                   (symbol-name major-mode)
                 (completing-read "Create major mode buffer: " modes nil t))))
    (split-window-sensibly)
    (switch-to-buffer (format "*%s*" mode))
    (funcall (intern
              (if (not (s-suffix? "-mode" mode))
                  (format "%s-mode" mode)
                mode)))))

(defun list-minor-modes ()
  "Returns list of currently active minor modes."
  (interactive)
  ;; https://stackoverflow.com/a/41709866/983746
  (sort
   (delq nil
         (mapcar
          (lambda (x)
            (let ((car-x (car x)))
              (when (and (symbolp car-x) (symbol-value car-x))
                (car x))))
          minor-mode-alist))
   'string<))

(defun th/toggle-minor-mode ()
  "Presents a selection of minor modes as to toggle one of them."
  (interactive)
  (let* ((active-modes (list-minor-modes))
         (modes (-map (lambda (x) (format "%s%s" (if (-contains? active-modes x) "-" "+") x))
                      minor-mode-list))
         (item (completing-read "minor mode (enable +mode or disable -mode): "
                                (sort modes 'string<) nil t "^- "))
         (enable (if (s-prefix? "+" item) 1 -1))
         (mode (substring item 1)))
    (funcall (intern mode) enable)
    (message "%s %s" mode (if enable "enabled" "disabled"))))

;;;###autoload
(defun th/toggle-minibuffer-prefix ()
  "Toggle between \"^+\" and \"^-\" in minibuffers."
  (interactive)
  (save-excursion
    (beginning-of-line)
    (when (looking-at "\\^")
      (forward-char 1)
      (cond
       ((looking-at "+")
        (delete-char 1)
        (insert "-"))
       ((looking-at "-")
        (delete-char 1)
        (insert "+"))))))

(defun th/toggle-minibuffer-prefix-set-key ()
  (local-set-key (kbd "C-c C-c") #'th/toggle-minibuffer-prefix))

(add-hook 'minibuffer-setup-hook #'th/toggle-minibuffer-prefix-set-key)

;;* Org

;;;* Base org

(straight-use-package '(org :type built-in))
(straight-use-package '(org-agenda :type built-in))
(straight-use-package '(org-macs :type built-in))

(use-feature org
  :bind (:map org-mode-map
              ;; I accidentally hit this one quite a lot, and the `pcomplete'
              ;; bullshit sucks.
              ("C-M-i" . org-cycle)

              ("C-c C-x C-a" . th/org-archive-done-tasks)
              ("C-c d" . th/org-add-cookie)
              ("C-c h" . th/org-toggle-html-export-on-save)
              ("C-c C-v" . th/toggle-hydra/body)
              ("C-x n" . th/narrow-or-widen-dwim)

              ("C-c o" . th/org-open-flat-block)
              ("C-c w" . th/org-tag-as-work)
              ("C-c b" . th/org-tag-as-book)
              ("C-c C-<return>" . org-insert-todo-heading))

  :hook
  ((org-after-todo-statistics . th/org-summary-todo)
   ;; (org-mode . visual-line-mode)
   )

  :config
  (setq org-archive-location "~/org/archive/%s::")
  (setq org-blank-before-new-entry '((heading . nil)
                                     (plain-list-item . nil)))
  (setq org-confirm-babel-evaluate nil)
  (setq org-directory "~/org")
  (setq org-ellipsis " ▼ ")
  (setq org-export-with-section-numbers nil)
  (setq org-export-with-email t)
  (setq org-fontify-emphasized-text t)
  (setq org-return-follows-link t)
  (setq org-hide-leading-stars t)
  (setq org-special-ctrl-a/e t)
  (setq org-special-ctrl-k t)
  (setq org-src-fontify-natively t)
  (setq org-src-tab-acts-natively t)
  (setq org-src-window-setup 'current-window)
  (setq org-use-speed-commands t)
  (setq org-use-tag-inheritance nil)
  (setq org-imenu-depth 5)
  (setq org-hierarchical-checkbox-statistics t)
  (setq org-M-RET-may-split-line '((default nil)))
  (setq org-startup-folded nil)
  (setq org-reverse-note-order t)
  (setq org-clock-idle-time nil)
  (setq org-tag-faces nil)
  (setq org-tags-column 70)
  (setq org-hide-emphasis-markers t)
  (setq org-log-done t)
  (setq org-refile-targets
        '(("~/org/gtd.org" :maxlevel . 3)
          ("~/org/someday.org" :level . 1)
          ("~/org/tickler.org" :maxlevel . 2)))
  (setq org-capture-templates
        `(("t" "Todo [inbox]" entry
           (file+headline "~/org/inbox.org" "Inbox")
           "* TODO %i%?")

          ("T" "Tickler" entry
           (file+headline "~/org/tickler.org" "Tickler")
           "* TODO %i%? \n %U")

          ("w" "Track weight" entry
           (file+headline "~/org/irl.org" "Weight")
           "* %T %^{Current weight} kg"
           :immediate-finish t)

          ("l" "Line" entry
           (file "~/org/code.org")
           "* [[file://%F::%(th/org-capture-current-line)][%^{Description}]]")))


  (setq org-todo-keywords
        '("TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)"))

  (setq org-todo-keyword-faces
        '(("NEXT" :foreground "#79740E" :weight bold)
          ("WAITING" :foreground "#3C3246" :weight bold)))

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (sql . t)
     (shell . t)))

  ;; TODO(thiderman): `flet' is deprecated and `cl-flet' or `cl-letf' should be used
  ;; instead, but neither of them actually work, even when seemingly adapted.
  ;; So, to mitigate warnings, we just disable all bytecomp warnings as this happens.
  (let ((byte-compile-warnings '(not obsolete)))
    (defadvice org-capture-place-template (around org-capture-without-breaking-windows activate)
      "Advice that doesn't move around windows when doing capture."
      ;; Override (dow), since it's the only destructive window operation in the
      ;; whole flow of `org-capture-place-template'
      (flet ((delete-other-windows (&optional window) nil))
        ad-do-it))

    (defadvice org-add-log-note (around org-add-log-note-without-breaking-windows activate)
      "Advice that doesn't move around windows when adding notes."
      (flet ((delete-other-windows (&optional window) nil))
        ad-do-it)))

  (use-package real-auto-save
    :init
    (setq real-auto-save-interval 10)
    :hook
    (org-mode . real-auto-save-mode))

  (defun th/org-project ()
    "Go to the org project for the current repository.

Go back if we're already in it."

    (interactive)
    (let* ((root (projectile-project-root))
           (name (car (last (s-split "/" (projectile-project-root)) 2))))
      (if (s-equals? (expand-file-name "~/org/") root)
          (progn
            (save-buffer)
            (previous-buffer))
        (find-file
         (format "~/org/%s.org" name)))))

  (defun th/org-archive-done-tasks ()
    (interactive)
    (org-map-entries
     (lambda ()
       (org-archive-subtree)
       (setq org-map-continue-from (outline-previous-heading)))
     "/DONE|CANCELLED" 'file)
    (save-buffer)
    (message "Done tasks archived"))

  (defun th/org-add-cookie (arg)
    "Adds a statistics cookie to the current heading"
    (interactive "P")
    (save-excursion
      (org-end-of-line)
      (if (looking-back "]")
          ;; Already exists, kill it
          (progn
            (backward-list)
            (backward-char)
            (org-kill-line))

        (insert (format " [%s]" (if arg "%" "/")))
        (org-ctrl-c-ctrl-c))))

  (defun th/org-open-flat-block ()
    (interactive)
    (org-end-of-line)
    (open-line 3)
    (forward-line 2)
    (beginning-of-line)
    (indent-according-to-mode))

  (defun th/org-tag-as-work ()
    (interactive)
    (org-toggle-tag "tink")
    (org-fix-tags-on-the-fly))

  (defun th/org-tag-as-book ()
    (interactive)
    (org-toggle-tag "tink")
    (org-toggle-tag "book")
    (org-fix-tags-on-the-fly))

  (defun th/org-align-tags ()
    "Simple helper to align tags, used as a before-save-hook."
    (interactive)
    (when (eq major-mode 'org-mode)
      (org-align-all-tags)))

  (add-hook 'before-save-hook #'th/org-align-tags)

  (defun th/org-summary-todo (n-done n-not-done)
    "Switch entry to DONE when all subentries are done, to TODO otherwise."
    (let (org-log-done org-log-states)  ; turn off logging
      (org-todo (if (and (not (= n-done 0)) (= n-not-done 0)) "DONE" "TODO"))))

  (defun th/org-current-is-todo ()
    (string= "TODO" (org-get-todo-state)))

  (defun th/org-capture-current-line ()
    (interactive)
    (with-current-buffer
        (org-capture-get :original-buffer)
      (number-to-string
       (line-number-at-pos))))

  (defun th/org-toggle-html-export-on-save ()
    (interactive)
    (if (memq 'org-html-export-to-html after-save-hook)
        (progn
          (remove-hook 'after-save-hook 'org-html-export-to-html t)
          (message "Disabled org html export on save for current buffer..."))
      (add-hook 'after-save-hook 'org-html-export-to-html nil t)
      (message "Enabled org html export on save for current buffer..."))))

;;;* org-agenda

(use-feature org-agenda
  :bind (:map org-agenda-mode-map
              ("i" . th/clock-in-from-agenda)
              ("o" . th/clock-out-from-agenda)
              ("c" . th/org-agenda-capture)
              ("s" . org-agenda-schedule))

  :hook (org-agenda-mode . writeroom-mode)

  :init
  (setq org-agenda-files
        '("~/org/inbox.org"
          "~/org/gtd.org"
          "~/org/tickler.org"))

  (setq org-agenda-ndays 7)
  (setq org-agenda-show-all-dates t)
  (setq org-agenda-block-separator ?-)
  (setq org-agenda-start-on-weekday 1)
  (setq org-agenda-window-setup 'current-window)
  (setq org-agenda-custom-commands nil)

  :config
  (defun th/org-agenda-open ()
    "Open the agenda, switch to it if it already exists"
    (interactive)
    (org-agenda nil "a"))

  (defun org-agenda-skip-all-siblings-but-first ()
    "Skip all but the first non-done entry."
    (let (should-skip-entry)
      (unless (th/org-current-is-todo)
        (setq should-skip-entry t))
      (save-excursion
        (while (and (not should-skip-entry) (org-goto-sibling t))
          (when (th/org-current-is-todo)
            (setq should-skip-entry t))))
      (when should-skip-entry
        (or (outline-next-heading)
            (goto-char (point-max))))))

  (defun th/clock-in-from-agenda ()
    "Clock in from an agenda buffer"
    (interactive)
    (save-window-excursion
      (org-agenda-switch-to)
      (org-clock-in)
      (let ((inhibit-message t))
        (org-save-all-org-buffers))))

  (defun th/clock-out-from-agenda ()
    "Clock out from an agenda buffer"
    (interactive)
    (org-clock-out)
    (let ((inhibit-message t))
      (org-save-all-org-buffers)
      (org-agenda-redo-all)))

  (defun th/org-agenda-capture ()
    (interactive)
    (org-capture :keys "t")))

;;;* org-journal

(use-package org-journal
  :init
  (setq org-journal-dir "~/org/journal/")
  (setq org-journal-file-format "%Y-%m-%d")
  (setq org-journal-date-format "%A, %Y-%m-%d")
  (setq org-journal-find-file 'find-file))

;; (use-package org-sidebar
;;   :straight (:host github :repo "alphapapa/org-sidebar"))

;;;* worf and org-gtd

(use-package worf
  :hook
  (org-mode . worf-mode)

  :blackout
  (worf-mode . "")

  :config
  ;; This is to override the functionality
  (setq th/org-gtd-files '("inbox.org"
                           "gtd.org"
                           "someday.org"
                           "tickler.org"))
  (defun worf-reserved ()
    "Switch between GTD files"
    (interactive)
    (let* ((fn (format "%s.org" (file-name-base
                                 (or (buffer-file-name)
                                     "hehe"))))
           ;; Add one to the current position in the list - we want the next
           ;; file, not the current one
           (pos (+ 1 (or (cl-position fn th/org-gtd-files :test #'string-equal)
                         -1)))
           ;; If the next position is not within bounds - go back to zero
           (idx (if (or (< pos 0)
                        (>= pos (length th/org-gtd-files)))
                    0
                  pos)))
      (find-file (format "~/org/%s" (nth idx th/org-gtd-files)))
      (beginning-of-buffer)
      (org-global-cycle 2)))

  (defun worf-refile-gtd ()
    "Refiles with the targets just set to the top-level headings"
    (interactive)
    (let ((org-refile-targets
           '(("gtd.org" :maxlevel . 1)
             ("someday.org" :maxlevel . 1)
             ("tickler.org" :maxlevel . 1))))
      (org-refile)))

  (defhydra+ hydra-refile ()
    "
Refile:^^   _k_eep: %`org-refile-keep
----------------------------------
_l_ast      _a_rchive
_o_ther     _d_one
_t_his      _e_nd
_r_: gtd

"
    ("r" worf-refile-gtd)
    ("d" th/org-archive-done-tasks)))


(defun th/org-show-project (name)
  "Show a narrowed project tree in gtd.org"
  (find-file "~/org/gtd.org")
  (widen)
  (beginning-of-buffer)
  (search-forward (format "** %s" name))
  (beginning-of-line)
  (org-narrow-to-subtree)
  (org-global-cycle 3))


;;;* org exporters
;;;;* ox-reveal

(use-package ox-reveal
  :config
  (setq org-reveal-root "https://cdn.jsdelivr.net/reveal.js/3.0.0/"))

;;;;* ox-hugo

(use-package ox-hugo
  :after ox)

;;;* calfw

(use-package calfw
  :init
  (setq calendar-week-start-day 1)
  (setq cfw:fchar-junction ?╋
        cfw:fchar-vertical-line ?┃
        cfw:fchar-horizontal-line ?━
        cfw:fchar-left-junction ?┣
        cfw:fchar-right-junction ?┫
        cfw:fchar-top-junction ?┯
        cfw:fchar-top-left-corner ?┏
        cfw:fchar-top-right-corner ?┓))
(use-package calfw-org)

(defhydra th/org-hydra (:exit t :idle 0.5 :columns 3)
  "Org commands"
  ("s-o" (org-capture :keys "t") "inbox capture")
  ("s-c" org-capture "template capture")
  ("s-w" (org-tags-view t "tink") "work tasks")
  ("s-t" (org-tags-view t "tink") "work tasks")
  ("s-p" (org-tags-view t (projectile-project-name)) "project tagged")
  ("s-d" (org-tags-view t "drunkenfall") "drunkenfall")
  ("s-f" (let ((default-directory org-directory)) (counsel-find-file)) "files")
  ("s-s" th/org-agenda-open "schedule")
  ("s-i" (find-file "~/org/inbox.org") "inbox")
  ("s-j" org-clock-jump-to-current-clock "jump to clocked")
  ("s-l" org-store-link "store link")
  ("s-SPC" worf-reserved "switch file")
  ("c" calendar "calendar")
  ("w" (org-capture :keys "w") "weight")
  ("t" (org-capture :keys "T") "tickler")
  ("d" org-journal-new-entry "diary")
  ("s-a" org-todo-list "all")
  ("s-q" org-tags-view "tags")
  ("s-v" cfw:open-org-calendar "visual")
  ("q" nil))

;;* Zettelkasten

(use-package deft
  :init
  (setq deft-extensions '("org" "md" "txt"))
  (setq deft-use-filename-as-title t))

(use-package zetteldeft
  :after deft
  :config
  (zetteldeft-set-classic-keybindings)
  (setq zetteldeft-link-indicator "§")
  (setq zetteldeft-id-format "%Y-%m-%d-%H:%M")
  (setq zetteldeft-id-regex "[0-9]\\{4\\}\\(-[0-9]\\{2,\\}\\)\\{3\\}")
  (setq zetteldeft-tag-regex "[#@][a-z-]+"))

(pretty-hydra-define th/zetteldeft
  (:color teal :idle 0.5 :title (with-faicon "book" "Zetteldeft"))
  ("Main"
   (("s-r" zetteldeft-follow-link "Follow")
    ("s-o" zetteldeft-find-file "Open")
    ("s-c" zetteldeft-new-file "New")
    ("s-n" zetteldeft-new-file-and-link "New"))
   "Search"
   (("s-s" deft "Search"))))

;;* Media
;;;* Spotify

(use-package counsel-spotify
  :bind ("s-u" . spotify-hydra/body)
  :init
  (setq counsel-spotify-use-notifications nil))

(defhydra spotify-hydra (:exit t :idle 0.5)
  "Spotify"
  ("s-u" counsel-spotify-toggle-play-pause "play/pause")
  ("s-p" counsel-spotify-next "next")
  ("s-n" counsel-spotify-previous "prev"))

;;* Sql

(require 'sql)

(setenv "PGHOST" "localhost")
(setenv "PGUSER" "postgres")

(setq-default sql-product "postgres")
(bind-key "C-c C-k" 'comint-clear-buffer comint-mode-map)

(use-package sqlup-mode
  :hook ((sql-mode . sqlup-mode)
         (sql-mode . yas-minor-mode-on)))

(defadvice sql-execute (after sql-mode-for-table-buffers activate)
  "Sets `sql-mode' in result buffers so they get fontification"
  (sql-mode))

(defun th/sql-list-table ()
  "Show the list table entry for the table under point"
  (interactive)
  (sql-list-table (thing-at-point 'symbol)))

(bind-key "C-c C-d" 'th/sql-list-table sql-mode-map)
(bind-key "C-c C-d" 'th/sql-list-table comint-mode-map)

(defadvice sql-send-paragraph (before save-sql-send-paragraph activate)
  "Save the buffer before sending the paragraph to the SQL buffer."
  (save-buffer))

(defun th/execute-printed-sql ()
  "Execute the logged SQL statement by sending it to the SQLi buffer"
  (interactive)
  (save-excursion
    (beginning-of-line)
    (search-forward " ")
    (sql-send-region
     (point)
     (progn (end-of-line) (point)))))

(bind-key "C-c C-c" 'th/execute-printed-sql compilation-mode-map)

(sql-set-product-feature 'mysql :prompt-regexp "^\\(MariaDB\\|MySQL\\) \\[[_a-zA-Z()]*\\]> ")

;;* Things that should be packaged as packages
(require 'enved)
(require 'weby)
(require 'overlay-expander)
(require 'exwm-status)
(require 'vue-buffers)
(require 'project-hydra)

;;* System things

;; Dunst silencer

(defvar th/dunst-silenced nil
  "Set to `t' if we have sent the special message to Dunst to make it silent")

(defun th/toggle-dunst ()
  "Silence or unsilence notifications from Dunst."
  (interactive)
  (setq th/dunst-silenced (not th/dunst-silenced))
  (shell-command (format "notify-send %s" (if th/dunst-silenced
                                              "DUNST_COMMAND_PAUSE"
                                            "DUNST_COMMAND_RESUME")))
  (message "dunst-silenced: `%s'" th/dunst-silenced))

;;* Hosts
;;;* silverwing

(defun th/silverwing ()
  "Open a tmux on silverwing"
  (interactive)
  (th/exwm-terminal
   "ssh sw -t TERM=xterm-256color '/usr/bin/tmux -u new -A -s main'"
   "silverwing"))

(defun th/silverwing-workspace ()
  "Switch to the silverwing workspace"
  (interactive)
  (th/ew/switch "silverwing"))

(defun th/send-torrents ()
  "Send torrents (of Linux distributions, ofc) to the server."
  (interactive)
  (let* ((server "sw")
         (dir (f-join (getenv "HOME") "tmp"))
         (files (-filter (lambda (f) (f-ext? f "torrent"))
                         (directory-files dir))))
    ;; Do the actual uploading
    (shell-command (format "scp ~/tmp/*.torrent %s:/nest/warez/incoming/" server))
    ;; Remove the torrents from here
    (shell-command "rm ~/tmp/*.torrent")
    ;; Make a nice popup
    (alert (s-join "\n" (-map (lambda (s) (concat (substring s 0 30)
                                             "..."))
                              files))
           :title (format "%d torrents uploaded" (length files)))))


(pretty-hydra-define th/silverwing-hydra
  (:color teal :quit-key "q" :title (with-faicon "fire" "silverwing" 1 -0.05))
  ("Main"
   (("RET" th/silverwing "terminal")
    ("s--" th/send-torrents "torrents"))))

;;* Lastly

(require 'th-abbrev)

;;;* Last

(diminish 'abbrev-mode)
(diminish 'auto-fill-function)
(diminish 'auto-revert-mode)
(diminish 'compilation-in-progress)
(diminish 'eldoc-mode)
(diminish 'projectile-mode)

;; Some things seem to override some of the face things, most notably the TODO
;; thing. This overrides this.
(load-theme 'nightmare t)

(use-package tink
  :straight (tink :type git :host github :repo "thiderman/tink.el")
  :init
  (setq tink/terminal-launcher #'th/exwm-terminal))

;;;* EXWM

(use-package exwm
  :straight (exwm :type git :host github :repo "ch11ng/exwm"))

(use-package exwm-edit
  :after exwm)

(server-start)

(setq exwm-workspace-number 24)
(setq exwm-workspace-warp-cursor t)

;; All buffers created in EXWM mode are named "*EXWM*". You may want to change
;; it in `exwm-update-class-hook' and `exwm-update-title-hook', which are run
;; when a new window class name or title is available. Here's some advice on
;; this subject:
;; + Always use `exwm-workspace-rename-buffer` to avoid naming conflict.
;; + Only renaming buffer in one hook and avoid it in the other. There's no
;;   guarantee on the order in which they are run.
;; + For applications with multiple windows (e.g. GIMP), the class names of all
;;   windows are probably the same. Using window titles for them makes more
;;   sense.
;; + Some application change its title frequently (e.g. browser, terminal).
;;   Its class name may be more suitable for such case.
(defun th/exwm-update-class-hook ()
  (cond
   ((and (string= th/browser-name exwm-class-name)
         th/browser-session)
    (exwm-workspace-rename-buffer th/browser-session)
    (setq th/browser-session nil))
   ((and (string= th/terminal exwm-class-name)
         th/terminal-session)
    (exwm-workspace-rename-buffer th/terminal-session)
    (setq th/terminal-session nil))
   ((string= "gimp" exwm-instance-name)
    t)
   (t
    (exwm-workspace-rename-buffer exwm-class-name))))
(add-hook 'exwm-update-class-hook #'th/exwm-update-class-hook)

(add-hook 'exwm-update-title-hook
          (lambda ()
            (when (or (not exwm-instance-name)
                      (string= "gimp" exwm-instance-name)
                      (string= "qutebrowser" exwm-instance-name))
              (exwm-workspace-rename-buffer exwm-title))))

;; Set input mode to be char for terminals
(setq exwm-manage-configurations
      '(((string= "terminal" exwm-instance-name)
         char-mode t)
        ((string= "discord" exwm-instance-name)
         char-mode t)
        (t
         char-mode t)))

(setq exwm-workspace-show-all-buffers t)
(setq exwm-layout-show-all-buffers t)

;; + Application launcher ('M-&' also works if the output buffer does not
;;   bother you). Note that there is no need for processes to be created by
;;   Emacs.
(defun exwm-execute (command)
  (interactive
   (list (read-shell-command "$ ")))
  (start-process-shell-command command nil command))

;;;###autoload
(defun th/golden-split ()
  "Splits the current window into two, at a golden-ratio like ratio"
  (interactive)
  (delete-other-windows)
  ;; Add one fifth to make it go from 1/2 to 1/3, ish
  (let* ((width (/ (window-width) 5))
         (buffer (current-buffer)))
    (split-window-right)
    (when (oddp exwm-workspace-current-index)
      (other-window 1))
    (enlarge-window-horizontally width)
    (set-frame-parameter nil 'th/prohibit-balance t)
    (when (oddp exwm-workspace-current-index)
      (other-window 1)
      (th/switch-to-previous-buffer))
    (switch-to-buffer buffer)))

(defvar th/terminal "alacritty")

(defvar keyboard-layouts ()
  "Symbols for keyboard variables and defuns")

(defmacro define-keyboard-switch (layout)
  "Defines a keyboard layout switcher function.

Also sets up internal variables so that the defined function also has a variable
that's set to `t' whenever the layout in question is enabled. This is to make
the `th/toggle-hydra' work with keyboard layouts."
  (let* ((name (symbol-name layout))
         (symbol (format "keyboard-%s-mode" name))
         (defun-name (intern symbol)))
    (add-to-list 'keyboard-layouts defun-name)
    (set defun-name nil)
    `(defun ,defun-name ()
       (interactive)
       ;; First, set all of the keyboard layouts to false, except for the one we
       ;; are switching to.
       (dolist (kb keyboard-layouts)
         (set kb (string-equal (symbol-name kb)
                               ,symbol)))
       ;; Execute the shell command to actually switch keyboard layouts
       (shell-command (format "keyboard-setup %s" ,name))
       (message "%s loaded" ,name))))

(define-keyboard-switch qwerty-a6-us)
(define-keyboard-switch us)

;; The default layout should always be the US one
(keyboard-us-mode)

(defvar th/terminal-session "main")

(defun th/exwm-terminal (&optional command title)
  "Open a terminal with a dedicated command"
  (interactive)
  (let ((bufname (format "terminal<%s>" (or title command))))
    ;; TODO(thiderman): This could probably be rewritten to be less clunky
    (when (not command)
      ;; If we don't have any command, assume that we want a workspace-specific tmux
      (setq command (format "tmux -u new -A -s %s" th/ew/current))
      (setq title th/ew/current)
      (setq bufname (format "terminal<%s>" title)))

    (if (get-buffer bufname)
        (switch-to-buffer bufname)
      (setq th/terminal-session bufname)
      (exwm-execute
       (format "%s -e %s" th/terminal command)))))

;;;###autoload
(defun th/spotify ()
  (interactive)
  (if (not (get-buffer "Spotify"))
      (exwm-execute "spotify")
    (if (string-equal (buffer-name) "Spotify")
        (previous-buffer)
      (switch-to-buffer "Spotify"))))

;;;###autoload
(defun th/steam ()
  (interactive)
  (if (not (get-buffer "Steam"))
      (progn
        (message "Starting steam...")
        (exwm-execute "steam"))
    (if (string-equal (buffer-name) "Steam")
        (previous-buffer)
      (switch-to-buffer "Steam"))))

;;;###autoload
(defun th/discord ()
  (interactive)
  (if (executable-find "discord")
      ;; If we have discord installed, do the application switch
      (if (not (get-buffer "discord"))
          (progn
            (message "Starting discord...")
            (exwm-execute "discord"))
        (if (string-equal (buffer-name) "discord")
            (previous-buffer)
          (switch-to-buffer "discord")))
    ;; If it's not installed - use the browser version
    (browser "discord")))

(defun th/muted-p ()
  (let ((muted (shell-command-to-string "pulsemixer --get-mute")))
    (string-equal (s-trim muted) "1")))

(defvar th/toggle-mute nil)
(defun th/volume (action)
  (interactive)
  (let ((command
         (cond
          ((eq action 'up) "--change-volume=+5")
          ((eq action 'down) "--change-volume=-5")
          ((eq action 'toggle) "--toggle-mute"))))
    (shell-command-to-string
     (format "vol %s" command)))
  (setq th/toggle-mute (th/muted-p)))

;; The following example demonstrates how to set a key binding only available
;; in line mode. It's simply done by first push the prefix key to
;; `exwm-input-prefix-keys' and then add the key sequence to `exwm-mode-map'.
;; The example shorten 'C-c q' to 'C-q'.
(push ?\C-q exwm-input-prefix-keys)
(define-key exwm-mode-map [?\C-q] #'exwm-input-send-next-key)

;; The following example demonstrates how to use simulation keys to mimic the
;; behavior of Emacs. The argument to `exwm-input-set-simulation-keys' is a
;; list of cons cells (SRC . DEST), where SRC is the key sequence you press and
;; DEST is what EXWM actually sends to application. Note that SRC must be a key
;; sequence (of type vector or string), while DEST can also be a single key.
;; (exwm-input-set-simulation-keys
;;  '(([?\C-b] . left)
;;    ([?\C-f] . right)
;;    ([?\C-p] . up)
;;    ([?\C-n] . down)
;;    ([?\C-a] . home)
;;    ([?\C-e] . end)
;;    ([?\M-v] . prior)
;;    ([?\C-v] . next)
;;    ([?\C-d] . delete)))
(exwm-input-set-simulation-keys nil)

;; You can hide the mode-line of floating X windows by uncommenting the
;; following lines
(add-hook 'exwm-floating-setup-hook #'exwm-layout-hide-mode-line)
(add-hook 'exwm-floating-exit-hook #'exwm-layout-show-mode-line)

;; You can hide the minibuffer and echo area when they're not used, by
;; uncommenting the following line
(setq exwm-workspace-minibuffer-position nil)

(defun exwm-randr-dragonisle ()
  (start-process-shell-command
   "xrandr" nil "xrandr --output DP-0 --right-of HDMI-0 --auto"))

(defun th/xrandr (&rest commands)
  (let ((command (format "xrandr --verbose --output %s" (s-join " --output " commands))))
    (message command)
    (start-process-shell-command "xrandr" "*xrandr*" command)))

(defun th/exwm-randr-hook ()
  (if (s-equals? (system-name) "dragonisle")
      (th/xrandr "DP-0 --right-of HDMI-0 --auto"
                 "HDMI-0 --auto")
    (let ((screens (th/get-connected-screens)))
      (message "Screens: %s" screens)
      (cond ((= 3 (length screens))
             ;; If we have three screens connected, that means that the laptop
             ;; screen is on and the two monitors are attached. Disable the
             ;; laptop screen and align the other two.
             (message "Triple screens")
             (let ((left "DP2")
                   (right "HDMI1"))
               (th/xrandr
                ;; First, disable the laptop screen
                (format "%s --off" (nth 0 screens))
                (format "%s --auto" left)
                (format "%s --right-of %s --auto" right left))
               (setq screens (list left right))))

            ;; If we have two screens connected, it means we want to
            ;; show what's going on; mirror the laptop screen to the HDMI
            ((= 2 (length screens))
             (message "Two screens; setting up laptop and screen")
             (let ((laptop (nth 0 screens))
                   (screen (nth 1 screens)))
               (th/xrandr (format "%s --auto" laptop)
                          (format "%s --auto --right-of %s" screen laptop))
               (setq screens (list laptop screen))))
            ;; Just a laptop screen
            (t
             (message "One screen; laptop only")
             (th/xrandr (format "%s --auto" (nth 0 screens))
                        (format "%s --off" (nth 1 screens)))
             (setq screens '("eDP1"))))

      (setq th/ew/screens screens)
      (setq exwm-randr-workspace-output-plist (th/ew/plist screens)))))

(defun th/get-connected-screens ()
  "Returns a list of the screens xrandr reports as connected"
  (mapcar
   (lambda (s)
     ;; Hehe, ugly, but works since the outputs are always the first
     ;; thing on every row.
     (s-replace-regexp " .*" "" s))
   (s-split
    "\n"
    (s-trim (shell-command-to-string
             "xrandr --query | grep \" connected \"")))))

(require 'exwm-randr)

;;;;* EXWM workspace

(defhydra th/ew/hydra (:exit t :columns 3)
  "exwm"
  ("s-SPC" exwm-execute "exec")
  ("s-q" (exwm-execute "firefox") "firefox")
  ("s-RET" (exwm-execute "terminal") "terminal")
  ("s-c" (th/exwm-terminal "ctop") "ctop")
  ("s-s" th/silverwing "silverwing")
  ("s-d" (exwm-execute "discord") "discord")
  ("s-h" (th/exwm-terminal "htop") "htop")
  ("s-i" th/toggle-dunst "notifications")
  ("s-g" (th/golden-split) "golden")
  ("s-p" (th/exwm-terminal "pulsemixer") "pulsemixer")
  ("s-b" th/toggle-prohibit-balance "balance toggle")
  ("s-r" th/ew/rename "rename")
  ("s-t" transpose-frame "transpose")
  ("s-w" th/ew/switch "switch")
  ("s-k" th/ew/remove "remove")
  ("s-x" exwm-randr-refresh "xrandr"))

(defvar th/ew/current "main")
(defvar th/ew/screens '("HDMI-0"))
(defvar th/ew/screens-per-workspace 2)
(defvar th/ew/workspaces '("main" "messaging" "mail"))
(defvar th/ew/default-workspaces '("main" "messaging" "mail"))
(defvar th/ew/last-dev nil)
(defvar th/ew/last-display 'left
  "Either `left' or `right'.")

;; TODO(thiderman): Make into a hook that sets up based on amount of
;; screens
(defun th/ew/plist (screens)
  "Returns workspace plist where the screens are spread out.

E.g. for (\"HDMI-1\" \"DP-1\") -> (0 \"HDMI-1\" 1 \"DP-1\" 2 \"HDMI-1\" ...)"
  (let (wp)
    (dotimes (i exwm-workspace-number)
      (setq wp (plist-put wp i (nth (mod i (length screens)) screens))))
    wp))

(defun th/ew/setup (screens hook)
  "Configure workspace variables.

`screens' is a list of randr outputs to configure
`spaces' is the name of the different workspaces
`hook' is a randr hook to be run on screen change"

  (setq th/ew/screens screens)
  (setq exwm-randr-workspace-output-plist (th/ew/plist screens))
  (add-hook 'exwm-randr-screen-change-hook hook))

(defun th/ew/name (index)
  "Return the name of the workspace at `index'"
  (nth index th/ew/workspaces))

(defun th/ew/name-for-exwm-workspace (index)
  "Return the name of the exwm workspace at `index'"
  (th/ew/name (/ index th/ew/screens-per-workspace)))

(defun th/ew/index (name)
  "Return the index of the workspace named `name'"
  (-elem-index name th/ew/workspaces))

(defun th/ew/workspace-index (&optional name)
  "Return the starting exwm workspace index of the workspace named `name'"
  (let ((name (or name th/ew/current)))
    (* th/ew/screens-per-workspace (th/ew/index name))))

(defun th/ew/goto (name)
  ;; This is so that the dev workspace is set properly when switching away from
  ;; the non-dev workspaces.
  (unless (member th/ew/current th/ew/default-workspaces)
    (setq th/ew/last-dev th/ew/current))
  (setq th/ew/current name)
  (let ((ws (th/ew/workspace-index name)))
    (exwm-workspace-switch ws)
    (when (th/ew/multi-screen-p)
      (exwm-workspace-switch (+ ws 1)))
    (when (eq th/ew/last-display 'left)
      (th/ew/left))))

(defun th/ew/multi-screen-p ()
  (not (= 1 (length th/ew/screens))))

(defun th/ew/-hud-internal ()
  (let* ((index exwm-workspace-current-index)
         (odd (oddp index))
         (even (evenp index))
         (one-screen (= (length th/ew/screens) 1)))

    (s-join (propertize " / " 'face 'font-lock-comment-delimiter-face)
            (-map
             (lambda (x)
               (let ((current (string-equal x th/ew/current)))
                 (concat
                  (propertize
                   (th/ew/-hud--screenchar current one-screen even "<")
                   'face
                   'font-lock-string-face)

                  (propertize x 'face (when current 'font-lock-keyword-face))

                  (propertize
                   (th/ew/-hud--screenchar current one-screen odd ">")
                   'face
                   'font-lock-string-face))))
             th/ew/workspaces))))

(defun th/ew/-hud--screenchar (current one-screen show char)
  (cond ((and current one-screen show)
         char)
        (one-screen
         " ")
        (t
         "")))

(defun th/ew/hud ()
  "Display a status bar with workspaces, with the current one highlighted"
  ;; Setting `message-log-max' to nil bypasses the *Messages* buffer
  (let ((message-log-max nil))
    (message "%s" (th/ew/-hud-internal))))

;;;###autoload
(defun th/ew/switch (&optional name)
  "Goto or create workspace `name'"
  (interactive)
  ;; We remove the workspaces in `th/ew/default-workspaces' since we always have
  ;; keyboard shortcuts for them
  (let ((name (or name (ivy-read
                        "workspace: "
                        (-filter
                         (lambda (x)
                           (not (member
                                 x
                                 th/ew/default-workspaces)))
                         th/ew/workspaces)
                        :preselect th/ew/current))))
    (add-to-list 'th/ew/workspaces name t)
    (th/ew/goto name)))



;;;###autoload
(defun th/ew/remove (&optional name)
  "Remove workspace `name'"
  (interactive)
  (when (and name (not (cl-position name th/ew/workspaces)))
    (user-error "'%s' is not currently a workspace" name))
  (let* ((name (or name (ivy-read "remove: " th/ew/workspaces :preselect th/ew/current)))
         (removing-current (string-equal name th/ew/current)))
    (when removing-current
      ;; If we removed the workspace we're on, go to the one earlier in the
      ;; list.
      (th/ew/previous))
    (setq th/ew/workspaces (remove name th/ew/workspaces))
    (th/ew/hud)))

;;;###autoload
(defun th/ew/rename (&optional name)
  "Rename the current workspace to `name'"
  (interactive)
  (let ((name (or name (read-string "rename: " th/ew/current))))
    (setq th/ew/workspaces (-replace th/ew/current name th/ew/workspaces))
    (setq th/ew/current name)))

;;;###autoload
(defun th/ew/next ()
  "Go to the next workspace, looping back if stepping over the last"
  (interactive)
  (th/ew/goto (th/ew/shift 1)))

;;;###autoload
(defun th/ew/previous ()
  "Go to the previous workspace, looping back if stepping over the first"
  (interactive)
  (th/ew/goto (th/ew/shift -1)))

;;;###autoload
(defun th/ew/dev (&optional select)
  "Go to the last visited development workspace.

If there aren't any, prompt for a project to go to. If the
optional SELECT argument is given, show a prompt of workspaces to
choose between."
  (interactive)
  (if (null th/ew/last-dev)
      (th/ew/switch-project)
    (th/ew/switch
     (if select
         ;; If we provide nil, we will be prompted in the switch function.
         nil
       ;; And if we don't, we go do whichever one we were last at.
       th/ew/last-dev))))

;;;###autoload
(defun th/ew/left ()
  "Go to the left screen of the current workspace"
  (interactive)
  (setq th/ew/last-display 'left)
  (exwm-workspace-switch (th/ew/workspace-index)))

;;;###autoload
(defun th/ew/right ()
  "Go to the right screen of the current workspace"
  (interactive)
  (setq th/ew/last-display 'right)
  (exwm-workspace-switch (+ 1 (th/ew/workspace-index))))

(defun th/ew/shift (shift)
  "Step `shift' steps in workspaces, looping back if stepping over the last"
  ;; TODO(thiderman): Currently only works in steps of one, but should
  ;; be good enough. Who steps multiple? :thonk:
  (let* ((current (or (-elem-index th/ew/current th/ew/workspaces) 0))
         (ws (length th/ew/workspaces))
         (target (+ shift current))
         (final (cond
                 ;; If we are on a negative target, we should just go
                 ;; to the last
                 ((= target -1)
                  (- ws 1))
                 ;; If we are moving outside, go back to the first
                 ((= ws target)
                  0)
                 (t
                  target))))
    (nth final th/ew/workspaces)))

(defun th/ew/switch-project ()
  "Switch to a project, creating its workspace if it doesn't exist"
  (interactive)
  (let* ((repo (magit-read-repository))
         ;; Also remove the tink- prefix
         (name (s-chop-suffix "-of-tink"
                              (s-chop-prefix "tink-" (f-base repo))))
         (exists (member name th/ew/workspaces)))
    ;; Always go to the workspace
    (th/ew/switch name)
    (setq th/ew/last-dev name)

    ;; If it didn't exist before, we want to populate both screens with
    ;; something that is relating to the project
    (unless exists
      ;; On the left side, open dired of the project
      (th/ew/left)
      (find-file repo)

      ;; On the right side, bring up magit
      (th/ew/right)
      (magit-status-internal repo)
      (delete-other-windows))))

(defadvice windmove-left (around windmove-left-screen activate)
  "Move to the left screen if we're hitting the left border.

Raise any other error normally"
  (condition-case errors
      ad-do-it
    (error
     (let ((e (second errors)))
       (if (string-equal e "No window left from selected window")
           (th/ew/left)
         (user-error e))))))

(defadvice windmove-right (around windmove-right-screen activate)
  "Move to the right screen if we're hitting the left border.

Raise any other error normally"
  (condition-case errors
      ad-do-it
    (error
     (let ((e (second errors)))
       (if (string-equal e "No window right from selected window")
           (th/ew/right)
         (user-error e))))))

(defadvice exwm-workspace-switch (after th/ew/auto-hud activate)
  "Display the HUD after switching workspaces"
  (th/ew/hud))

;;;;* EXWM init

(defun th/exwm-init ()
  (cond
   ((s-equals? (system-name) "dragonisle")
    (th/ew/setup
     (th/get-connected-screens)
     #'exwm-randr-dragonisle))

   (t
    (th/ew/setup
     ;; If we have three screens, just use two of them
     (let ((screens (th/get-connected-screens)))
       (if (= 3 (length screens))
           (cdr screens)
         screens))
     #'th/exwm-randr-hook))))

(th/exwm-init)
(exwm-randr-enable)
;; Do not forget to enable EXWM. It will start by itself when things are ready.
(exwm-enable)

;;;* Keymap

(defmacro il (&rest body)
  "Shorthand for making an interactive lambda for use in keymaps"

  `(lambda () (interactive) ,@body))

;; Emacs bindings
;; C-c
(bind-key "C-c C-j" #'dumb-jump-go-other-window)

;; C-x
(bind-key "C-x C-j" #'dired-jump)
(bind-key "C-x k" (il
                   (let ((name (buffer-name)))
                     (kill-buffer (current-buffer))
                     (delete-window)
                     (message "Buffer %s killed" name))))
(bind-key "C-x M-k" #'kill-buffer)
(bind-key "C-x e" #'emojify-insert-emoji)
(bind-key "C-x C-r" #'revert-buffer)

;; M-
(bind-key "M-p" #'beginning-of-defun)
(bind-key "M-n" #'end-of-defun)

;; Window management
(bind-key "<escape>" (il (split-window-sensibly)))
(bind-key "<M-escape>" #'delete-other-windows)
(bind-key "C-q" #'delete-window)

(bind-key "<s-XF86Launch6>" #'worf-reserved)

;; Since I do these ones the most, it makes sense to have simpler binds
(bind-key "s-M-7" (il (th/iosevka 10)))
(bind-key "s-M-8" (il (th/iosevka 13)))
(bind-key "s-M-9" (il (th/iosevka 17)))
(bind-key "s-M-0" (il (th/iosevka 24)))

;; Extra things
(bind-key "<f2>" #'purple-heart)
(bind-key "<f3>" (il (find-file
                      (file-truename
                       (expand-file-name "init.el" user-emacs-directory)))))

;; Special things on F5
(cond
 ((s-equals? (system-name) "dragonwing")
  (bind-key "<f5>" (il (th/xrandr "eDP1 --auto"))))
 ((s-equals? (system-name) "dragonisle")
  (bind-key "<f5>" (il (shell-command-to-string
                        "systemctl --user restart redshift")
                       (message "Restarted redshift")))))

;; EXWM bindings
;; + Bind "s-0" to "s-9" to switch to the corresponding workspace.
(dotimes (i 10)
  (exwm-input-set-key (kbd (format "s-%d" i)) `(lambda () (interactive) (exwm-workspace-switch-create ,i))))

(exwm-input-set-key (kbd "s-`") #'outline-show-entry)
(exwm-input-set-key (kbd "s-q") #'th/browser-hydra/body)
(exwm-input-set-key (kbd "s-w") (il (th/ew/switch "mail")))
(exwm-input-set-key (kbd "s-e") #'th/eshell-here)
(exwm-input-set-key (kbd "s-r") #'th/zetteldeft/body)
(exwm-input-set-key (kbd "s-t") #'tink-hydra/body)
(exwm-input-set-key (kbd "s-y") #'hydra-flycheck/body)
(exwm-input-set-key (kbd "s-u") #'spotify-hydra/body)
(exwm-input-set-key (kbd "s-i") nil) ; dunst notification clear
(exwm-input-set-key (kbd "s-o") #'th/org-hydra/body)
(exwm-input-set-key (kbd "s-p") #'th/ew/next)
(exwm-input-set-key (kbd "s-=") #'mail-hydra/body)

(exwm-input-set-key (kbd "s-a") (il (th/ew/switch "main")))
(exwm-input-set-key (kbd "s-s") (il (th/ew/switch "messaging")))
(exwm-input-set-key (kbd "s-d") #'th/ew/dev)
(exwm-input-set-key (kbd "s-f") #'th/projects/body)
(exwm-input-set-key (kbd "s-g") #'magit-status)
(exwm-input-set-key (kbd "s-h") #'windmove-left)
(exwm-input-set-key (kbd "s-j") #'windmove-down)
(exwm-input-set-key (kbd "s-k") #'windmove-up)
(exwm-input-set-key (kbd "s-l") #'windmove-right)
(exwm-input-set-key (kbd "s-;") #'th/spotify)
(exwm-input-set-key (kbd "s-'") #'th/ew/switch-project)

(exwm-input-set-key (kbd "s-z") #'split-window-below)
(exwm-input-set-key (kbd "s-x") #'split-window-right)
(exwm-input-set-key (kbd "s-c") #'hydra-lsp/body)
(exwm-input-set-key (kbd "s-v") #'th/toggle-hydra/body)
(exwm-input-set-key (kbd "s-b") #'th/buffer-switch)
(exwm-input-set-key (kbd "s-n") #'th/ew/previous)
(exwm-input-set-key (kbd "s-m") #'mail-hydra/body)
(exwm-input-set-key (kbd "s-,") #'emojify-insert-emoji)
(exwm-input-set-key (kbd "s-.") #'exwm-status)
(exwm-input-set-key (kbd "s-/") #'major-mode-hydra)
(exwm-input-set-key (kbd "s--") #'th/silverwing-hydra/body)

(exwm-input-set-key (kbd "s-SPC") #'th/ew/hydra/body)
(exwm-input-set-key (kbd "s-<backspace>") (il (start-process "" nil "lock")))
(exwm-input-set-key (kbd "s-<tab>") #'th/switch-to-previous-buffer)
(exwm-input-set-key (kbd "s-M-<tab>") #'th/ew/next)
(exwm-input-set-key (kbd "s-<return>") #'th/exwm-terminal)
(exwm-input-set-key (kbd "s-M-.") (il (exwm-status t)))
(exwm-input-set-key (kbd "s-M-,") #'emojify-mode)

(exwm-input-set-key (kbd "s-M-SPC") #'exwm-execute)
(exwm-input-set-key (kbd "s-M-C-h") #'shrink-window-horizontally)
(exwm-input-set-key (kbd "s-M-C-j") #'shrink-window)
(exwm-input-set-key (kbd "s-M-C-k") #'enlarge-window)
(exwm-input-set-key (kbd "s-M-C-l") #'enlarge-window-horizontally)
(exwm-input-set-key (kbd "s-M-w") #'writeroom-mode)
(exwm-input-set-key (kbd "s-M-t") #'transpose-frame)
(exwm-input-set-key (kbd "s-M-s") #'th/silverwing-workspace)
(exwm-input-set-key (kbd "s-M-d") (il (th/ew/dev t)))
(exwm-input-set-key (kbd "s-M-h") (il (shrink-window-horizontally 3)))
(exwm-input-set-key (kbd "s-M-j") (il (shrink-window 3)))
(exwm-input-set-key (kbd "s-M-k") (il (enlarge-window 3)))
(exwm-input-set-key (kbd "s-M-l") (il (enlarge-window-horizontally 3)))
(exwm-input-set-key (kbd "s-M-o") #'outline-show-all)

(exwm-input-set-key (kbd "s-H") #'buf-move-left)
(exwm-input-set-key (kbd "s-J") #'buf-move-down)
(exwm-input-set-key (kbd "s-K") #'buf-move-up)
(exwm-input-set-key (kbd "s-L") #'buf-move-right)

;; These are the alphabetical keys with Fn held down
(exwm-input-set-key (kbd "s-(") #'th/cycle-prev)        ; q
(exwm-input-set-key (kbd "s-)") #'th/cycle-next)        ; w
(exwm-input-set-key (kbd "s-[") nil)                    ; e
(exwm-input-set-key (kbd "s-]") nil)                    ; r
(exwm-input-set-key (kbd "s-_") #'th/dired-goto-tmp)    ; t
(exwm-input-set-key (kbd "s-~") #'th/dired-goto-home)   ; y
(exwm-input-set-key (kbd "s-|") #'verb-the-noun)        ; i
(exwm-input-set-key (kbd "s-\\") #'th/wide-emoji-text)  ; o
;; (exwm-input-set-key (kbd "s-å") nil)                 ; =

(exwm-input-set-key (kbd "s-^") nil)                    ; a
(exwm-input-set-key (kbd "s-$") nil)                    ; s
(exwm-input-set-key (kbd "s-{") nil)                    ;
(exwm-input-set-key (kbd "s-}") nil)
(exwm-input-set-key (kbd "s-\"") nil)

(exwm-input-set-key (kbd "s-<left>") (il (th/volume 'down)))
(exwm-input-set-key (kbd "s-<right>") (il (th/volume 'up)))
(exwm-input-set-key (kbd "s-<up>") (il (th/volume 'toggle)))
(exwm-input-set-key (kbd "s-<down>") (il (th/volume 'toggle)))

(exwm-input-set-key (kbd "s-<XF86Launch8>") #'winner-undo)
(exwm-input-set-key (kbd "s-<XF86Launch9>") #'winner-redo)
(exwm-input-set-key (kbd "s-?") nil)
(exwm-input-set-key (kbd "s-:") nil)

;; dunst binds s-i to close notification. If there are none, it passes the
;; keypress on to Emacs, so we might just as well show nothing here rather
;; than the "not bound" message.
(exwm-input-set-key (kbd "s-i") (il (message "")))
(exwm-input-set-key (kbd "s-M-b") #'balance-windows)
(exwm-input-set-key (kbd "C-s-p") (il (start-process-shell-command "ss" nil "ss -s")))
(exwm-input-set-key (kbd "C-M-s-p") (il (start-process-shell-command "ss" nil "ss")))
(exwm-input-set-key (kbd "s-C-q") #'browser-start-session)
(exwm-input-set-key (kbd "s-C-M-q") #'th/golden-split)
(exwm-input-set-key (kbd "C-s-k") #'delete-window)
(exwm-input-set-key (kbd "s-<escape>") #'delete-window)
(exwm-input-set-key (kbd "s-C-g") #'th/goto-repo)
(exwm-input-set-key (kbd "s-S") #'shrug)
(exwm-input-set-key (kbd "s-P") #'purple-heart)
